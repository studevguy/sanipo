module.exports = function (grunt) {
    'use strict';
    var _ = require('lodash');
    var env = grunt.option('env');

    var today = new Date();

    var project = {
        bowerComponent: 'bower_components/',
        scriptSrc: 'js/',
        templateSrc: 'templates/',
        assetsDir: 'assets/',
        lessSrc: 'less/merchant/main.less',
        cssDir: 'assets/css/',
        dirs: {},
        feature: 'merchant',
        buildTracking: '' + today.getFullYear() + today.getMonth() + today.getDate() + today.getHours() + today.getMinutes(),
        files: {
            scripts: 'js/',
            vendor: '/scripts/vendor',
            any: '/**/*',
            thisDir: '/',
            dot: {
                javascript: '.js',
                html: '.html'
            }
        },
        application: {
            commonDir: 'js/common/',
            configFile: 'Configuration.js'
        }
    };


    var merchant_dirs = {
        index: 'merchant/',
        root: 'merchant/',
        temp: 'merchant/temp/'
    };

    var manifest_merchant = require('./js/merchant/manifest.app.js');
    var templates_merchant = require('./js/merchant/manifest.templates.js');

    //config manifest
    manifest_merchant = manifest_merchant.map(function (fileName) {
        fileName = fileName.replace('../', '');
        return fileName + '.js';
    });
    manifest_merchant.unshift('js/merchant/main.js');

    templates_merchant = templates_merchant.map(function (tpl) {
        return tpl.replace('../', '');
    });
    //end

    if ('prod' == env) {
        /*merchant_dirs.root = 'merchant-prod/';
         merchant_dirs.temp = 'merchant-prod/temp/';
         project.feature = 'merchant-prod';*/

        /*Config to load processed Configuration file*/
        var index = _.findIndex(manifest_merchant, function (item) {
            return item == project.application.commonDir + project.application.configFile;
        });
        manifest_merchant[index] = merchant_dirs.temp + project.application.configFile;
        console.log(index);
        console.log(manifest_merchant[index]);
    }

    project.dirs = merchant_dirs;
    project.files.indexHtml = merchant_dirs.index + 'merchant.html';
    project.files.mainJs = 'main-booker.js';
    project.files.templates = templates_merchant;
    project.files.manifest = manifest_merchant;


    var config = {
        pkg: grunt.file.readJSON('package.json'),
        project: project
    };

    grunt.initConfig(config);

    // Read config files from the `grunt/config/` folder
    grunt.file.expand('grunt/merchant/config/*.js').forEach(function (path) {
        var property = /grunt\/merchant\/config\/(.+)\.js/.exec(path)[1],
            module = require('./' + path);
        config[property] = typeof module === 'function' ? module(grunt) : module;
    });

    // Load development dependencies specified in package.json
    for (var dependency in config.pkg.devDependencies) {
        if (/^grunt-/.test(dependency)) {
            grunt.loadNpmTasks(dependency);
        }
    }

    // Load tasks from the `grunt-tasks/` folder
    grunt.loadTasks('grunt/merchant/tasks');
};