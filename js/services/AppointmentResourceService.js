"use strict";

app.service("AppointmentResourceService", ["Configuration", "AjaxService", function (Configuration, AjaxService) {
    var self = this;
    var API_URL = {
        getShopScheduleDetails: Configuration.api.getShopScheduleDetails,
        bookNewAppointment: Configuration.api.bookNewAppointment,
        updateAppointment: Configuration.api.updateAppointment,
        getAllAppointments: Configuration.api.getAllAppointments,
        saveAppointment: Configuration.api.saveAppointment


    };


    this.data = {};
    this.ANY_ONE_ID = '00000000-0000-0000-0000-000000000000';
    this.getAllAppointments = function (params, callBack) {
        var email = params.email;
        var customerId = params.customerId;
        var deferred = Q.defer();
        var url = API_URL.getAllAppointments;

        if (customerId && customerId != '')
            url += 'customerId=' + customerId;
        else if (email && email != '')
            url += 'email=' + email;


        console.log(url);
        AjaxService.ajax({
            method: "GET",
            url: url
        }).then(function (data) {
            console.log(data);
            self.data = data;
            deferred.resolve(data);
            if (callBack)
                callBack(data);

        }, function (error) {

        });

        return deferred.promise;
    };
    this.getShopScheduleDetails = function (params, callBack) {
        var shopId = params.shopId;
        var date = params.date;
        var appointmentId = params.appointmentId;
        var deferred = Q.defer();
        var url = API_URL.getShopScheduleDetails + shopId + "&queryDate=" + date;
        if (appointmentId)
            url += "&appointmentId=" + appointmentId;

        console.log(url);
        AjaxService.ajax({
            method: "GET",
            url: url
        }).then(function (data) {
            console.log(data);
            self.data = data;
            deferred.resolve(data);
            if (callBack)
                callBack(data);

        }, function (error) {

        });

        return deferred.promise;
    };

    this.bookNewAppointment = function (dataInfo, callBack) {

        var deferred = Q.defer();
        var url = API_URL.bookNewAppointment;

        AjaxService.ajax({
            method: "POST",
            url: url,
            data: dataInfo,
            contentType: "application/json"
        }).then(function (response) {
            deferred.resolve(response);
            if (callBack)
                callBack(response);

        }, function (error) {

        });

        return deferred.promise;
    };
    this.updateAppointment = function (dataInfo, callBack) {

        var deferred = Q.defer();
        var url = API_URL.updateAppointment;

        AjaxService.ajax({
            method: "POST",
            url: url,
            data: dataInfo,
            contentType: "application/json"
        }).then(function (response) {
            deferred.resolve(response);
            if (callBack)
                callBack(response);

        }, function (error) {

        });

        return deferred.promise;
    };

    this.saveAppointment = function (dataInfo, callBack) {

        var deferred = Q.defer();
        var url = API_URL.saveAppointment;

        AjaxService.ajax({
            method: "POST",
            url: url,
            data: dataInfo,
            contentType: "application/json"
        }).then(function (response) {
            deferred.resolve(response);
            if (callBack)
                callBack(response);

        }, function (error) {

        });

        return deferred.promise;
    };


    this.isEmployeeAndProductHaveRelation = function (empId, proId) {
        var res = false;
        this.data.employeeProductRelations.forEach(function (relation) {
            if (empId == relation.employeeId && proId == relation.productId) {
                res = true;
                return true;
            }
        });

        return res;
    };


    this.getBookeTimesOfAnyOne = function (shopInfo, strScheduleDate, employeeWorkingTimes) {
        var response = [];
        if (shopInfo.dayOff) {
            return response;
        }

        var i = 0;
        var fromTime;
        var toTime;
        var timeBlock = shopInfo.timeBlock;
        var acceptableSlot = shopInfo.acceptableSlot;
        var strToTime, strFromTime, bookedTime, tmp;

        var startTime = moment(shopInfo.startTime, 'HH:mm');
        var startAcceptanceTime = moment(shopInfo.startAcceptanceTime, 'HH:mm');
        if (startAcceptanceTime > startTime) {

            strFromTime = startTime.format("HH:mm");
            strToTime = startAcceptanceTime.format("HH:mm")
            var bookedTime = {fromTime: strFromTime, toTime: strToTime};
            response.push(bookedTime);
            startTime = startAcceptanceTime;
        }

        var endTime = moment(shopInfo.endTime, 'HH:mm');
        var totalAppointemnts = 0;
        fromTime = startTime;
        while (fromTime.isBefore(endTime)) {
            toTime = fromTime.clone();
            toTime.add(timeBlock, "minutes");
            if (endTime.isBefore(toTime))
                toTime = endTime

            strFromTime = fromTime.format("HH:mm");
            strToTime = toTime.format("HH:mm")
            totalAppointemnts = self.getTotalAppointmentsOnTimes(employeeWorkingTimes, strFromTime, strToTime);
            if (totalAppointemnts >= acceptableSlot) {
                bookedTime = {fromTime: strFromTime, toTime: strToTime};
                response.push(bookedTime);
            } else {
                tmp = self.getFirstEmployeeCanBookOnTimes(employeeWorkingTimes, strFromTime, strToTime);
                if (!tmp) {
                    bookedTime = {fromTime: strFromTime, toTime: strToTime};
                    response.push(bookedTime);
                }
            }
            fromTime.add(timeBlock, "minutes");
        }
        return response;
    }

    this.getBookeTimesRange = function (shopInfo, employeeWorkingTimes) {
        var response = [shopInfo.startTime, shopInfo.endTime];
        employeeWorkingTimes.forEach(function (employee) {

            if (!employee.dayOff && !employee.disable && employee.id != self.ANY_ONE_ID) {
                var timesEmployee = [employee.fromTime, employee.toTime];
                response = _.union(response, timesEmployee);
                employee.booktimes.forEach(function (item) {
                    var times = [item.fromTime, item.toTime];
                    response = _.union(response, times);
                });
            }
        });
        response = _.sortBy(response, function (time) {
            return time;
        });
        return response;
    }


    this.getTotalAppointmentsOnTimes = function (employeeWorkingTimes, fromTime, toTime) {
        var result = 0;

        employeeWorkingTimes.forEach(function (item) {
            if (!item.dayOff && item.id != self.ANY_ONE_ID) {
                result += self.getTotalAppointmentOfEmployeeOnTimes(item, fromTime, toTime);
            }
        });
        return result;
    }
    this.getFirstEmployeeCanBookOnTimes = function (employeeWorkingTimes, fromTime, toTime) {
        var result = false;
        employeeWorkingTimes.forEach(function (item) {
            if (!item.dayOff && !item.disable && item.id != self.ANY_ONE_ID) {
                var canBookOnTimes = self.isEmployeeCanBookOnTimes(item, fromTime, toTime);
                if (canBookOnTimes) {
                    result = item;
                    return result;
                }
            }
        });
        return result;
    }

    this.isTimesIntersect = function (fromTime, toTime, fromTime1, toTime1) {
        if (fromTime >= fromTime1 && fromTime < toTime1) {

            return true;
        }

        if (toTime > fromTime1 && toTime <= toTime1) {

            return true;
        }

        if (fromTime <= fromTime1 && toTime > fromTime1) {

            return true;
        }

        if (fromTime < toTime1 && toTime >= toTime1) {

            return true;
        }

        return false;
    }

    this.getTotalAppointmentOfEmployeeOnTimes = function (employee, fromTime, toTime) {
        var result = 0;
        employee.booktimes.forEach(function (item) {
            if (self.isTimesIntersect(fromTime, toTime, item.fromTime, item.toTime)) {
                result += 1;
            }
        });
        return result;
    }

    this.isEmployeeCanBookOnTimes = function (employee, fromTime, toTime, appointmentTime) {

        var result = true;
        var staffWorkingFromTime = employee.fromTime;
        var staffWorkingToTime = employee.toTime;
        if (staffWorkingFromTime > fromTime || staffWorkingToTime <= fromTime) {
            return false;
        }
        if (staffWorkingFromTime >= toTime || staffWorkingToTime < toTime) {
            return false;
        }
        employee.booktimes.forEach(function (item) {
            if (self.isTimesIntersect(fromTime, toTime, item.fromTime, item.toTime)) {
                result = false;
                return result;
            }

        });

        if (appointmentTime) {
            appointmentTime.forEach(function (item) {
                if (self.isTimesIntersect(fromTime, toTime, item.timeFrom, item.timeTo)) {
                    result = false;
                    return result;
                }
            });
        }

        return result;
    }

    this.isHavingAcceptanceConfig = function (shopSettings) {
        if (angular.isUndefined(shopSettings.acceptanceFromDay))
            return false;

        if (angular.isUndefined(shopSettings.acceptanceToDay))
            return false;
        if (angular.isUndefined(shopSettings.acceptanceToMinutes))
            return false;
        if (angular.isUndefined(shopSettings.acceptanceType))
            return false;


        return true
    };

    function isAllOfDayBetweenOff(fromDate, toDate, datesOff) {
        var result = true;
        var date = fromDate.clone();
        while (!date.isAfter(toDate,'days')) {
            if (!isDateOff(date, datesOff)) {
                result = false;
                return result;
            }
            date.add(1, 'days');
        }

        return result;
    }

    function isDateOff(date, datesOff) {
        var strDate = date.format('YYYY/MM/DD');

        var existed = datesOff.indexOf(strDate) > -1;
        return existed;
    }

    this.isShopAcceptableBookingTime = function (datesOff, shopSettings, strBookDate, checkPrevious) {

        if (!self.isHavingAcceptanceConfig(shopSettings))
            return true;

        var acceptanceFromDay = shopSettings.acceptanceFromDay;

        var acceptanceToMinutes = shopSettings.acceptanceToMinutes;

        var acceptanceType = shopSettings.acceptanceType;

        var acceptanceToDay = shopSettings.acceptanceToDay;

        var mBookDate = moment(strBookDate, 'YYYY/MM/DD');
        if (checkPrevious)
            mBookDate.add(-1, 'days');
        else
            mBookDate.add(1, 'days');

        var currentDate = moment();

        var minDate = currentDate.clone();
        minDate.add(acceptanceToDay, 'days');

        var maxDate = currentDate.clone();
        maxDate.add(acceptanceFromDay, 'days');

        // must be: minDate<bookDate<maxDate
        if (minDate.isAfter(mBookDate, 'days') || maxDate.isBefore(mBookDate, 'days'))
            return false;

        if (checkPrevious) {
            if (isAllOfDayBetweenOff(minDate, mBookDate, datesOff))
                return false;
        }
        else {
            if (isAllOfDayBetweenOff(mBookDate, maxDate, datesOff))
                return false;
        }
        return true;

    }

}]);