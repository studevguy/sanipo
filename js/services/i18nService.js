'use strict';
app.factory('i18nService',
    ['Constants', function (Constants) {
        var __lng = {
            common: {},
            message: {},
            label: {},
            merchant_mobile: {},
            usermobile: {}
        };
        var currentLanguage;
        var deferred;

        init();

        function init() {
            currentLanguage = Constants.LANGUAGE.JAPANESE;
            var $i18nextProviderOptions = {
                getAsync: true,
                lng: currentLanguage,
                useCookie: false,
                useLocalStorage: false,
                fallbackLng: currentLanguage,
                resGetPath: '../assets/translations/merchant-mobile/__lng__.json'
            };

            deferred = Q.defer();

            i18n.init($i18nextProviderOptions, function () {

                extendUserMobileKey();
                extendCommon();

                deferred.resolve(__lng);
            });
        }


        function extendUserMobileKey() {
            __lng.usermobile = {
                title_history: i18n.t('user-mobile.title_history'),
                title_appointment_info: i18n.t('user-mobile.title_appointment_info'),
                title_select_store: i18n.t('user-mobile.title_select_store'),

                title_customer_info: i18n.t('user-mobile.title_customer_info'),
                title_confirm: i18n.t('user-mobile.title_confirm'),
                title_error: i18n.t('user-mobile.title_error'),
                label_product: i18n.t("user-mobile.label_product"),
                label_employee: i18n.t("user-mobile.label_employee"),
                label_time: i18n.t("user-mobile.label_time"),
                label_icon_monney: i18n.t("user-mobile.label_icon_monney"),
                label_anyone: i18n.t("user-mobile.label_anyone"),
                label_name: i18n.t("user-mobile.label_name"),
                label_phone: i18n.t("user-mobile.label_phone"),
                label_email: i18n.t("user-mobile.label_email"),
                label_note: i18n.t("user-mobile.label_note"),
                msg_from_shop: i18n.t("user-mobile.msg_from_shop"),
                your_reserves: i18n.t("user-mobile.your_reserves"),
                past_reserves: i18n.t("user-mobile.past_reserves")
            };
        }

        function extendCommon() {
            var common = {
                today: i18n.t('common.today'),
                tomorrow: i18n.t('common.tomorrow'),
                yesterday: i18n.t('common.yesterday'),
                weekdayNamesShort: [
                    i18n.t('common.week_sunday_short'),
                    i18n.t('common.week_monday_short'),
                    i18n.t('common.week_tuesday_short'),
                    i18n.t('common.week_wednesday_short'),
                    i18n.t('common.week_thursday_short'),
                    i18n.t('common.week_friday_short'),
                    i18n.t('common.week_saturday_short')
                ],

                monthNames: [
                    i18n.t('common.month_january'),
                    i18n.t('common.month_february'),
                    i18n.t('common.month_march'),
                    i18n.t('common.month_april'),
                    i18n.t('common.month_may'),
                    i18n.t('common.month_june'),
                    i18n.t('common.month_july'),
                    i18n.t('common.month_august'),
                    i18n.t('common.month_september'),
                    i18n.t('common.month_october'),
                    i18n.t('common.month_november'),
                    i18n.t('common.month_december')
                ],
                name_title: i18n.t("common.name_title"),
                text_date: i18n.t("common.text_date"),
                text_year: i18n.t("common.text_year")
            };
            var message = {
                msg_shop_close_on_date: i18n.t("message.msg_shop_close_on_date"),
                msg_shop_is_not_configured: i18n.t("message.msg_shop_is_not_configured"),
                msg_required_phone: i18n.t("message.msg_required_phone"),
                msg_required_name: i18n.t("message.msg_required_name"),
                msg_invalid_email: i18n.t("message.msg_invalid_email"),
                msg_reservation_success: i18n.t("message.msg_reservation_success"),
                msg_reservation_error: i18n.t("message.msg_reservation_error"),
                msg_reservation_success_approved: i18n.t("message.msg_reservation_success_approved"),
                msg_reservation_success_unapproved: i18n.t("message.msg_reservation_success_unapproved")
                , msg_booking_time_invalid: i18n.t("message.msg_booking_time_invalid")
                , msg_appointment_deleted_success: i18n.t("message.msg_appointment_deleted_success")
                , msg_error: i18n.t("message.msg_error")
                , msg_invalid_booker_information: i18n.t("message.msg_invalid_booker_information")
                , msg_appointment_confirm_deleted: i18n.t("message.msg_appointment_confirm_deleted")
                , msg_update_appointment_success: i18n.t("message.msg_update_appointment_success")
                , msg_unknown_shop_id: i18n.t("message.msg_unknown_shop_id")
                , msg_invalid_username_password: i18n.t("message.msg_invalid_username_password")
                , msg_appointment_view_only: i18n.t("message.msg_appointment_view_only")
                , msg_appointment_cannot_canceled: i18n.t("message.msg_appointment_cannot_canceled")
                , msg_usrname_pass_incorrect: i18n.t("message.msg_usrname_pass_incorrect")
                , msg_signout_confirm: i18n.t("message.msg_signout_confirm")
                , msg_invalid_customer_name: i18n.t("message.msg_invalid_customer_name")
                , msg_invalid_maxlength_phoneNumber: i18n.t("message.msg_invalid_maxlength_phoneNumber")
                , msg_invalid_maxlength_bookerName: i18n.t("message.msg_invalid_maxlength_bookerName")
                , msg_invalid_maxlength_email: i18n.t("message.msg_invalid_maxlength_email")
                , msg_invalid_maxlength_notes: i18n.t("message.msg_invalid_maxlength_notes")


            };

            var label = {
                label_close: i18n.t("label.label_close"),
                label_minute: i18n.t("label.label_minute"),
                label_shopname: i18n.t("label.label_shopname"),
                label_status: i18n.t("label.label_status"),
                label_status_unapproved: i18n.t("label.label_status_unapproved"),
                label_status_approved: i18n.t("label.label_status_approved"),
                label_status_atstore: i18n.t("label.label_status_atstore"),
                label_status_cancel: i18n.t("label.label_status_cancel"),
                label_cancel: i18n.t("label.label_cancel"),
                label_cancel_appointment: i18n.t("label.label_cancel_appointment"),
                label_update: i18n.t("label.label_update"),
                label_ok: i18n.t("label.label_ok"),
                next: i18n.t('label.next'),
                confirm: i18n.t('label.confirm'),
                back: i18n.t('label.back'),
                label_login: i18n.t('label.label_login'),
                label_logout: i18n.t('label.label_logout'),
                label_shop_id: i18n.t('label.label_shop_id'),
                label_password: i18n.t('label.label_password'),
                label_username: i18n.t('label.label_username'),
                title_popup_confirm: i18n.t("label.title_popup_confirm")

            };

            for (var area in __lng) {
                _.assign(__lng[area], common);
                _.assign(__lng[area], message);
                _.assign(__lng[area], label);
            }
        }

        function registerInitCallback(func) {
            return deferred.promise;
        }

        function translate(area, key) {
            return __lng[area][key];
        }

        function getLanguage(area){
            return __lng[area];
        }

        function locale(){
            return currentLanguage;
        }

        return {
            locale:locale,
            getLanguage: getLanguage,
            translate: translate,
            registerInitCallback: registerInitCallback
        }
    }]);