"use strict";

app.service("ShopResourceService", [
    "Configuration",
    "AjaxService",
    'Constants',
    'i18nService',
    function (Configuration,
              AjaxService,
              Constants,
              i18nService) {
        var self = this;
        var API_URL = {
            getListShopsInfo: Configuration.api.getListShopsInfo,
            shopSettingInfo: Configuration.api.shopSettingInfo,
            shopWokingSchedule: Configuration.api.shopWokingInfo,
            updateAppointmentInfo: Configuration.api.updateAppointmentInfo,
            getAppointmentInfoById: Configuration.api.getAppointmentInfoById,
            cancelAppointment: Configuration.api.cancelAppointment
        };
        var STATUS_KEY_TRANSLATE = Constants.STATUS_KEY_TRANSLATE;
        var DATE_STATUSES = Constants.DATE_STATUSES;
        var APPOINTMENT_STATUS = Constants.APPOINTMENT_STATUS;
        var LANGUAGE = Constants.LANGUAGE;
        var EMPLOYEE_COLOR_RANGE = Constants.EMPLOYEE_COLOR_RANGE;
        this.shops = [];
        this.shopSettingInfos = {};
        this.shopWorkingSchedule = {};
        this.fromDate = '';
        this.toDate = '';

        this.getListShopsInfo = function (callBack) {
            var deferred = Q.defer();
            var url = API_URL.getListShopsInfo;

            AjaxService.ajax({
                method: "GET",
                url: url
            }).then(function (data) {
                _.assign(self.shops, data.shops);
                deferred.resolve(self.shops);
                if (callBack)
                    callBack(self.shops);

            }, function (error) {

            });

            return deferred.promise;
        };

        this.getShopSettingsInfos = function (params, callBack) {
            var shopId = params.shopId;
            var deferred = Q.defer();
            var url = self.__processUrl1(shopId);

            AjaxService.ajax({
                method: "GET",
                url: url
            }).then(function (data) {
                _.assign(self.shopSettingInfos, data);

                self.__translateAppointmentStatus();

                self.__decorateEmployeeColor();

                deferred.resolve(self.shopSettingInfos);

                if (callBack)
                    callBack(self.shopSettingInfos);

            }, function (error) {

            });

            return deferred.promise;
        };

        this.getShopWorkingSchedule = function (shopId, fromDate, toDate) {
            this.fromDate = moment(fromDate, 'YYYY/MM/DD');
            this.toDate = moment(toDate, 'YYYY/MM/DD');
            var deferred = Q.defer();
            var url = self.__processUrl2(shopId, fromDate, toDate);

            AjaxService.ajax({
                method: "GET",
                url: url
            }).then(function (data) {
                self.shopWorkingSchedule = data;
                self.__transferTimeStringToUnixType();
                self.__getEmployeeDetailsForAppointment();
                deferred.resolve(self.shopWorkingSchedule);
            }, function (error) {

            });

            return deferred.promise;
        };

        this.getAppointmentInfoById = function (appointmentId, callBack) {

            var deferred = Q.defer();
            var url = API_URL.getAppointmentInfoById + appointmentId;

            AjaxService.ajax({
                method: "GET",
                url: url
            }).then(function (data) {
                deferred.resolve(data);
                if (callBack)
                    callBack(data);
            }, function (error) {

            });

            return deferred.promise;
        };


        this.updateAppointmentInfo = function (dataInfo, callBack) {

            var deferred = Q.defer();
            var url = API_URL.updateAppointmentInfo;

            AjaxService.ajax({
                method: "POST",
                url: url,
                data: dataInfo,
                contentType: "application/json"
            }).then(function (response) {
                deferred.resolve(response);
                if (callBack)
                    callBack(response);

            }, function (error) {

            });

            return deferred.promise;
        };

        this.cancelAppointment = function (id, dayOfList) {

            var deferred = Q.defer();
            var url = API_URL.cancelAppointment + id;
            console.log(url);
            AjaxService.ajax({
                method: "POST",
                url: url,
                contentType: "application/json"
            }).then(function (response) {
                response.id = id;
                response.dayOfList = dayOfList;
                deferred.resolve(response);
            }, function (error) {

            });

            return deferred.promise;
        };

        this.isEmployeeAndProductHaveRelation = function (empId, proId) {
            var res = false;
            this.shopSettingInfos.employeeProductRelations.forEach(function (relation) {
                if (empId == relation.employeeId && proId == relation.productId) {
                    res = true;
                    return true;
                }
            });

            return res;
        };

        this.getAppointmentsPerDayList = function () {
            var dayList = this.__generateWeekDayList();
            var tempDate;

            dayList.forEach(function (item) {
                self.shopWorkingSchedule.appointments.forEach(function (appt) {
                    tempDate = moment(appt.scheduleDate, 'YYYY/MM/DD').format('MMMM DD');
                    if (tempDate == item.dateOfList) {
                        appt.service = appt.details[0].product.productName;
                        appt.serviceDuration = moment.duration(appt.toTime, 'HH:mm').subtract(moment.duration(appt.fromTime, 'HH:mm')).as('minutes');
                        item.appointmentsList.push(appt);
                    }
                });
            });

            //decorate dayList date
            var i = 0;
            do {
                if (!dayList[i].appointmentsList.length && dayList[i].status == DATE_STATUSES.WORKING) {
                    if (!dayList[i + 1].appointmentsList.length && dayList[i + 1].status == DATE_STATUSES.WORKING) {
                        dayList[i + 1].startPreviousEmptyDate = dayList[i].startPreviousEmptyDate;
                        dayList[i + 1].startPreviousEmptyDate_t = dayList[i].startPreviousEmptyDate_t;
                        dayList.splice(i, 1);
                        i--;
                    }
                } else if (dayList[i].status == DATE_STATUSES.NO_SCHEDULE_SETTING) {
                    if (!dayList[i + 1].appointmentsList.length && dayList[i + 1].status == DATE_STATUSES.NO_SCHEDULE_SETTING) {
                        dayList[i + 1].startPreviousEmptyDate = dayList[i].startPreviousEmptyDate;
                        dayList[i + 1].startPreviousEmptyDate_t = dayList[i].startPreviousEmptyDate_t;
                        dayList.splice(i, 1);
                        i--;
                    }
                }
                i++;
            } while (i < dayList.length - 1);

            //sort schedule by fromTime && toTime
            dayList.forEach(function (item) {
                item.appointmentsList = _.sortByOrder(item.appointmentsList, ['fromTime_unix', 'toTime_unix'], [true, false]);

            });

            return dayList;
        };

        this.getEmployeeTasksByEmployee = function (employeeId) {
          return _.filter(this.shopWorkingSchedule.taskInfos, function (item) {
              return item.employeeId == employeeId;
          });
        };

        this.getTimeRange = function () {
            if (!this.shopWorkingSchedule.shopWorkingTimes.length) {
                return {};
            }
            return {
                fromTime: this.shopWorkingSchedule.shopWorkingTimes[0].fromTime,
                toTime: this.shopWorkingSchedule.shopWorkingTimes[0].toTime
            }
        };

        this.getEmployeeWorkingTime = function (id) {
            return _.find(this.shopWorkingSchedule.employeeWorkingTimes, function (em) {
                return em.employeeId == id;
            })
        };

        this.__findServiceById = function (productId) {
            var product = _.find(self.shopSettingInfos.products, function (item) {
                return item.id == productId;
            });

            return product;
        };
        this.__findAppointmentStatusByValue = function (value) {
            var status = _.find(self.shopSettingInfos.appointmentStatuses, function (item) {
                return item.code == value || item.name == value;
            });

            return status;
        };
        this.__findEmployeeById = function (employeeId) {
            var product = _.find(self.shopSettingInfos.employees, function (item) {
                return item.id == employeeId;
            });

            return product;
        };

        this.__processUrl1 = function (shopId) {
            return API_URL.shopSettingInfo + "?shopId=" + shopId;
        };

        this.__processUrl2 = function (shopId, fromDate, toDate) {
            return API_URL.shopWokingSchedule + "?shopId=" + shopId + "&fromDate=" + fromDate + "&toDate=" + toDate;
        };

        this.__generateWeekDayList = function () {
            var dayList = [];
            var date, dateFormat;
            var today = moment();
            var tommorow = moment().add(1, 'days');
            var yesterday = moment().subtract(1, 'days');

            if(moment.locale() == LANGUAGE.JAPANESE){
                dateFormat = 'MMMMDD' + i18nService.translate('common', 'text_date');
            } else {
                dateFormat = 'MMMM DD';
            }

            for (var i = this.fromDate; (i.isBefore(this.toDate)) || (i.isSame(this.toDate)); i.add(1, "days")) {
                date = {
                    monthOfList_t: moment(i).format("MMMM"),
                    dateOfList_t: moment(i).format(dateFormat),
                    dateOfList: moment(i).format("MMMM DD"),
                    dateOfList_ISO: moment(i).format("YYYY-MM-DD"),
                    startPreviousEmptyDate: moment(i).format("MMMM DD"),
                    startPreviousEmptyDate_t: moment(i).format(dateFormat),
                    status: this.__getDateStatus(i.format('YYYY/MM/DD')),
                    appointmentsList: []
                };

                if(i.isSame(today, 'day')){
                    date.isToday = true;
                } else if (i.isSame(tommorow,'day')){
                    date.isTomorrow = true;
                } else if (i.isSame(yesterday, 'day')){
                    date.isYesterday = true
                }
                dayList.push(date);
            }
            return dayList;
        };

        this.__getDateStatus = function (date) {
            var scheduleDate = _.find(this.shopWorkingSchedule.shopWorkingTimes, function (item) {
                return item.date == date;
            });
            if (scheduleDate) {
                return scheduleDate.status;
            }
            return DATE_STATUSES.NO_SCHEDULE_SETTING;

        };

        this.__translateAppointmentStatus = function () {
            var key, translatedText;
            this.shopSettingInfos.appointmentStatuses.forEach(function (item) {
                key = STATUS_KEY_TRANSLATE[item.name];
                translatedText = i18nService.translate('common', key);
                item.name_t = translatedText ? translatedText : item.name;
            });
        };

        this.getChannelBySpecialType = function (specialType) {
            return _.find(self.shopSettingInfos.bookingChannelInfos, function (item) {
                return item.specialType == specialType;
            })
        };

        this.__hasAlwaysApproveAppointmentSetting = function () {
            return this.shopSettingInfos.shop.tabletAppointmentStatus == APPOINTMENT_STATUS.APPROVED
                && this.shopSettingInfos.shop.webAppointmnetStatus == APPOINTMENT_STATUS.APPROVED;
        };

        this.configStatusesOptionForAppointment = function () {
            if (this.__hasAlwaysApproveAppointmentSetting()) {
                var status = _.find(this.shopSettingInfos.appointmentStatuses, function (item) {
                    return item.name == 'APPROVED';
                });

                status.disable = true;

                return true;
            } else {
                return false;
            }
        };

        this.__transferTimeStringToUnixType = function () {
            this.shopWorkingSchedule.appointments.forEach(function (item) {
                item.fromTime_unix = moment(item.fromTime, 'HH:mm').unix();
                item.toTime_unix = moment(item.toTime, 'HH:mm').unix();
            });
        };

        this.isNoSchedule = function () {
            return this.shopWorkingSchedule.shopWorkingTimes.length == 0 || this.shopWorkingSchedule.employeeWorkingTimes.length == 0;
        };

        this.isDayOff = function () {
            if(this.shopWorkingSchedule.shopWorkingTimes.length ==0){
                return false;
            }
            return this.shopWorkingSchedule.shopWorkingTimes[0].status == DATE_STATUSES.DATE_OFF;
        };

        this.__decorateEmployeeColor = function () {
            var employees = this.shopSettingInfos.employees;
            var colorRangeLength = EMPLOYEE_COLOR_RANGE.length;
            var index;
            for (var i = 0; i < employees.length; i++){

                //to repeat the color of employee
                index = i % (colorRangeLength);
                employees[i].color = EMPLOYEE_COLOR_RANGE[index];
            }
        };

        this.__getEmployeeDetailsForAppointment = function () {
            var empId, emp;
            this.shopWorkingSchedule.appointments.forEach(function (appt) {
                empId = appt.details[0].employeeId;
                emp = self.__findEmployeeById(empId);
                if(emp){
                    appt.details[0].name = emp.fullName;
                    appt.details[0].color = emp.color;
                }
            })
        }

    }]);