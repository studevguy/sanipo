"use strict";

app.service("UtilityService", [function () {
    this.formatCurrency = function (num) {
        var p = num.toFixed(2).split(".");
        return p[0].split("").reverse().reduce(function (acc, num, i, orig) {
                return num + (i && !(i % 3) ? "," : "") + acc;
            }, "");
    };


    this.getStatusText = function (status, i18n) {
        if (status == 'CANCEL')
            return i18n.label_status_cancel;

        if (status == 'UNAPPROVED')
            return i18n.label_status_unapproved;

        if (status == 'APPROVED')
            return i18n.label_status_approved;

        if (status == 'ATSTORE')
            return i18n.label_status_atstore;

        return '';
    };

    this.getInvalidUnicode =  function(theString,chars) {
        var result = false;
        for (var i=0; i < theString.length; i++) {
            var theUnicode = theString.charCodeAt(i).toString(16).toUpperCase();
            while (theUnicode.length < 4) {
                theUnicode = '0' + theUnicode;
            }
            var base16 = parseInt(theUnicode,16);
            var char = _.find(chars, function (item) {
                return item  == base16;
            });

            if(!char)
            {
                if(!result)
                  result = theString.charAt(i);
                else
                  result =result+","+  theString.charAt(i);

            }
        }
        return result;
    }
    this.getWordCount =    function  (text) {
        //var matches = text.match(/[\u00ff-\uffff]|\S+/g);
        return   length.length ;
    };
}]);