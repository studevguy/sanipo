﻿'use strict';

var app = null, templateModule = null;

(function main() {
    templateModule = angular.module('templateModule', []);

    app = angular.module('SunnyPoint',
        [
            'templateModule',
            'ngRoute',
            'angularjs-dropdown-multiselect',
            'ui.bootstrap',
            'angulartics',
            'angulartics.google.analytics'
        ]);

    app.register = app;

    app.config(
        ['$routeProvider', config]);

    function config($routeProvider) {
        $routeProvider
            .when('/m/shopper/select/:shopId/:customerId?', controllerResolver('SelectShopController','../templates/user-mobile/shop_select.html'))
            .when('/m/shopper/edit/:shopId/:appointmentId?',controllerResolver('SelectShopController', '../templates/user-mobile/shop_select.html'))

            .when('/m/shopper/history/:shopId?/:customerId?', controllerResolver('HistoryController', '../templates/user-mobile/history.html'))
            .when('/m/shopper/shoplist', controllerResolver( 'BookerController','../templates/user-mobile/shop_list.html'))
            .otherwise({
                redirectTo: '/m/shopper/shoplist'
            });

        toastr.options = {
            timeOut: 1500,
            showEasing: 'linear',
            hideEasing: 'swing',
            preventDuplicates: false,
            positionClass: "toast-bottom-center"
        };
    }

    function controllerResolver(controller, template) {
        return {
            controller: controller,
            templateUrl: template,
            resolve: {
                i18n: ['i18nService', function (i18nService) {
                    return i18nService.registerInitCallback();
                }]
            }
        }
    }

    if (typeof isDevEnvironment != "undefined") {
        var manifest = [
            '../js/common/JSPrototypeExtension.js',
            '../js/common/Constants',
            '../js/common/Unicodes',
            '../js/common/Configuration',
            '../js/common/ConfigMomentLocale.js',
            '../js/common/AuthenticationRequest',
            '../js/common/InitApplicationData',
            '../js/common/WizardDirective',
            '../js/common/TemplateUrlDirective',
            '../js/common/ProcessAttributeDirective',
            '../js/common/AjaxBlockUIController',
            '../js/common/SwipeDirective',
            '../js/common/DateTimePickerService.js',
            '../js/common/DateTimePickerDirective.js',
            '../js/common/VisibilityDirective.js',

            '../js/services/AppointmentResourceService',
            '../js/services/LocalStorageService',
            '../js/services/i18nService',
            '../js/services/AjaxService',
            '../js/services/ShopResourceService',
            '../js/services/PopupService',
            '../js/services/ApiWebResourceService',
            '../js/services/UtilityService',
            '../js/user-mobile//ModalPopupController.js',
            '../js/user-mobile/BookerController.js',
            '../js/user-mobile/SelectShopController.js',
            '../js/user-mobile/TimeLineDirective.js',
            '../js/user-mobile/TimeLineService.js',
            '../js/user-mobile/EmphasizeHourDirective.js',
            '../js/user-mobile/TimeLineSelectingDirective.js',
            '../js/user-mobile/ColumnScaleDirective.js',
            '../js/user-mobile/HistoryController.js',
            '../js/user-mobile/FineTuneSrollDirective.js',

        ];

        var templates = [
            '../templates/user-mobile/shop_list.html',
            '../templates/user-mobile/shop_select.html',
            '../templates/user-mobile/select_time.html',
            '../templates/user-mobile/customer_info.html',
            '../templates/user-mobile/confirm.html',
            '../templates/user-mobile/error.html',
            '../templates/user-mobile/time_block_partial.html',
            '../templates/user-mobile/history.html',
            '../templates/user-mobile/history_list.html',
            '../templates/user-mobile/display.html',
            '../templates/pgloading.html',
            '../templates/popup.html'
        ];

        var textPlugin = function (url) {
            var plugin = '../bower_components/text/text!';
            return plugin + url;
        };

        var requireDeferred = Q.defer(), domDeferred = Q.defer(), templateDeferred = Q.defer();
        var promises = [requireDeferred.promise, domDeferred.promise, templateDeferred.promise];

        require(_.map(templates, textPlugin), function () {
            for (var i = 0; i < arguments.length; i++) {
                (function (url, template) {
                    templateModule.run(["$templateCache", function ($templateCache) {
                        $templateCache.put(url, template);
                    }]);
                }(templates[i], arguments[i]))
            }
            templateDeferred.resolve();
        });

        require(manifest, function () {
            requireDeferred.resolve();
        });

        $(document).ready(function () {
            domDeferred.resolve();
        });

        //bootstrap angular application after loading all components
        Q.all(promises).then(function () {
            angular.bootstrap(document, ['SunnyPoint']);
        });
    } else {
        angular.element(document).ready(function () {
            angular.bootstrap(document, ['SunnyPoint']);
        });
    }
}());
