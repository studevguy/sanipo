"use strict";

app.controller("LoginController", ['$scope',
    'Constants',
    'i18nService',
    "ShopResourceService",
    "ApiWebResourceService",
    "LocalStorageService",
    '$location',
    function ($scope,
              Constants,
              i18nService,
              shopResourceService,
              apiWebResourceService,
              localStorageService,
              $location) {


        $scope.i18n = i18nService.getLanguage("merchant_mobile");
        $scope.fn = {};
        $scope.credentials = {};
        $scope.credentials.username = "shop1";
        $scope.credentials.password = "123456";
        $scope.credentials.shopId = 2142;
        $scope.showSelectShop = false;

        if (checkCredentials()) {
            redirectToAppointmentsPage();
        }

        $scope.fn.back = function () {
            $scope.showSelectShop = false;
            return false;
        };
        $scope.fn.attemptLogin = function () {
            $scope.loading = true;
            $scope.error = false;
            var authdata = Base64.encode($scope.credentials.username + ':' + $scope.credentials.password);
            processLogin(authdata);
        };

        function processLogin(authdata) {
            $scope.loading = true;
            $scope.error = false;

            if ($scope.showSelectShop) {
                loginToShop($scope.credentials.shopId);
            } else {
                authenticate();
            }

            function loginToShop(shopId) {
                shopResourceService.getShopSettingsInfos({shopId: shopId}, finishGetShopSettingsInfos);
            }

            function finishGetShopSettingsInfos(response) {
                if (response.status != 0) {
                    $scope.error = true;
                    var msg = $scope.i18n.msg_unknown_shop_id
                    msg = msg.replace('{0}', $scope.credentials.shopId);
                    $scope.errormessage = msg;
                } else {
                    var shop = response.shop;
                    localStorageService.store(Constants.SHOP_ID, shop.shopId);
                    localStorageService.store(Constants.SHOP_NAME, shop.shopName);
                    localStorageService.store(Constants.SHOP_PHONE, shop.shopTel);

                    localStorageService.store(Constants.CONTACT_MAIL, shop.shopMail);
                    SetCredentials($scope.credentials, authdata);
                    window.location = "#/appointments";
                }
                $scope.$digest();
            }

            function authenticate() {
                var authdatadecode = Base64.decode(authdata);
                apiWebResourceService.authenticate(authdata, finishAuthenticate);
            }

            function finishAuthenticate(response) {
                $scope.loading = false;
                $scope.error = false;
                console.log(response);
                if (response.status != 0) {
                    $scope.error = true;
                    $scope.errormessage = $scope.i18n.msg_invalid_username_password;
                } else {
                    $scope.credentials.username = response.userName;
                    $scope.credentials.accountType = response.accountType;

                    if (response.accountType == "ADMIN") {
                        $scope.showSelectShop = true;
                        $scope.$digest();
                        return;
                    }

                    if (response.accountType == "BROKER") {
                        if (response.shops.length > 1) {
                            $scope.showSelectShop = true;

                            $scope.shops = response.shops;
                            $scope.$digest();
                            return;
                        }
                    }

                    // if (response.accountType == "SHOP_ACCOUNT") {
                    $scope.showSelectShop = false;
                    var shop = _.first(response.shops);
                    if (!shop.appointmentEnabled) {
                        $scope.error = true;
                        var msg = $scope.i18n.msg_shop_is_not_configured
                        msg = msg.replace('{0}', $scope.credentials.shopId);
                        $scope.errormessage = msg;
                        return;
                    }
                    $scope.credentials.shopId = shop.shopId;

                    localStorageService.store(Constants.SHOP_ID, $scope.credentials.shopId);
                    localStorageService.store(Constants.SHOP_NAME, shop.shopName);
                    localStorageService.store(Constants.SHOP_PHONE, shop.shopPhone);

                    localStorageService.store(Constants.CONTACT_MAIL, shop.contactEmail);
                    SetCredentials($scope.credentials, authdata);
                    redirectToAppointmentsPage();
                    //}
                }
                $scope.$digest();
            }

        };

        function SetCredentials(credentials, authdata) {
            var currentUser = {
                username: credentials.username,
                accountType: credentials.accountType,
                authdata: authdata
            };

            localStorageService.store(Constants.LOCAL_STORAGE_KEY.USER_INFO, currentUser);
        }

        function checkCredentials() {
            var shopId = localStorageService.retrieveString(Constants.SHOP_ID);
            var userInfo = localStorageService.retrieve(Constants.LOCAL_STORAGE_KEY.USER_INFO);

            return shopId && userInfo;
        }

        function redirectToAppointmentsPage() {
            window.location.replace(Constants.PATH.APPOINTMENTS_HASH);
        }

        // Base64 encoding service used by AuthenticationService
        var Base64 = {

            keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

            encode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                do {
                    chr1 = input.charCodeAt(i++);
                    chr2 = input.charCodeAt(i++);
                    chr3 = input.charCodeAt(i++);

                    enc1 = chr1 >> 2;
                    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                    enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                    enc4 = chr3 & 63;

                    if (isNaN(chr2)) {
                        enc3 = enc4 = 64;
                    } else if (isNaN(chr3)) {
                        enc4 = 64;
                    }

                    output = output +
                        this.keyStr.charAt(enc1) +
                        this.keyStr.charAt(enc2) +
                        this.keyStr.charAt(enc3) +
                        this.keyStr.charAt(enc4);
                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";
                } while (i < input.length);

                return output;
            },

            decode: function (input) {
                var output = "";
                var chr1, chr2, chr3 = "";
                var enc1, enc2, enc3, enc4 = "";
                var i = 0;

                do {
                    enc1 = this.keyStr.indexOf(input.charAt(i++));
                    enc2 = this.keyStr.indexOf(input.charAt(i++));
                    enc3 = this.keyStr.indexOf(input.charAt(i++));
                    enc4 = this.keyStr.indexOf(input.charAt(i++));

                    chr1 = (enc1 << 2) | (enc2 >> 4);
                    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                    chr3 = ((enc3 & 3) << 6) | enc4;

                    output = output + String.fromCharCode(chr1);

                    if (enc3 != 64) {
                        output = output + String.fromCharCode(chr2);
                    }
                    if (enc4 != 64) {
                        output = output + String.fromCharCode(chr3);
                    }

                    chr1 = chr2 = chr3 = "";
                    enc1 = enc2 = enc3 = enc4 = "";

                } while (i < input.length);

                return output;
            }
        };

    }]);

