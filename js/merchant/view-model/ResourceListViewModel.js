"use strict";

(function (app) {
    app.factory("ResourceListViewModelFactory", [ResourceListViewViewModelFactory]);

    function ResourceListViewViewModelFactory() {

        function ResourceListViewViewModel() {
            this.list = [];
            this.idCache = [];
        }

        ResourceListViewViewModel.prototype.addResource = function (resources) {
                this.list.push(resources);
                this.idCache.push(resources.id)
        };

        ResourceListViewViewModel.prototype.addResourceList = function (resources) {
            var self = this;
            if (Array.isArray(resources)) {
                self.list = [];
                self.idCache = [];
                resources.forEach(function (rsr) {
                    self.list.push(rsr);
                    self.idCache.push(rsr.id);
                });
            }
        };

        ResourceListViewViewModel.prototype.removeResource = function (resource) {
            _.remove(this.list, function (item) {
                return resource.id == item.id;
            });

            _.remove(this.idCache, function (id) {
                return resource.id == id;
            });
        };

        ResourceListViewViewModel.prototype.isResourceInList = function (resourceId) {
            return -1 != this.idCache.indexOf(resourceId);
        };

        ResourceListViewViewModel.prototype.toggleResource = function (resouce) {
            if (this.isResourceInList(resouce.id)) {
                this.removeResource(resouce)
            } else {
                this.addResource(resouce);
            }
        };

        function newInstance() {
            return new ResourceListViewViewModel();
        }

        return {
            newInstance: newInstance
        };
    }
})(app);
