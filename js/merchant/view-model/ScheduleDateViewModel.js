"use strict";

(function (app) {
    app.factory("ScheduleDateViewModelFactory", [
        'Constants',
        "i18nService",
        ScheduleDateViewModelFactory]);

    function ScheduleDateViewModelFactory(Constants,
                                          i18nService) {

        function ScheduleDateViewModel(date, format) {
            this.date = {};
            this.preserveDate = {};
            this.displayDate = '';
            this.isToday  = true;
            this.BASE_FORMAT = 'YYYY-MM-DD';
            this.POST_FORMAT = 'DD/MM/YYYY';
            this.setDate(date, format);
        }

        ScheduleDateViewModel.prototype.setDate = function (date, format, isTemporary) {
            var fmt = format || this.BASE_FORMAT;
            if(isTemporary){
                this.preserveDate = this.date.clone();
            }
            if (!date) {
                this.date = moment(new Date);
            } else {
                this.date = moment(date, fmt);
            }
            if(!isTemporary){
                this.preserveDate = this.date.clone();
            }
            this.setDisplayDate();
            this.checkToday();
        };

        ScheduleDateViewModel.prototype.setDisplayDate = function () {
            var locale = moment.locale();
            var format = 'YYYY MMMM DD';
            var yearCharacter = i18nService.translate('merchant_mobile', 'text_year');
            var dateCharacter = i18nService.translate('merchant_mobile', 'text_date');
            if (locale == Constants.LANGUAGE.JAPANESE) {
                format = 'YYYY' + yearCharacter + 'MMMM' + 'DD' + dateCharacter;
            }
            this.displayDate = this.date.format(format);
        };

        ScheduleDateViewModel.prototype.getDateFollowPostFormat = function () {
            return this.date.format(this.POST_FORMAT);
        };

        ScheduleDateViewModel.prototype.getDate = function (format) {
            var fmt = format || this.BASE_FORMAT;
            return this.date.format(fmt);
        };

        ScheduleDateViewModel.prototype.revertDate = function(){
            this.date = this.preserveDate.clone();
            this.setDisplayDate();
            this.checkToday();
        };


        ScheduleDateViewModel.prototype.checkToday = function(){
            this.isToday = this.date.isSame(moment(), 'day');
            return this.isToday;
        };

        function newInstance(date, format) {
            return new ScheduleDateViewModel(date, format);
        }

        return {
            newInstance: newInstance
        };
    }
})(app);