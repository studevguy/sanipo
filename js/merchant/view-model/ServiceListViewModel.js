"use strict";

(function (app) {
    app.factory("ServiceListViewModelFactory", [ServiceListViewModelFactory]);

    function ServiceListViewModelFactory() {

        function ServiceListViewModel() {
            this.list = [];
            this.idCache = [];
        }

        ServiceListViewModel.prototype.addService = function (services) {
                this.list.push(services);
                this.idCache.push(services.id)
        };

        ServiceListViewModel.prototype.addServiceList = function (services) {
            var self = this;
            if (Array.isArray(services)) {
                self.list = [];
                self.idCache = [];
                services.forEach(function (svc) {
                    self.list.push(svc);
                    self.idCache.push(svc.id);
                });
            }
        };

        ServiceListViewModel.prototype.removeService = function (service) {
            _.remove(this.list, function (item) {
                return service.id == item.id;
            });

            _.remove(this.idCache, function (id) {
                return service.id == id;
            });
        };

        ServiceListViewModel.prototype.isServiceInList = function (serviceId) {
            return -1 != this.idCache.indexOf(serviceId);
        };

        ServiceListViewModel.prototype.toggleService = function (service) {
            if (this.isServiceInList(service.id)) {
                this.removeService(service)
            } else {
                this.addService(service);
            }
        };

        ServiceListViewModel.prototype.getOverallDuration = function () {
            var duration = _.sum(this.list, function (item) {
                return item.duration;
            });

            return duration;
        };

        ServiceListViewModel.prototype.removeIfServiceNotEnable = function (enableList) {
            var isProductEnable, prodId;

            for (var i = 0; i < this.list.length; i++) {
                prodId = this.list[i].id;
                isProductEnable = -1 != _.findIndex(enableList, function (item) {
                      return item == prodId;
                    });
                if (!isProductEnable) {
                    this.removeService(this.list[i]);
                    i--;
                }
            }
        };

        function newInstance() {
            return new ServiceListViewModel();
        }

        return {
            newInstance: newInstance
        };
    }
})(app);