"use strict";

(function (app) {
    app.factory("TagListViewModelFactory", [TagListViewViewModelFactory]);

    function TagListViewViewModelFactory() {

        function TagListViewModel() {
            this.list = [];
        }

        TagListViewModel.prototype.addTags = function (ids) {
            var self = this;
            if (Array.isArray(ids)) {
                ids.forEach(function (rsr) {
                    self.list.push(rsr);
                });
            } else {
                self.list.push(ids);
            }
        };

        TagListViewModel.prototype.removeTag = function (id) {
            _.remove(this.list, function (item) {
                return id == item;
            });
        };

        TagListViewModel.prototype.isTagInList = function (id) {
            var index = _.findIndex(this.list, function (item) {
                return id == item;
            });

            return -1 != index;
        };

        TagListViewModel.prototype.toggleTag = function (tag) {
            if(this.isTagInList(tag)) {
                this.removeTag(tag)
            } else {
                this.addTags(tag);
            }
        };

        function newInstance() {
            return new TagListViewModel();
        }

        return {
            newInstance: newInstance
        };
    }
})(app);