"use strict";

(function (app) {
    app.controller("CancelAppointmentController", [
    '$scope',
    'CancelAppointmentView',
    CancelAppointmentController]);

    function CancelAppointmentController($scope, CancelAppointmentView) {
        var canceledAppointment = $scope.$parent.canceledAppointment;
        var view = CancelAppointmentView.newInstance($scope, canceledAppointment);
        view.show();

    }
})(app);