"use strict";

(function (app) {
    app.factory("CancelAppointmentView", [
        'PopupService',
        'AppointmentEventBus',
        'ShopResourceService',
        'Configuration',
        'i18nService',
        'Constants',
        CancelAppointmentView]);

    function CancelAppointmentView(PopupService,
                                   AppointmentEventBus,
                                   ShopResourceService,
                                   Configuration,
                                   i18nService,
                                   Constants) {

        function View($scope, canceledAppointment) {
            this.scope = $scope;
            this.data = {};
            this.fn = {};
            this.status = {};
            this.scope.data = this.data;
            this.scope.fn = this.fn;
            this.scope.status = this.status;

            this.status.sendEmail = false;
            this.status.isSendmailMessageShown = false;

            this.canceledAppointment = canceledAppointment;
        }

        View.prototype.show = function () {
            console.log('start to show CancelAppointmentView');
            this.bindEvents();
            this.verifyToShowSendEmailMessage();
        };

        View.prototype.bindEvents = function () {
            var self = this;

            self.fn.closeCancelPopup = function () {
                PopupService.close();
            };

            self.fn.cancelAppointment = function () {
                ShopResourceService.cancelAppointment(self.canceledAppointment.appointmentId)
                    .then(self.finishCancelAppointmentMission.bind(self), self.showError.bind(self));
            };
        };

        View.prototype.finishCancelAppointmentMission = function () {
            var self = this;

            PopupService.close();
            toastr.success(i18nService.translate('merchant_mobile', 'msg_appointment_deleted_success'));
            if(this.status.sendEmail){
                var data = _.clone(this.canceledAppointment, true);
                data.dayOfList = undefined;
                data.channel = undefined;
                ShopResourceService.sendEmailInformAppointmentChanged(data);
            }
            setTimeout(function () {
                AppointmentEventBus.publishCancel(self.canceledAppointment);
            }, 500);
        };

        View.prototype.verifyToShowSendEmailMessage = function(){

            if(Constants.CHANNEL_TYPE.TABLET != this.canceledAppointment.channel && this.canceledAppointment.emailAddress){
                this.status.isSendmailMessageShown = true;
            }
        };

        View.prototype.showError = function (code) {
            if (code == Configuration.ERROR_CODE.SERVER_ERROR) {
                toastr.error(i18nService.translate('merchant_mobile', 'msg_server_error'));
            }
        };


        function newInstance($scope, canceledAppointment) {
            return new View($scope, canceledAppointment);
        }

        return {
            newInstance: newInstance
        };
    }
})(app);