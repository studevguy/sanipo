"use strict";

(function (app) {
    app.factory("NewAppointmentView", [
        'Configuration',
        'ShopResourceService',
        'Constants',
        'LocalStorageService',
        'TabsetSvc',
        'i18nService',
        'AppointmentEventBus',
        '$routeParams',
        'ServiceListViewModelFactory',
        'ResourceListViewModelFactory',
        'ScheduleDateViewModelFactory',
        'PopupService',
        'UtilityService',
        'TagListViewModelFactory',
        NewAppointmentView]);

    function NewAppointmentView(Configuration,
                                ShopResourceService,
                                Constants,
                                LocalStorageService,
                                TabsetSvc,
                                i18nService,
                                AppointmentEventBus,
                                $routeParams,
                                ServiceListViewModelFactory,
                                ResourceListViewModelFactory,
                                ScheduleDateViewModelFactory,
                                PopupService,
                                UtilityService,
                                TagListViewModelFactory) {

        function View($scope) {
            this.scope = $scope;
            this.data = {};
            this.fn = {};
            this.status = {};
            this.i18n = {};
            this.scope.data = this.data;
            this.scope.fn = this.fn;
            this.scope.status = this.status;
            this.scope.i18n = this.i18n;

            this.data.newAppointment = {
                products: ServiceListViewModelFactory.newInstance(),
                resources: ResourceListViewModelFactory.newInstance(),
                employee: {},
                fromTime: '',
                toTime: '',
                scheduleDate: ScheduleDateViewModelFactory.newInstance(),
                booker: {},
                notes: '',
                channel: Constants.CHANNEL_TYPE.TABLET,
                status: '',
                tags: TagListViewModelFactory.newInstance()

            };
            this.shopId = LocalStorageService.retrieve(Constants.SHOP_ID);
            this.appointmentId = $routeParams.appointmentId;
        }

        View.prototype.registerFirstLoad = function () {
            var registerings = [];
            registerings.push(this.getShopResourceSetting.bind(this));
            if (this.appointmentId) {
                registerings.push(ShopResourceService.getAppointmentInfoById.bind(null, this.appointmentId));
            }
            return registerings;
        };

        View.prototype.show = function () {
            console.log('start show NewAppointment');
            if (this.appointmentId) {
                this.showAppointment();
                TabsetSvc.goToTab(3);
                // store object to detect changing
                this.storedAppointment = _.clone(this.data.newAppointment, true);
                this.storedAppointment.scheduleDate.date = this.data.newAppointment.scheduleDate.date.clone();
            } else {
                TabsetSvc.showFirstTab();
            }
            this.getLanguage();
            this.bindEvents();
            this.bindEventBus();
        };

        View.prototype.bindEvents = function () {
            var self = this;

            self.fn.closePopup = function () {
                PopupService.close();
            };
        };

        View.prototype.bindEventBus = function () {
            var self = this;
            AppointmentEventBus.subscribeScheduleSelectingEvent('NewAppointmentView', function (data) {
                self.data.newAppointment.products.addServiceList(_.clone(data.products.list, true));
                self.data.newAppointment.employee = _.clone(data.employee, true);
                self.data.newAppointment.resources.addResourceList(_.clone(data.resources.list, true));
                self.data.newAppointment.toTime = data.toTime;

                TabsetSvc.nextTab();
            });

            AppointmentEventBus.subscribeDateTimeSelectingEvent('NewAppointmentView', function (signal) {
                self.data.newAppointment.fromTime = _.clone(signal.data.fromTime, true);
                self.data.newAppointment.toTime = _.clone(signal.data.toTime, true);
                self.data.newAppointment.scheduleDate.setDate(signal.data.scheduleDate.getDate());

                if (signal.isGoNext) {
                    TabsetSvc.nextTab();
                } else {
                    TabsetSvc.prevTab();
                }
            });

            AppointmentEventBus.subscribeDetailsSelectingEvent('NewAppointmentView', function (signal) {
                self.data.newAppointment.booker = _.clone(signal.data.booker, true);
                self.data.newAppointment.notes = signal.data.notes;
                self.data.newAppointment.status = signal.data.status;
                self.data.newAppointment.channel = signal.data.channel;
                self.data.newAppointment.tags.list = _.clone(signal.data.tags.list, true);

                if (signal.isGoNext) {
                    self.updateAppointmentInfo();
                } else if (signal.isGoFirst) {
                    TabsetSvc.goToTab(1);
                }
                else {
                    TabsetSvc.prevTab();
                }
            });
        };

        View.prototype.updateAppointmentInfo = function () {
            var data = {};
            data.bookerInfo = {};
            data.employees = [];
            if (!$routeParams.appointmentId) {
                data.id = UtilityService.createUUID();
                this.data.newAppointment.booker.id = UtilityService.createUUID();
            } else {
                data.id = $routeParams.appointmentId;
            }
            data.fromTime = this.data.newAppointment.fromTime;
            data.toTime = this.data.newAppointment.toTime;
            data.notes = this.data.newAppointment.notes;
            angular.extend(data.bookerInfo, this.data.newAppointment.booker);

            //config date format
            angular.extend(data, this.data.newAppointment.info);
            data.scheduleDate = moment(this.data.newAppointment.scheduleDate.getDateFollowPostFormat(), 'DD/MM/YYYY').format('YYYY/MM/DD');

            //config status
            if (ShopResourceService.shopSettingInfos.shop.tabletAppointmentStatus) {
                data.status = ShopResourceService.shopSettingInfos.shop.tabletAppointmentStatus;
            }

            //in case of editing set selected status
            if (this.appointmentId) {
                data.status = this.data.newAppointment.status;
            }

            data.shopId = this.shopId;
            data.bookerInfo.tagIds = [];
            this.data.newAppointment.tags.list.forEach(function (item) {
                data.bookerInfo.tagIds.push(item);
            });

            data.employees.push({
                employeeId: this.data.newAppointment.employee.id,
                employeeRequested: this.data.newAppointment.employee.employeeRequested
            });
            data.products = _.pluck(this.data.newAppointment.products.list, 'id');
            data.resources = _.pluck(this.data.newAppointment.resources.list, 'id');
            data.channel = this.data.newAppointment.channel;

            console.log("------UpdateAppointmentInfo -------");
            console.log(data);
            console.log("------End UpdateAppointmentInfo -------");
            ShopResourceService.updateAppointmentInfo(angular.toJson(data))
                .then(this.finishUpdateAppointmentInfo.bind(this), this.showError.bind(this));

        };

        View.prototype.finishUpdateAppointmentInfo = function (response) {
            console.info(response);
            if (response.status == 0) {

                this.scope.message = i18nService.translate('merchant_mobile', 'msg_reservation_success');
                var options = {
                    templateUrl: '../templates/merchant/update_success_popup.html',
                    size: {
                        top: '25%',
                        height: '300px'
                    },
                    modal: true,
                    closeCallback: function () {
                        window.location.href = Constants.PATH.APPOINTMENTS_HASH;
                    }
                };

                this.verifyAppointmentInfoIsModified();
                this.storeSelectedDate(this.data.newAppointment.scheduleDate.getDate());
                PopupService.open(options, this.scope);
            }
        };

        View.prototype.showError = function (code) {
            if (code == Configuration.ERROR_CODE.SERVER_ERROR) {
                toastr.error(i18nService.translate('merchant_mobile', 'msg_server_error'));
            } else if (code == Configuration.ERROR_CODE.EMPLOYEE_FULL_SLOT) {
                toastr.warning(i18nService.translate('merchant_mobile', 'msg_can_not_book_at_selected_time'))
            }
        };

        View.prototype.showAppointment = function () {

            var appointmentInfo = ShopResourceService.currentAppointment;

            if (!appointmentInfo) {
                console.log('No appointment data');
                return false;
            }
            console.log("edit appointment; id =" + this.appointmentId);

            this.data.newAppointment.channel = appointmentInfo.channel;
            this.data.newAppointment.displayChannelName = '';
            this.data.newAppointment.booker = appointmentInfo.bookerInfo;
            this.data.newAppointment.notes = appointmentInfo.notes;
            this.data.newAppointment.employee = appointmentInfo.employees[0];
            this.data.newAppointment.products.addServiceList(appointmentInfo.products);
            this.data.newAppointment.resources.addResourceList(appointmentInfo.resources);
            this.data.newAppointment.tags.addTags(appointmentInfo.bookerInfo.tagIds);
            this.data.newAppointment.status = appointmentInfo.status;
            this.data.newAppointment.fromTime = appointmentInfo.fromTime;
            this.data.newAppointment.toTime = appointmentInfo.toTime;
            this.data.newAppointment.scheduleDate = ScheduleDateViewModelFactory.newInstance(appointmentInfo.scheduleDate, 'YYYY/MM/DD');
        };

        View.prototype.getShopResourceSetting = function () {
            return ShopResourceService.getShopSettingsInfos({shopId: this.shopId});
        };

        View.prototype.getLanguage = function () {
            _.assign(this.i18n, i18nService.getLanguage("merchant_mobile"));
        };

        View.prototype.unbindAllEventBus = function () {
            AppointmentEventBus.unsubscribeAll();
        };

        View.prototype.clearTabs = function () {

            TabsetSvc.clearActiveTabs();
        };

        View.prototype.clearViewMission = function () {

            this.unbindAllEventBus();
            this.clearTabs();
        };

        View.prototype.storeSelectedDate = function (selectedDate) {

            var storedDateSelectionName = Constants.LOCAL_STORAGE_KEY.SELECTED_DATE;
            LocalStorageService.store(storedDateSelectionName, selectedDate);
        };

        View.prototype.verifyAppointmentInfoIsModified = function () {

            if(!this.appointmentId){
                return false;
            }

            var isProductsModified = false;
            var isAppointmentInfoModified = false;

            isProductsModified = !(_.isEqual(this.storedAppointment.products.idCache.sort(),
                this.data.newAppointment.products.idCache.sort()));

            isAppointmentInfoModified = isProductsModified
                || this.storedAppointment.employee.id != this.data.newAppointment.employee.id
                || this.storedAppointment.fromTime != this.data.newAppointment.fromTime
                || this.storedAppointment.toTime != this.data.newAppointment.toTime
                || !this.storedAppointment.scheduleDate.date.isSame(this.data.newAppointment.scheduleDate.date, 'day')
                || this.storedAppointment.booker.fullName != this.data.newAppointment.booker.fullName
                || this.storedAppointment.booker.displayName != this.data.newAppointment.booker.displayName
                || this.storedAppointment.booker.phoneNumber != this.data.newAppointment.booker.phoneNumber
                || this.storedAppointment.booker.emailAddress != this.data.newAppointment.booker.emailAddress
                || this.storedAppointment.status != this.data.newAppointment.status;

            if (isAppointmentInfoModified
                && this.data.newAppointment.booker.emailAddress
                && this.data.newAppointment.channel != Constants.CHANNEL_TYPE.TABLET) {
                this.status.sendMail = true;

                if ((this.storedAppointment.status != this.data.newAppointment.status)
                    && (Constants.CHANNEL_TYPE.CONFIRM_TYPE_APPROVED == this.data.newAppointment.status)) {
                    this.status.confirmType = Constants.CHANNEL_TYPE.CONFIRM_TYPE_APPROVED;
                } else {
                    this.status.confirmType = Constants.EMAIL_CONFIRM_TYPE.CONFIRM_TYPE_CHANGED;
                }
            }
        };

        function newInstance($scope, appointmentId) {
            return new View($scope, appointmentId);
        }

        return {
            newInstance: newInstance
        };
    }
})(app);