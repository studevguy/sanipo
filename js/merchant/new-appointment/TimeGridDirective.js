"use strict";

(function (app) {
    app.directive("ngTimeGrid", ['TimeGridService', 'Constants', timeGridDirective]);

    function timeGridDirective(TimeGridService, constants) {
        return {
            restrict: "A",
            scope: false,
            compile: function (elm) {
                TimeGridService.load(elm);
            }
        };
    }
})(app);