"use strict";

(function (app) {
    app.factory("AppointmentDetailsView", [
        'TabsetSvc',
        'ShopResourceService',
        'Constants',
        'i18nService',
        'LocalStorageService',
        '$routeParams',
        'AppointmentEventBus',
        'PopupService',
        'Configuration',
        'UtilityService',
        AppointmentDetailsView]);

    function AppointmentDetailsView(TabsetSvc,
                                    ShopResourceService,
                                    Constants,
                                    i18nService,
                                    LocalStorageService,
                                    $routeParams,
                                    AppointmentEventBus,
                                    PopupService,
                                    Configuration,
                                    UtilityService) {

        function View($scope, newAppointment) {
            this.scope = $scope;
            this.data = {};
            this.fn = {};
            this.status = {};
            this.validator = {
                shopperCode: {},
                fullName: {},
                displayName: {},
                phoneNumber: {},
                email: {},
                note: {}
            };
            this.shopId = LocalStorageService.retrieve(Constants.SHOP_ID);
            this.appointmentId = $routeParams.appointmentId;
            this.VALIDATION_RULE = Constants.VALIDATION_RULE;

            this.scope.data = this.data;
            this.scope.fn = this.fn;
            this.scope.status = this.status;

            this.data.newAppointment = newAppointment;
        }

        View.prototype.show = function () {
            console.log('start to show AppointmentDetailsView');
            this.verifyIsEditing();
            this.verifyIsViewing();
            this.data.tags = ShopResourceService.shopSettingInfos.tagInfos;
            this.data.channels = ShopResourceService.shopSettingInfos.bookingChannelInfos;
            this.data.appointmentStatuses = ShopResourceService.shopSettingInfos.appointmentStatuses;
            this.data.newAppointment.displayChannelName = ShopResourceService.getChannelName(this.data.newAppointment.channel);
            this.bindEvents();
            this.bindEventBus();
            if (this.status.isEditing) {
                this.configAppointmentStatus();
            }
        };

        View.prototype.bindEvents = function () {
            var self = this;

            self.fn.toggleStatusSelection = function (status) {

                self.data.newAppointment.status = status.code;
            };

            self.fn.toggleTagSelection = function (tag) {
                if (self.status.isViewing) {
                    return false;
                }
                self.data.newAppointment.tags.toggleTag(tag.id);
            };

            self.fn.toggleChannelAccordion = function () {
                if (self.status.isViewing) {
                    return false;
                }
                self.status.isChanelAccordionOpen = !self.status.isChanelAccordionOpen;
            };

            self.fn.displaySelectedChannelName = function (channel) {
                self.data.newAppointment.displayChannelName = channel.channelName;
            };

            self.fn.goToPrevTab = function () {
                AppointmentEventBus.publishDetailsSelectingEvent({
                    isGoNext: false,
                    data: self.data.newAppointment
                })
            };

            self.fn.goToFirstTab = function () {
                AppointmentEventBus.publishDetailsSelectingEvent({
                    isGoFirst: true,
                    data: self.data.newAppointment
                })
            };

            self.fn.finish = function () {
                if (self.validateUpdateAppointmentInfo()) {
                    AppointmentEventBus.publishDetailsSelectingEvent({
                        isGoNext: true,
                        data: self.data.newAppointment
                    })
                } else {
                    toastr.warning(i18nService.translate('merchant_mobile', 'msg_invalid_booker_information'));
                }
            };

            self.fn.validateEmail = function () {
                self.validateEmail(self.data.newAppointment.booker.emailAddress);
            };

            self.fn.validateShopperCodeFullNameDisplayName = function () {
                self.validateShopperCodeFullNameDisplayName(self.data.newAppointment.booker.shopperCode,
                    self.data.newAppointment.booker.fullName,
                    self.data.newAppointment.booker.displayName);
            };

            self.fn.validatePhoneNumber = function () {
                self.validatePhoneNumber(self.data.newAppointment.booker.phoneNumber);
            };

            self.fn.validateNote = function () {
                self.validateNote(self.data.newAppointment.notes);
            };

            self.fn.showCancelPopup = function () {
                var products = [];
                self.data.newAppointment.products.list.forEach(function (prod) {
                    var tempProd = {};
                    tempProd.id = prod.id;
                    tempProd.name = prod.productName;
                    tempProd.duration = prod.duration.toString();
                    tempProd.price = prod.price.toString();
                    products.push(tempProd);
                });

                var canceledAppointment = {
                    appointmentId: self.appointmentId,
                    confirmType: Constants.EMAIL_CONFIRM_TYPE.CONFIRM_TYPE_CANCELED,
                    bookingDate: self.data.newAppointment.scheduleDate.getDate(),
                    startTime: self.data.newAppointment.fromTime,
                    notes: self.data.newAppointment.notes,
                    duration: UtilityService.calculateTimeDurationByMinute(self.data.newAppointment.fromTime, self.data.newAppointment.toTime),
                    bookerName: self.data.newAppointment.booker.fullName || self.data.newAppointment.booker.displayName || self.data.newAppointment.booker.shopperCode,
                    emailAddress: self.data.newAppointment.booker.emailAddress,
                    mobilePhone: self.data.newAppointment.booker.phoneNumber,
                    shopId: LocalStorageService.retrieveString(Constants.LOCAL_STORAGE_KEY.SHOP_ID),
                    shopName: LocalStorageService.retrieveString(Constants.LOCAL_STORAGE_KEY.SHOP_NAME),
                    shopTel: LocalStorageService.retrieveString(Constants.LOCAL_STORAGE_KEY.SHOP_PHONE),
                    employeeId: self.data.newAppointment.employee.id,
                    employeeName: self.data.newAppointment.employee.fullName,
                    requestStaff: self.data.newAppointment.employee.employeeRequested,
                    productName: products[0].name,
                    productId: products[0].id,
                    productJson: JSON.stringify(products),
                    channel: self.data.newAppointment.channel
                };

                var newScope = self.scope.$new();
                newScope.canceledAppointment = canceledAppointment;



                var options = {
                    templateUrl: '../templates/merchant/cancel_popup.html',
                    size: {
                        top: '30%',
                        height: '250px'
                    },
                    modal: true
                };

                PopupService.open(options, newScope);
            }
        };

        View.prototype.bindEventBus = function () {
            var self = this;

            AppointmentEventBus.subscribeCancel('AppointmentDetailsController', function () {
                setTimeout(function () {
                    window.location.href = Constants.PATH.APPOINTMENTS_HASH;
                }, 1000);
            });
        };

        View.prototype.validateEmail = function (email) {
            if (!email) {
                this.validator.email.isNotValid = false;
                return true;
            }
            var re = this.VALIDATION_RULE.EMAIL.REGEX;

            var isMatched = re.test(email) && (email.length < this.VALIDATION_RULE.EMAIL.MAX);

            console.log('email rule', re.test(email));

            this.validator.email.isNotValid = !isMatched;

            return isMatched;

        };

        View.prototype.validateShopperCodeFullNameDisplayName = function (shopperCode, fullName, displayName) {
            var re = Constants.VALIDATION_RULE.SPECIAL_CHARACTER.REGEX;

            if (!shopperCode && !fullName && !displayName) {
                this.validator.shopperCode.isNotValid = true;
                this.validator.fullName.isNotValid = true;
                this.validator.displayName.isNotValid = true;
                return false
            }

            var isShopperCodeMatched = true, isFullNameMatched = true, isDisplayNameMatched = true;

            if (shopperCode) {
                isShopperCodeMatched = shopperCode.length < this.VALIDATION_RULE.SHOPPER_CODE.MAX;
                //isShopperCodeMatched = isShopperCodeMatched && !re.test(shopperCode);
            }


            if (fullName) {
                isFullNameMatched = fullName.length < this.VALIDATION_RULE.FULL_NAME.MAX;
                isFullNameMatched = isFullNameMatched && !re.test(fullName);
                isFullNameMatched = isFullNameMatched && !UtilityService.isKatakanaCharacter(fullName);
            }


            if (displayName) {
                isDisplayNameMatched = displayName.length < this.VALIDATION_RULE.DISPLAY_NAME.MAX;
                isShopperCodeMatched = isShopperCodeMatched && !re.test(displayName);
                isDisplayNameMatched = isDisplayNameMatched && !UtilityService.isJapaneseCharacter(displayName);
            }

            this.validator.shopperCode.isNotValid = !isShopperCodeMatched;
            this.validator.fullName.isNotValid = !isFullNameMatched;
            this.validator.displayName.isNotValid = !isDisplayNameMatched;

            return isShopperCodeMatched && isFullNameMatched && isDisplayNameMatched;

        };

        View.prototype.validatePhoneNumber = function (phoneNumber) {
            if (!phoneNumber) {
                this.validator.phoneNumber.isNotValid = false;
                return true;
            }

            var re = this.VALIDATION_RULE.PHONE_NUMBER.REGEX;
            var isMatched = re.test(phoneNumber) && (phoneNumber.length < this.VALIDATION_RULE.PHONE_NUMBER.MAX);

            console.log('phone rule', re.test(phoneNumber));
            console.log('phone number lenght', phoneNumber < this.VALIDATION_RULE.PHONE_NUMBER.MAX);

            this.validator.phoneNumber.isNotValid = !isMatched;

            return isMatched;

        };


        View.prototype.validateNote = function (note) {
            if (!note) {
                this.validator.note.isNotValid = false;
                return true;
            }

            var isMatched = note.length < this.VALIDATION_RULE.NOTE.MAX;

            this.validator.note.isNotValid = !isMatched;

            return isMatched;

        };


        View.prototype.validateAllFields = function () {
            this.validateShopperCodeFullNameDisplayName(this.data.newAppointment.booker.shopperCode,
                this.data.newAppointment.booker.fullName,
                this.data.newAppointment.booker.displayName);
            this.validatePhoneNumber(this.data.newAppointment.booker.phoneNumber);
            this.validateEmail(this.data.newAppointment.booker.emailAddress);
            this.validateNote(this.data.newAppointment.notes);
        };

        View.prototype.validateUpdateAppointmentInfo = function () {
            var isPass = true;

            this.validateAllFields();

            for (var field in this.validator) {
                isPass = isPass && !this.validator[field].isNotValid;
            }
            return isPass;

        };

        View.prototype.verifyIsViewing = function () {
            var appointmentTime = this.data.newAppointment.scheduleDate.getDate() + ' ' + this.data.newAppointment.toTime;
            var appointmentTime_m = moment(appointmentTime, 'YYYY-MM-DD HH:mm');
            if (appointmentTime_m.isBefore(moment())) {
                this.status.isViewing = true;
            } else {
                this.status.isViewing = false;
            }
        };

        View.prototype.verifyIsEditing = function () {

            if ($routeParams.appointmentId) {
                this.status.isEditing = true;
            } else {
                this.status.isEditing = false;
            }
        };

        View.prototype.configAppointmentStatus = function () {

            if (this.data.newAppointment.status == Configuration.APPOINTMENT_CODE.APPROVED) {
                _.remove(this.data.appointmentStatuses, function (sts) {
                    return sts.code == Configuration.APPOINTMENT_CODE.UNAPPROVED;
                });
            } else if (this.data.newAppointment.status == Configuration.APPOINTMENT_CODE.ATSTORE) {
                _.remove(this.data.appointmentStatuses, function (sts) {
                    return (sts.code == Configuration.APPOINTMENT_CODE.UNAPPROVED)
                        || (sts.code == Configuration.APPOINTMENT_CODE.APPROVED);
                });
            }
        };

        function newInstance($scope, newAppointment) {
            return new View($scope, newAppointment);
        }

        return {
            newInstance: newInstance
        };
    }
})(app);