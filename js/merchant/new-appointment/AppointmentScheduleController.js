"use strict";

(function (app) {
    app.controller("AppointmentScheduleController", [
        '$scope',
        '$rootScope',
        'AppointmentScheduleView',
        'ServiceListViewModelFactory',
        'ResourceListViewModelFactory',
        AppointmentScheduleController]);

    function AppointmentScheduleController($scope,
                                           $rootScope,
                                           AppointmentServiceView,
                                           ServiceListViewModelFactory,
                                           ResourceListViewModelFactory) {

        var newAppointment;
        var products = ServiceListViewModelFactory.newInstance();
        var resources = ResourceListViewModelFactory.newInstance();
        var employee = $scope.$parent.data.newAppointment.employee || {};

        products.addServiceList(_.clone($scope.$parent.data.newAppointment.products.list, true));
        resources.addResourceList(_.clone($scope.$parent.data.newAppointment.resources.list, true));

        newAppointment = {
            products: products,
            employee: employee,
            resources: resources
        };

        newAppointment.fromTime = $scope.$parent.data.newAppointment.fromTime;
        newAppointment.toTime = $scope.$parent.data.newAppointment.toTime;
        newAppointment.channel = $scope.$parent.data.newAppointment.channel;

        var view = AppointmentServiceView.newInstance($scope, newAppointment);

        view.show();
    }
})(app);