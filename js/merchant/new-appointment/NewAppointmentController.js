"use strict";

(function (app) {
    app.controller("NewAppointmentController", [
        '$scope',
        '$q',
        'NewAppointmentView',
        NewAppointmentController]);

    function NewAppointmentController($scope,
                                      $q,
                                      NewAppointmentView) {
        var view = NewAppointmentView.newInstance($scope);
        var firstLoadRegistering = view.registerFirstLoad();
        if (!firstLoadRegistering.length) {
            view.show();
        } else {
            firstLoad();
        }

        function firstLoad() {
            var promises = [];
            firstLoadRegistering.forEach(function (request) {
                promises.push(request());
            });
            $q.all(promises).then(function () {
                view.show();
            });
        }

        $scope.$on('$destroy', function () {
            view.clearViewMission();
        });
    }
})(app);