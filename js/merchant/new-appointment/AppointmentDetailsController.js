"use strict";

(function (app) {
    app.controller("AppointmentDetailsController", [
        '$scope',
        'AppointmentDetailsView',
        'ScheduleDateViewModelFactory',
        'ServiceListViewModelFactory',
        'ResourceListViewModelFactory',
        'TagListViewModelFactory',
        AppointmentDetailsController]);

    function AppointmentDetailsController($scope,
                                          AppointmentDetailsView,
                                          ScheduleDateViewModelFactory,
                                          ServiceListViewModelFactory,
                                          ResourceListViewModelFactory,
                                          TagListViewModelFactory) {
        var newAppointment = {};
        newAppointment.scheduleDate = ScheduleDateViewModelFactory.newInstance();
        newAppointment.products = ServiceListViewModelFactory.newInstance();
        newAppointment.resources = ResourceListViewModelFactory.newInstance();
        newAppointment.tags = TagListViewModelFactory.newInstance();
        newAppointment.booker = _.clone($scope.$parent.data.newAppointment.booker, true);
        newAppointment.notes = $scope.$parent.data.newAppointment.notes;
        newAppointment.employee = _.clone($scope.$parent.data.newAppointment.employee, true);
        newAppointment.channel = $scope.$parent.data.newAppointment.channel;
        newAppointment.fromTime = $scope.$parent.data.newAppointment.fromTime;
        newAppointment.toTime = $scope.$parent.data.newAppointment.toTime;
        newAppointment.status = $scope.$parent.data.newAppointment.status;

        if ($scope.$parent.data.newAppointment.scheduleDate) {
            newAppointment.scheduleDate.setDate($scope.$parent.data.newAppointment.scheduleDate.getDate());
        }

        if($scope.$parent.data.newAppointment.products.list.length){
            newAppointment.products.addServiceList(_.clone($scope.$parent.data.newAppointment.products.list, true));
        }

        if($scope.$parent.data.newAppointment.resources.list.length){
            newAppointment.resources.addResourceList(_.clone($scope.$parent.data.newAppointment.resources.list, true));
        }

        if($scope.$parent.data.newAppointment.tags.list.length){
            newAppointment.tags.list = _.clone($scope.$parent.data.newAppointment.tags.list, true);
        }

        var view = AppointmentDetailsView.newInstance($scope, newAppointment);

        view.show();

    }
})(app);