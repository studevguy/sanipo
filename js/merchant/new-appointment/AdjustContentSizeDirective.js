/**
 * Created by tiger on 03/07/2015.
 */
"use strict";

app.directive('spAdjustSize', [function () {
    return {
        restrict: 'A',
        scope: false,
        compile: function (scope, elm, attr) {
            function postLink(scope, elm, attr) {
                var trigger = attr.trigger;
                //var $formHeader = $('.select_time .body_header');
                var $dateDisplay = $('.date_display');
                var $datePicker;
                var $timeGrid = $('.time_grid');
                var bodyHeaderHeight, dateDisplayHeight, datePickerHeight ;
                scope.$watch(trigger, function (newVal, oldVal) {
                    $datePicker = $('.picker');
                    //bodyHeaderHeight = $formHeader.height();
                    dateDisplayHeight = $dateDisplay.height();
                    datePickerHeight = $datePicker.height();
                    $timeGrid.animate({
                        'top':  dateDisplayHeight + datePickerHeight + 'px',
                    }, 'fast');
                })
            }

            return {
                post: postLink
            }

        }
    }
}]);