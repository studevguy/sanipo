"use strict";

(function (app) {
    app.factory("AppointmentScheduleView", [
        'ShopResourceService',
        'Constants',
        'i18nService',
        'AppointmentEventBus',
        '$routeParams',
        NewAppointmentView]);

    function NewAppointmentView(ShopResourceService,
                                Constants,
                                i18nService,
                                AppointmentEventBus,
                                $routeParams) {

        function View($scope, newAppointment) {
            this.scope = $scope;
            this.data = {};
            this.fn = {};
            this.i18n = {};
            this.status = {};

            this.scope.i18n = this.i18n;
            this.scope.data = this.data;
            this.scope.fn = this.fn;
            this.scope.status = this.status;

            this.data.newAppointment = newAppointment;
        }

        View.prototype.show = function () {
            console.log('start show AppointmentService');
            //for editing case
            if(this.data.newAppointment.employee.id){
                ShopResourceService.updateProductsData(this.data.newAppointment.employee.id);
            }
            this.data.products = ShopResourceService.shopSettingInfos.products;
            this.data.employees = ShopResourceService.createEmployeeViewModel();
            this.data.resources = ShopResourceService.shopSettingInfos.resources;
            this.getLanguage();
            this.bindEvents();
            this.checkCanGoToNextStep();
            this.verifyIsEditing();
            this.verifyIsHotPepper();
        };

        View.prototype.bindEvents = function () {
            var self = this;

            self.fn.selectService = function ($event, item) {
                if (!item.enable) {
                    return false;
                }
                if(self.status.isHotPepper){
                    return false;
                }
                self.data.newAppointment.products.toggleService(item);
                if(!self.data.newAppointment.products.list.length){
                    self.data.newAppointment.resources.list = [];
                }

                if(self.data.newAppointment.fromTime){
                    var productDuration = self.data.newAppointment.products.getOverallDuration();
                    var fromTime = self.data.newAppointment.fromTime;
                    self.data.newAppointment.toTime = moment(fromTime, 'HH:mm').add(productDuration, 'minutes').format("HH:mm");
                }
                self.checkCanGoToNextStep();
            };

            self.fn.selectEmployee = function ($event, item) {
                self.data.newAppointment.employee.id = item.id;
                self.data.newAppointment.employee.fullName = item.fullName;
                self.data.newAppointment.employee.employeeRequested = item.requireValue;
                ShopResourceService.updateProductsData(item.id);
                self.data.newAppointment.products.removeIfServiceNotEnable(ShopResourceService.getEnableProductList(item.id));
                self.checkCanGoToNextStep();
            };

            self.fn.selectResource = function ($event, item) {
                if(!self.data.newAppointment.products.list.length){
                    return false;
                }
                self.data.newAppointment.resources.toggleResource(item);
            };

            self.fn.goToNextTab = function () {
                AppointmentEventBus.publishScheduleSelectingEvent(self.data.newAppointment);
            };

            self.fn.goToPrevTab = function () {
                window.location.href = Constants.PATH.APPOINTMENTS_HASH;
            };

        };

        View.prototype.checkCanGoToNextStep = function () {
            this.status.isCanGoToNextStep = (this.data.newAppointment.products.list.length)
                && this.data.newAppointment.employee.hasOwnProperty('id')

        };

        View.prototype.verifyIsEditing = function () {

            if ($routeParams.appointmentId) {
                this.status.isEditing = true;
            } else {
                this.status.isEditing = false;
            }
        };

        View.prototype.verifyIsHotPepper = function(){

            if(this.data.newAppointment.channel == Constants.CHANNEL_TYPE.HOT_PEPPER){
                this.status.isHotPepper = true;
            }
        };

        View.prototype.getLanguage = function () {
            _.assign(this.i18n, i18nService.getLanguage("merchant_mobile"));
        };

        function newInstance($scope, newAppointment) {
            return new View($scope, newAppointment);
        }

        return {
            newInstance: newInstance
        };
    }
})(app);