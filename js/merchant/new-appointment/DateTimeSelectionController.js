"use strict";

(function (app) {
    app.controller("DateTimeSelectionController", [
        '$scope',
        'DateTimeSelectionView',
        'ScheduleDateViewModelFactory',
        'ServiceListViewModelFactory',
        DateTimeSelectionController]);

    function DateTimeSelectionController($scope,
                                         DateTimeSelectionView,
                                         ScheduleDateViewModelFactory,
                                         ServiceListViewModelFactory) {
        var newAppointment = {};
        newAppointment.scheduleDate = ScheduleDateViewModelFactory.newInstance();
        newAppointment.products = ServiceListViewModelFactory.newInstance();
        newAppointment.fromTime = _.clone($scope.$parent.data.newAppointment.fromTime);
        newAppointment.toTime = _.clone($scope.$parent.data.newAppointment.toTime);
        newAppointment.employee = _.clone($scope.$parent.data.newAppointment.employee, true);

        if ($scope.$parent.data.newAppointment.scheduleDate) {
            newAppointment.scheduleDate.setDate($scope.$parent.data.newAppointment.scheduleDate.getDate());
        }

        if($scope.$parent.data.newAppointment.products.list.length){
            newAppointment.products.list = _.clone($scope.$parent.data.newAppointment.products.list);
        }
        var view = DateTimeSelectionView.newInstance($scope, newAppointment);
        view.show();
    }
})(app);