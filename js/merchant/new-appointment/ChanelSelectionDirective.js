/**
 * Created by sangdao on 21/07/2015.
 */
"use strict";

(function (app) {
    app.directive("spChanelSelectionHeader", ['ShopResourceService', ChanelSelectionHeaderDirective]);

    function ChanelSelectionHeaderDirective(shopResourceService) {
        return {
            restrict: "A",
            scope: false,
            compile: function (elm) {
                return {
                    post: postLink
                }
            }
        };

        function postLink(scope, elm, attr) {
            var $elm = $(elm);

            $elm.click(clickHandler);

            function clickHandler($event) {
                var target = $event.target;
                if (target.tagName != 'INPUT') {
                    return false;
                }
                setHeaderValue(scope.form.info.channelId);
            }

            function setHeaderValue(channelId) {
                var selectedChannel = shopResourceService.getChannelById(channelId);
                scope.selectedChannelName = selectedChannel.channelName;
                scope.$digest();
            }
        }
    }
})(app);