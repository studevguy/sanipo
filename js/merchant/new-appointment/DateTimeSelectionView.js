"use strict";

(function (app) {
    app.factory("DateTimeSelectionView", [
        "DateTimePickerService",
        "i18nService",
        "Constants",
        "ShopResourceService",
        "TimeGridService",
        "ScheduleDateViewModelFactory",
        "$routeParams",
        "LocalStorageService",
        "TabsetSvc",
        "AppointmentEventBus",
        DateTimeSelectionView]);

    function DateTimeSelectionView(DateTimePickerService,
                                   i18nService,
                                   Constants,
                                   ShopResourceService,
                                   TimeGridService,
                                   ScheduleDateViewModelFactory,
                                   $routeParams,
                                   LocalStorageService,
                                   TabsetSvc,
                                   AppointmentEventBus) {

        function View($scope, newAppointment) {
            this.scope = $scope;
            this.data = {};
            this.status = {};
            this.fn = {};
            this.i18n = {};

            this.scope.data = this.data;
            this.scope.fn = this.fn;
            this.scope.i18n = this.i18n;
            this.scope.status = this.status;

            this.data.newAppointment = newAppointment;
            if (!newAppointment.scheduleDate) {
                this.data.newAppointment.scheduleDate = ScheduleDateViewModelFactory.newInstance();
            }
            this.appointmentId = $routeParams.appointmentId;
            this.shopId = LocalStorageService.retrieve(Constants.SHOP_ID);
        }

        View.prototype.show = function () {
            console.log('start show DateTimeSelection');
            this.status.isDayOff = false;
            this.status.isNoSchedule = false;
            this.getLanguage();
            this.bindEvents();
            this.renderDateTimePicker();
            this.getShopWorkingSchedule();
        };

        View.prototype.getLanguage = function () {
            _.assign(this.i18n, i18nService.getLanguage("merchant_mobile"));
        };

        View.prototype.bindEvents = function () {
            var self = this;

            self.fn.selectNextDate = function () {
                DateTimePickerService.increaseSelectedDate();
            };

            self.fn.selectPreDate = function () {
                DateTimePickerService.decreaseSelectedDate();
            };

            self.fn.selectTime = function ($event) {
                var $target = $($event.currentTarget);
                var fromTime = $target.attr('data-value');
                var productDuration = self.data.newAppointment.products.getOverallDuration();

                self.data.newAppointment.fromTime = fromTime;
                self.data.newAppointment.toTime = moment(fromTime, 'HH:mm').add(productDuration, 'minutes').format("HH:mm");

                self.status.isCanGoToNextStep = true;

                TimeGridService.highlightSelectedTime(fromTime, productDuration);
            };

            self.fn.toggleDatePicker = function () {
                DateTimePickerService.togglePicker();
            };

            self.fn.goToNextTab = function () {
                AppointmentEventBus.publishDateTimeSelectingEvent({
                    isGoNext: true,
                    data: self.data.newAppointment
                });
            };

            self.fn.goToPrevTab = function () {
                AppointmentEventBus.publishDateTimeSelectingEvent({
                    isGoNext: false,
                    data: self.data.newAppointment
                });
            };
        };

        View.prototype.getShopWorkingSchedule = function () {
            var selectedDate = DateTimePickerService.getSelectedDate('YYYY-MM-DD');
            ShopResourceService.getShopWorkingSchedule(this.shopId, selectedDate, selectedDate)
                .then(this.finishGetShopWorkingSchedule.bind(this));
        };

        View.prototype.finishGetShopWorkingSchedule = function () {
            var employeeId = this.data.newAppointment.employee.id;
            var employeeSchedule = ShopResourceService.getEmployeeWorkingTime(employeeId);

            this.status.isDayOff = !employeeSchedule ||  ShopResourceService.isDayOff();
            this.status.isNoSchedule = ShopResourceService.isNoSchedule();

            if (!this.status.isDayOff && !this.status.isNoSchedule) {
                this.status.isCanGoToNextStep = true;
                this.renderTimeGrid();
            } else {
                this.status.isCanGoToNextStep = false;
            }

            this.scope.$digest();
        };

        View.prototype.renderTimeGrid = function () {

            if (!this.data.newAppointment.employee) {
                return false;
            }
            var employeeId = this.data.newAppointment.employee.id;
            var timeRange = ShopResourceService.getTimeRange();
            var employeeSchedule = ShopResourceService.getEmployeeWorkingTime(employeeId);
            var selectedDate = DateTimePickerService.getSelectedDate('YYYY-MM-DD');
            var appointmentFromTime = this.data.newAppointment.fromTime;
            var duration = this.data.newAppointment.products.getOverallDuration();
            var tasks = ShopResourceService.getEmployeeTasksByEmployee(employeeId);
            var timeScale = ShopResourceService.shopSettingInfos.shop.timeBlock;
            TimeGridService.render(this.scope,
                selectedDate,
                timeRange.fromTime,
                timeRange.toTime,
                employeeSchedule.fromTime,
                employeeSchedule.toTime,
                appointmentFromTime,
                duration,
                tasks,
                timeScale);
            if (this.data.newAppointment.fromTime
                && TimeGridService.isTimeValid(this.data.newAppointment.fromTime)
                && TimeGridService.isAppointmentNotPastTime()
            ) {

                TimeGridService.highlightSelectedTime(this.data.newAppointment.fromTime
                    , this.data.newAppointment.products.getOverallDuration());
            } else {
                this.status.isCanGoToNextStep = false;
            }
        };

        View.prototype.renderDateTimePicker = function () {
            var today = new Date(), selectedDate, storedSelectedDate, options, callbacks;

            if (!this.appointmentId) {
                storedSelectedDate = LocalStorageService.retrieveString(Constants.LOCAL_STORAGE_KEY.SELECTED_DATE);
                if (storedSelectedDate
                    && !this.data.newAppointment.fromTime
                    && !this.data.newAppointment.toTime
                ) {
                    var storedSelectedDate_m = moment(storedSelectedDate, 'YYYY-MM-DD');
                    if (storedSelectedDate_m.isAfter(moment(), 'day')) {
                        this.data.newAppointment.scheduleDate.setDate(storedSelectedDate);
                    }
                }
            }
            selectedDate = this.data.newAppointment.scheduleDate.getDate();

            options = {
                weekdaysShort: this.i18n.weekdayNamesShort,
                today: '',
                clear: '',
                close: '',
                min: today,
                selected: selectedDate
            };
            callbacks = {
                onStop: this.onDatePickerStop.bind(this),
                onStart: this.onDatePickerStart.bind(this),
                onSet: this.onDatesPickerSet.bind(this),
                onChange: this.onDatePickerViewChanged.bind(this)
            };

            DateTimePickerService.renderPicker(options, callbacks);
        };


        View.prototype.onDatePickerStop = function () {
            var self = this;
            setTimeout(function () {
                self.status.isPickerShow = false;
                self.data.newAppointment.scheduleDate.revertDate();
                self.scope.$digest();
            }, 10);

        };

        View.prototype.onDatePickerStart = function () {
            var self = this;

            self.status.isPickerShow = true;
            setTimeout(function () {
                self.scope.$digest();
            }, 10);
        };

        View.prototype.onDatesPickerSet = function () {
            var self = this;
            self.getShopWorkingSchedule();
            self.data.newAppointment.scheduleDate.setDate(DateTimePickerService.getSelectedDate());
            setTimeout(function () {
                self.scope.$digest();
            }, 10);
        };

        View.prototype.onDatePickerViewChanged = function () {
            this.data.newAppointment.scheduleDate.setDate(DateTimePickerService.getViewingMonth('YYYY-MM-DD'), null, true);
            this.scope.$digest();
        };


        function newInstance($scope, newAppointment) {
            return new View($scope, newAppointment);
        }

        return {
            newInstance: newInstance
        };
    }
})(app);