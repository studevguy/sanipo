"use strict";

app.service("TimeGridService", ['$compile', '$templateCache', function ($compile, $templateCache) {
    var self = this;

    var TIME_SCALE = 30;
    var CHECK_CURRENT_PERIOD = 60000;
    var CLASS = {
        TIME_CELL: '.time_cell'
    };
    var ATTR = {
        DATA_VALUE: 'data-value'
    };

    var timeGridInterval, startTime, productDuration, appointmentFromTime, selectedDate, workingTimeFrom,
        workingTimeTo, endTime, tasks;

    this.load = function (elm) {
        this.$timeGrid = $(elm);
    };

    this.render = function (scope,
                            $selectedDate,
                            fromTime,
                            toTime,
                            $workingTimeFrom,
                            $workingTimeTo,
                            $appointmentFromTime,
                            $productDuration,
                            $tasks,
                            timeScale) {
        var time_cell_template = $templateCache.get('../templates/merchant/time_cell_partial.html');
        var startTime_m = moment(fromTime, 'HH:mm');
        var endTime_m = moment(toTime, 'HH:mm');

        var bindTime, $timeCell;

        this.$timeGrid.empty();

        selectedDate = $selectedDate;
        startTime = fromTime;
        endTime = toTime;
        productDuration = $productDuration;
        appointmentFromTime = $appointmentFromTime;
        workingTimeFrom = $workingTimeFrom;
        workingTimeTo = $workingTimeTo;
        tasks = $tasks;
        TIME_SCALE = timeScale;


        //if staff off gray all timecells
        if (!workingTimeFrom || !workingTimeTo) {
            do {
                bindTime = startTime_m.format('HH:mm');
                $timeCell = $(time_cell_template);
                $timeCell.attr('data-value', bindTime);
                $timeCell.find('span').text(bindTime);
                $timeCell.addClass('unavailable');
                $timeCell.removeAttr('ng-click');
                $timeCell.appendTo(this.$timeGrid);
                startTime_m.add(TIME_SCALE, "minutes");
            } while ((startTime_m.isSame(endTime_m) || startTime_m.isBefore(endTime_m)));
        } else {
            do {
                bindTime = startTime_m.format('HH:mm');
                $timeCell = $(time_cell_template);
                $timeCell.attr('data-value', bindTime);
                $timeCell.find('span').text(bindTime);
                if (!this.checkIsInStaffWorkingTime(bindTime)
                    || !this.checkIsInShopWorkingTime(bindTime)
                    || this.checkIsInTasksTime(bindTime)
                ) {
                    $timeCell.addClass('unavailable');
                    $timeCell.removeAttr('ng-click');
                }
                $timeCell.appendTo(this.$timeGrid);
                startTime_m.add(TIME_SCALE, "minutes");
            } while ((startTime_m.isSame(endTime_m) || startTime_m.isBefore(endTime_m)));
        }

        if (appointmentFromTime) {
            if (this.isAppointmentNotPastTime()) {

                if (this.isTimeValid(appointmentFromTime)) {
                    this.highlightSelectedTime();
                }
            }
        }

        //clear interval firstly
        this.__clearInterval();

        //inactive past time if selected date is today
        if (this.__isSelectedDayToday()) {
            this.__inactiveThePastTime();
            this.__startIntervalCheckCurrentTime();
        }

        $compile(this.$timeGrid)(scope);

    };
    this.__isSelectedDayToday = function () {
        var selectedDate_m = moment(selectedDate, 'YYYY-MM-DD');
        return selectedDate_m.isSame(moment(), 'day');
    };

    this.highlightSelectedTime = function ($startTime, $duration) {
        this.__removeSelectedClassFromTimeCells();
        var startTime_m = $startTime
            ? moment($startTime, 'HH:mm')
            : moment(appointmentFromTime, 'HH:mm');
        var endTime_m = $duration
            ? moment($startTime, 'HH:mm').add($duration, 'minutes')
            : moment(appointmentFromTime, 'HH:mm').add(productDuration, 'minutes');
        var bindingTime;
        do {
            bindingTime = startTime_m.format('HH:mm');
            this.__setTimeCellSelected(bindingTime);
            startTime_m.add(TIME_SCALE, "minutes");
        } while (startTime_m.isBefore(endTime_m));
    };

    this.__setTimeCellSelected = function (bindingTime) {
        this.$timeGrid.find(".time_cell[data-value='" + bindingTime + "']").addClass('selected');
    };

    this.__removeSelectedClassFromTimeCells = function () {
        this.$timeGrid.find('.time_cell').removeClass('selected');
    };

    this.checkIsInStaffWorkingTime = function (time) {
        var time_m = moment(time, 'HH:mm');
        var workingTimeFrom_m = moment(workingTimeFrom, 'HH:mm');
        var workingTimeTo_m = moment(workingTimeTo, 'HH:mm').subtract(productDuration, 'minutes');
        if (!workingTimeFrom_m.isValid() || !workingTimeTo_m.isValid()) {
            return false;
        }
        return (time_m.isSame(workingTimeFrom_m) || time_m.isAfter(workingTimeFrom_m))
            && (time_m.isSame(workingTimeTo_m) || time_m.isBefore(workingTimeTo_m));
    };

    this.checkIsInShopWorkingTime = function (time) {
        var time_m = moment(time, 'HH:mm');
        var timeFrom_m = moment(startTime, 'HH:mm');
        var timeTo_m = moment(endTime, 'HH:mm').subtract(productDuration, 'minutes');
        return (time_m.isSame(timeFrom_m) || time_m.isAfter(timeFrom_m))
            && (time_m.isSame(timeTo_m) || time_m.isBefore(timeTo_m));
    };

    this.checkIsInTasksTime = function (time) {
        var result = false;
        var fromTimeConfigured;
        tasks.forEach(function (task) {
            fromTimeConfigured = moment(task.fromTime, 'HH:mm').subtract(productDuration, 'minutes').format('HH:mm');
            result = result || self.__checkInTimeRange(time, fromTimeConfigured, task.toTime);
        });

        return result;
    };

    this.__checkInTimeRange = function (checkedTime, $timeFrom, $timeTo) {
        var time_m = moment(checkedTime, 'HH:mm');
        var timeFrom_m = moment($timeFrom, 'HH:mm');
        var timeTo_m = moment($timeTo, 'HH:mm');
        return (time_m.isAfter(timeFrom_m))
            && (time_m.isBefore(timeTo_m));
    };

    this.isAppointmentNotOverlapTaskTime = function (time) {
        var checkedTime = time || appointmentFromTime;
        var result = true;
        var startTaskTime_m, endTaskTime_m;
        var fromTime_m = moment(checkedTime, 'HH:mm');
        var toTime_m = moment(checkedTime, 'HH:mm').add(productDuration, 'minutes');
        tasks.forEach(function (task) {
            startTaskTime_m = moment(task.fromTime, 'HH:mm');
            endTaskTime_m = moment(task.toTime, 'HH:mm');

            result = result &&
                (
                    //the appointment statTime & endTime all have to be before task start time
                    (fromTime_m.isBefore(startTaskTime_m) && (toTime_m.isBefore(startTaskTime_m) || toTime_m.isSame(startTaskTime_m)))
                        // or he appointment statTime & endTime all have to be after task end time
                    || ((fromTime_m.isAfter(endTaskTime_m) || fromTime_m.isSame(endTaskTime_m)) && (toTime_m.isAfter(endTaskTime_m)))
                )
        });
        return result;
    };

    this.isTimeValid = function ($time) {
        return this.checkIsInStaffWorkingTime($time)
            && this.checkIsInShopWorkingTime($time)
            && this.isAppointmentNotOverlapTaskTime();
    };

    this.isAppointmentFromTimeValid = function () {
        if (!appointmentFromTime) {
            return false;
        }
        return this.isTimeValid(appointmentFromTime);
    };

    this.__startIntervalCheckCurrentTime = function () {
        timeGridInterval = setInterval(function () {
            self.__inactiveThePastTime();
            self.highlightSelectedTime();
        }, CHECK_CURRENT_PERIOD);
    };

    this.__inactiveThePastTime = function () {
        var timeCells = this.__getTimeCells();
        var fromTime, fromTime_m, $timeCell;
        timeCells.each(function () {
            $timeCell = $(this);
            fromTime = $timeCell.attr(ATTR.DATA_VALUE);
            if (!self.__isNotPastTime(fromTime)) {
                $timeCell.addClass('unavailable');
                $timeCell.removeAttr('ng-click');
            }
        });
    };

    this.__getTimeCells = function () {
        return this.$timeGrid.find(CLASS.TIME_CELL);
    };

    this.__clearInterval = function () {
        clearInterval(timeGridInterval);
    };

    this.__isNotPastTime = function (time) {
        if (time) {
            return moment(time, 'HH:mm').isAfter(moment());
        }
        return moment(appointmentFromTime, 'HH:mm').isAfter(moment());
    };

    this.isAppointmentNotPastTime = function () {
        if (!appointmentFromTime) {
            return false;
        }
        return moment(selectedDate + ' ' + appointmentFromTime, 'YYYY-MM-DD HH:mm').isAfter(moment());
    };

}]);


