"use strict";

app.service("AjaxService", ['Configuration',
    function (Configuration) {
        this.ajax = function (params, isNotConfigHeader) {
            if (!('v1.1' == Configuration.VERSION_API_APPOINTMENT) && !isNotConfigHeader ) {
                params.headers = {
                    'auth': Configuration.AUTH
                };
            }

            return Q($.ajax(params));
        }
    }]);

