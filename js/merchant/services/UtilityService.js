"use strict";

app.service("UtilityService",
    ['Unicodes', function (Unicodes) {
        this.formatCurrency = function (num) {
            var p = num.toFixed(2).split(".");
            return p[0].split("").reverse().reduce(function (acc, num, i, orig) {
                return num + (i && !(i % 3) ? "," : "") + acc;
            }, "");
        };


        this.getStatusText = function (status, i18n) {
            if (status == 'CANCEL')
                return i18n.label_status_cancel;

            if (status == 'UNAPPROVED')
                return i18n.label_status_unapproved;

            if (status == 'APPROVED')
                return i18n.label_status_approved;

            if (status == 'ATSTORE')
                return i18n.label_status_atstore;

            return '';
        };

        this.createUUID = function () {
            var s = [];
            var hexDigits = "0123456789abcdef";
            for (var i = 0; i < 36; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
            s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
            s[8] = s[13] = s[18] = s[23] = "-";

            var uuid = s.join("");
            return uuid;
        };

        this.isJapaneseCharacter = function (theString) {

            return this.verifyValidCharacter(theString, Unicodes.japaneseCharacter)
        };

        this.isKatakanaCharacter = function (theString) {

            return this.verifyValidCharacter(theString, Unicodes.katakanaCharacter)
        };

        this.verifyValidCharacter = function (theString, chars) {
            var result = false;
            for (var i = 0; i < theString.length; i++) {
                var theUnicode = theString.charCodeAt(i).toString(16).toUpperCase();
                while (theUnicode.length < 4) {
                    theUnicode = '0' + theUnicode;
                }
                var base16 = parseInt(theUnicode, 16);
                var char = _.find(chars, function (item) {
                    return item == base16;
                });

                if (!char) {
                    if (!result)
                        result = theString.charAt(i);
                    else
                        result = result + "," + theString.charAt(i);

                }
            }
            return result;
        };

        this.convertDateFormat = function (date, fromFormat, toFormat) {
            var date_m = moment(date, fromFormat);
            return date_m.format(toFormat);
        };

        this.calculateTimeDurationByMinute = function (fromTime, toTime) {
            var fromTime_m = moment.duration(fromTime, 'HH:mm');
            var toTime_m = moment.duration(toTime, 'HH:mm');
            return toTime_m.subtract(fromTime_m).asMinutes();
        };

    }]);