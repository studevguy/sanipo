"use strict";

app.service("ShopResourceService", [
    "Configuration",
    "AjaxService",
    'Constants',
    'i18nService',
    function (Configuration,
              AjaxService,
              Constants,
              i18nService) {
        var self = this;
        var API_URL = {
            shopSettingInfo: Configuration.api.shopSettingInfo,
            shopWokingSchedule: Configuration.api.shopWokingInfo,
            updateAppointmentInfo: Configuration.api.updateAppointmentInfo,
            getAppointmentInfoById: Configuration.api.getAppointmentInfoById,
            cancelAppointment: Configuration.api.cancelAppointment,
            sendEmailInformAppointmentChanged: Configuration.api.sendEmailInformAppointmentChanged

        };
        var STATUS_KEY_TRANSLATE = Constants.STATUS_KEY_TRANSLATE;
        var DATE_STATUSES = Constants.DATE_STATUSES;
        var APPOINTMENT_STATUS = Constants.APPOINTMENT_STATUS;
        var LANGUAGE = Constants.LANGUAGE;
        var EMPLOYEE_COLOR_RANGE = Constants.EMPLOYEE_COLOR_RANGE;
        var NO_FILTER = Constants.NO_EMPLOYEE_FILTER;
        var ERROR_CODE = Configuration.ERROR_CODE;
        var SERVER_SUCCESS_CODE = Configuration.SERVER_SUCCESS_CODE;

        var employeeFilter = NO_FILTER;

        this.shopSettingInfos = {};
        this.shopWorkingSchedule = {};
        this.currentAppointment = {};
        this.fromDate = '';
        this.toDate = '';

        this.getShopSettingsInfos = function (params) {
            var shopId = params.shopId;
            var deferred = Q.defer();
            var url = self.__processUrl1(shopId);

            AjaxService.ajax({
                method: "GET",
                url: url
            }).then(function (response) {
                if (SERVER_SUCCESS_CODE == response.status) {
                    _.assign(self.shopSettingInfos, response);

                    self.__translateAppointmentStatus();

                    self.__generateEmployeeColor();

                    deferred.resolve(self.shopSettingInfos);
                } else {
                    deferred.reject(response);
                }

            }, function (error) {

            });

            return deferred.promise;
        };

        this.getShopWorkingSchedule = function (shopId, fromDate, toDate) {
            this.fromDate = moment(fromDate, 'YYYY/MM/DD');
            this.toDate = moment(toDate, 'YYYY/MM/DD');
            var deferred = Q.defer();
            var url = self.__processUrl2(shopId, fromDate, toDate);

            AjaxService.ajax({
                method: "GET",
                url: url
            }).then(function (response) {
                if (SERVER_SUCCESS_CODE == response.status) {
                    self.shopWorkingSchedule = response;
                    self.__transferTimeStringToUnixType();
                    //self.__getEmployeeDetailsForAppointment();
                    deferred.resolve(self.shopWorkingSchedule);
                } else {
                    deferred.reject(response.status);
                }
            }, function (error) {

            });

            return deferred.promise;
        };

        this.getAppointmentInfoById = function (appointmentId) {

            var deferred = Q.defer();
            var url = API_URL.getAppointmentInfoById + appointmentId;

            AjaxService.ajax({
                method: "GET",
                url: url
            }).then(function (response) {
                if (SERVER_SUCCESS_CODE == response.status) {
                    self.currentAppointment = response.appointmentInfo;
                    console.log("Current appointment information: ", response.appointmentInfo);
                    deferred.resolve(response);
                } else {
                    deferred.reject(response.status);
                }

            }, function (error) {

            });

            return deferred.promise;
        };


        this.updateAppointmentInfo = function (dataInfo) {

            var deferred = Q.defer();
            var url = API_URL.updateAppointmentInfo;

            AjaxService.ajax({
                method: "POST",
                url: url,
                data: dataInfo,
                contentType: "application/json"
            }).then(function (response) {
                if (SERVER_SUCCESS_CODE == response.status) {
                    deferred.resolve(response);
                } else {
                    deferred.reject(response.status);
                }
            }, function (error) {

            });

            return deferred.promise;
        };

        this.cancelAppointment = function (id) {

            var deferred = Q.defer();
            var url = API_URL.cancelAppointment + id;
            AjaxService.ajax({
                method: "POST",
                url: url,
                contentType: "application/json"
            }).then(function (response) {
                if (SERVER_SUCCESS_CODE == response.status) {
                    deferred.resolve(response);
                } else {
                    deferred.reject(response.status);
                }
            }, function (error) {

            });

            return deferred.promise;
        };

        this.sendEmailInformAppointmentChanged = function (appointmentInfo) {
            var url = API_URL.sendEmailInformAppointmentChanged;
            AjaxService.ajax({
                method: "POST",
                url: url,
                data:JSON.stringify(appointmentInfo),
                contentType: "application/json"
            })
        };

        this.createEmployeeViewModel = function (productId) {
            var self = this;
            var employees = [];
            var enable = true;
            var showRequire = true;
            var requireValue = enable;
            var emp = {};
            self.shopSettingInfos.employees.forEach(function (employee) {
                if (employee.display) {
                    enable = self
                        .isEmployeeAndProductHaveRelation(employee.id, productId);
                    enable = enable && (employee.acceptableSlot > 0);
                    //check employee working on this date
                    showRequire = enable && (employee.allowRequestStaff == 1);
                    requireValue = false;
                    emp = {
                        id: employee.id,
                        fullName: employee.fullName,
                        requireValue: requireValue,
                        showRequire: showRequire,
                        enable: enable
                    };
                    employees.push(emp);

                }
            });
            return employees;
        };

        this.getEnableProductList = function (empId) {
            var self = this;
            var temp = [];

            self.shopSettingInfos.products.forEach(function (product) {
                if (self.isEmployeeAndProductHaveRelation(empId, product.id)) {
                    temp.push(product.id);
                }
            });

            return temp;

        };

        this.updateProductsData = function (empId) {
            var self = this;
            var enable = true;
            self.shopSettingInfos.products.forEach(function (product) {
                enable = self
                    .isEmployeeAndProductHaveRelation(empId, product.id);
                //check employee working on this date
                product.enable = enable;
            });
        };

        this.isEmployeeAndProductHaveRelation = function (empId, proId) {
            if (!proId) {
                return true;
            }
            var res = false;
            this.shopSettingInfos.employeeProductRelations.forEach(function (relation) {
                if (empId == relation.employeeId && proId == relation.productId) {
                    res = true;
                    return true;
                }
            });

            return res;
        };

        this.setEmployeeFilter = function (employeeId) {
            employeeFilter = employeeId ? employeeId : NO_FILTER;
        };

        this.getAppointmentsPerDayList = function () {
            var self = this;
            var dayList = this.__generateWeekDayList();
            var tempDate;
            var appointmentData;

            //check whether to apply filter by employee
            if (!employeeFilter || NO_FILTER === employeeFilter) {
                appointmentData = self.shopWorkingSchedule.appointments;
            } else {
                appointmentData = _.filter(self.shopWorkingSchedule.appointments, function (appt) {
                    return appt.employees[0].id == employeeFilter;
                });
            }

            //decorate display color for employee
            appointmentData.forEach(function (appt) {
                appt.employees.forEach(function (emp) {
                    emp.color = self.__getEmployeeDisplayColor(emp.id);
                });
            });

            //populate appointment for date
            dayList.forEach(function (item) {
                appointmentData.forEach(function (appt) {
                    tempDate = moment(appt.scheduleDate, 'YYYY/MM/DD').format('MMMM DD');
                    if (tempDate == item.dateOfList) {
                        item.appointmentsList.push(appt);
                    }
                });
            });

            //decorate dayList date
            var i = 0;
            do {
                if (dayList[i].status == DATE_STATUSES.NO_SCHEDULE_SETTING) {
                    if (!dayList[i + 1].appointmentsList.length && dayList[i + 1].status == DATE_STATUSES.NO_SCHEDULE_SETTING) {
                        dayList[i + 1].startPreviousEmptyDate = dayList[i].startPreviousEmptyDate;
                        dayList[i + 1].startPreviousEmptyDate_t = dayList[i].startPreviousEmptyDate_t;
                        dayList.splice(i, 1);
                        i--;
                    }
                }
                i++;
                //else if (!dayList[i].appointmentsList.length && dayList[i].status == DATE_STATUSES.WORKING) {
                //    if (!dayList[i + 1].appointmentsList.length && dayList[i + 1].status == DATE_STATUSES.WORKING) {
                //        dayList[i + 1].startPreviousEmptyDate = dayList[i].startPreviousEmptyDate;
                //        dayList[i + 1].startPreviousEmptyDate_t = dayList[i].startPreviousEmptyDate_t;
                //        dayList.splice(i, 1);
                //        i--;
                //    }
                //}
                //else if (dayList[i].status == DATE_STATUSES.DATE_OFF) {
                //    if (!dayList[i + 1].appointmentsList.length && dayList[i + 1].status == DATE_STATUSES.DATE_OFF) {
                //        dayList[i + 1].startPreviousEmptyDate = dayList[i].startPreviousEmptyDate;
                //        dayList[i + 1].startPreviousEmptyDate_t = dayList[i].startPreviousEmptyDate_t;
                //        dayList.splice(i, 1);
                //        i--;
                //    }
                //}
            } while (i < dayList.length - 1);

            //sort schedule by fromTime && toTime
            dayList.forEach(function (item) {
                item.appointmentsList = _.sortByOrder(item.appointmentsList, ['fromTime_unix', 'toTime_unix'], [true, false]);
            });


            return dayList;
        };

        this.getEmployeeTasksByEmployee = function (employeeId) {
            return _.filter(this.shopWorkingSchedule.taskInfos, function (item) {
                return item.employeeId == employeeId;
            });
        };

        this.getTimeRange = function () {
            if (!this.shopWorkingSchedule.shopWorkingTimes.length) {
                return {};
            }
            return {
                fromTime: this.shopWorkingSchedule.shopWorkingTimes[0].fromTime,
                toTime: this.shopWorkingSchedule.shopWorkingTimes[0].toTime
            }
        };

        this.getEmployeeWorkingTime = function (id) {
            return _.find(this.shopWorkingSchedule.employeeWorkingTimes, function (em) {
                return em.employeeId == id;
            })
        };

        this.__findServiceById = function (productId) {
            var product = _.find(self.shopSettingInfos.products, function (item) {
                return item.id == productId;
            });

            return product;
        };
        this.__findAppointmentStatusByValue = function (value) {
            var status = _.find(self.shopSettingInfos.appointmentStatuses, function (item) {
                return item.code == value || item.name == value;
            });

            return status;
        };
        this.__findEmployeeById = function (employeeId) {
            var product = _.find(self.shopSettingInfos.employees, function (item) {
                return item.id == employeeId;
            });

            return product;
        };

        this.__processUrl1 = function (shopId) {
            return API_URL.shopSettingInfo + "?shopId=" + shopId;
        };

        this.__processUrl2 = function (shopId, fromDate, toDate) {
            return API_URL.shopWokingSchedule + "?shopId=" + shopId + "&fromDate=" + fromDate + "&toDate=" + toDate;
        };

        this.__generateWeekDayList = function () {
            var dayList = [];
            var date, dateFormat;
            var today = moment();
            var tommorow = moment().add(1, 'days');
            var yesterday = moment().subtract(1, 'days');
            var fromDate = this.fromDate.clone();
            var toDate = this.toDate.clone();

            if (moment.locale() == LANGUAGE.JAPANESE) {
                dateFormat = 'MMMMDD' + i18nService.translate('common', 'text_date');
            } else {
                dateFormat = 'MMMM DD';
            }

            for (var i = fromDate; (i.isBefore(toDate)) || (i.isSame(toDate)); i.add(1, "days")) {
                date = {
                    monthOfList_t: moment(i).format("MMMM"),
                    dateOfList_t: moment(i).format(dateFormat),
                    dateOfList: moment(i).format("MMMM DD"),
                    dateOfList_ISO: moment(i).format("YYYY-MM-DD"),
                    startPreviousEmptyDate: moment(i).format("MMMM DD"),
                    startPreviousEmptyDate_t: moment(i).format(dateFormat),
                    status: this.__getDateStatus(i.format('YYYY/MM/DD')),
                    appointmentsList: []
                };

                if (i.isSame(today, 'day')) {
                    date.isToday = true;
                } else if (i.isSame(tommorow, 'day')) {
                    date.isTomorrow = true;
                } else if (i.isSame(yesterday, 'day')) {
                    date.isYesterday = true
                }
                dayList.push(date);
            }
            return dayList;
        };

        this.__getDateStatus = function (date) {
            var scheduleDate = _.find(this.shopWorkingSchedule.shopWorkingTimes, function (item) {
                return item.date == date;
            });
            if (scheduleDate) {
                return scheduleDate.status;
            }
            return DATE_STATUSES.NO_SCHEDULE_SETTING;

        };

        this.__translateAppointmentStatus = function () {
            var key, translatedText;
            this.shopSettingInfos.appointmentStatuses.forEach(function (item) {
                key = STATUS_KEY_TRANSLATE[item.name];
                translatedText = i18nService.translate('common', key);
                item.name_t = translatedText ? translatedText : item.name;
            });
        };

        this.getChannelBySpecialType = function (specialType) {
            return _.find(self.shopSettingInfos.bookingChannelInfos, function (item) {
                return item.specialType == specialType;
            })
        };

        this.__hasAlwaysApproveAppointmentSetting = function () {
            return this.shopSettingInfos.shop.tabletAppointmentStatus == APPOINTMENT_STATUS.APPROVED
                && this.shopSettingInfos.shop.webAppointmnetStatus == APPOINTMENT_STATUS.APPROVED;
        };

        this.configStatusesOptionForAppointment = function () {
            if (this.__hasAlwaysApproveAppointmentSetting()) {
                var status = _.find(this.shopSettingInfos.appointmentStatuses, function (item) {
                    return item.name == 'APPROVED';
                });

                status.disable = true;

                return true;
            } else {
                return false;
            }
        };

        this.__transferTimeStringToUnixType = function () {
            this.shopWorkingSchedule.appointments.forEach(function (item) {
                item.fromTime_unix = moment(item.fromTime, 'HH:mm').unix();
                item.toTime_unix = moment(item.toTime, 'HH:mm').unix();
            });
        };

        this.isNoSchedule = function () {
            return this.shopWorkingSchedule.shopWorkingTimes.length == 0 || this.shopWorkingSchedule.employeeWorkingTimes.length == 0;
        };

        this.isDayOff = function () {
            if (this.shopWorkingSchedule.shopWorkingTimes.length == 0) {
                return false;
            }
            return this.shopWorkingSchedule.shopWorkingTimes[0].status == DATE_STATUSES.DATE_OFF;
        };

        this.__generateEmployeeColor = function () {
            var employees = this.shopSettingInfos.employees;
            var colorRangeLength = EMPLOYEE_COLOR_RANGE.length;
            var index;
            for (var i = 0; i < employees.length; i++) {

                //to repeat the color of employee
                index = i % (colorRangeLength);
                employees[i].color = EMPLOYEE_COLOR_RANGE[index];
            }
        };

        this.__getEmployeeDetailsForAppointment = function () {
            var empId, emp;
            this.shopWorkingSchedule.appointments.forEach(function (appt) {
                empId = appt.details[0].employeeId;
                emp = self.__findEmployeeById(empId);
                if (emp) {
                    appt.details[0].name = emp.fullName;
                    appt.details[0].color = emp.color;
                }
            })
        };

        this.getEmployeeList = function () {
            return self.shopSettingInfos.employees;
        };

        this.getEmployeeById = function (id) {
            if (-1 == id) {
                return {};
            }
            var emp = _.filter(this.shopSettingInfos.employees, function (item) {
                return id == item.id;
            });
            return emp[0];
        };

        this.getCurrentEmployee = function () {
            return this.getEmployeeById(employeeFilter);
        };

        this.__getEmployeeDisplayColor = function (id) {
            var emp = _.find(this.shopSettingInfos.employees, function (item) {
                return id == item.id;
            });

            return emp.color;
        };

        this.getChannelName = function (channelCode) {
            if (!channelCode) {
                return null;
            }
            var channel = _.filter(this.shopSettingInfos.bookingChannelInfos, function (chal) {
                return channelCode == chal.specialType;
            });

            return channel[0].channelName;
        };

    }]);