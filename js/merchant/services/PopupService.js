"use strict";

app.service("PopupService", ['$compile', '$templateCache', function ($compile, $templateCache) {
    var self = this;
    this.contentSize = null;
    this.offCallMeFn = function () {
    };
    this.popupLink = "a.popup";
    this.popupBack = '.popup_back';
    this.popupContainer = '.popup_cont';
    this.popupLinkElm = {};
    this.popup = {};
    this.showCallBack = function () {
    };
    this.closeCallback = function () {
    };

    /**
     *
     * @param $options = {
     *              templateUrl: "",
     *              size: "",
     *              closeCallback: func,
     *              showCallback: func
     *              }
     * @param afterElement = {}
     * @param scope
     */
    this.open = function ($options, scope) {
        var templateUrl = $options.templateUrl;
        if (!templateUrl) {
            templateUrl = "templates/popup.html";
        }

        if ($options.closeCallback) {
            this.closeCallback = $options.closeCallback;
        } else {
            this.closeCallback = function () {
            }
        }
        if ($options.showCallback) {
            this.showCallBack = $options.showCallback;
        } else {
            this.showCallBack = function () {
            }
        }

        var html = $templateCache.get(templateUrl);
        var content = $compile(html)(scope);
        scope.$evalAsync();

        var options = {
            modal: $options.modal,
            afterClose: this.closeHandler.bind(this),
            show: function (popup) {
                self.showHandler(popup, this, scope)
            },
            backOpacity: $options.notShowOverlay ? 0 : 0.5
        };

        this.__addPopupLinkElement();

        this.__setContentAppearance($options.size);

        this.__configNavigationBehaviour(scope);

        this.popup = new $.Popup(options);

        this.popup.open(content, 'html', this.popupLinkElm);
    };

    this.__setContentAppearance = function (size) {
        if (size) {
            this.contentSize = {};
            if (size.top) {
                this.contentSize.top = size.top;
            }

            if (size.left) {
                this.contentSize.left = size.left;
            }

            if (size.right) {
                this.contentSize.right = size.right;
            }

            if (size.height) {
                this.contentSize.height = size.height;
            }

        } else {
            this.contentSize = null
        }
    };

    this.__addPopupLinkElement = function () {
        this.popupLinkElm = $("<a>", {
            class: "popup",
            href: ''
        });

        this.popupLinkElm.appendTo('body');
    };

    this.close = function (callback) {
        if (callback) {
            if (typeof (callback) == 'function') {
                this.closeCallback = callback;
            }
        }
        this.popup.close();
    };

    this.closeHandler = function () {
        this.enableDocumentScrollBar();
        this.offCallMeFn();
        this.__cleanUp();
        this.closeCallback();
    };

    this.__cleanUp = function () {
        $(document).off('click.popup');
        $(this.popupLink).each(function (index, item) {
            $(item).remove();
        });
    };

    this.showHandler = function (popup, plugin, scope) {
        this.__configClickBubling();
        this.disableDocumentScrollBar();
        this.showCallBack(popup);
        var cssconfig = this.__getContentAppearance(plugin);
        popup.css(cssconfig);
        try {
            popup[0].children[0].style.overflowY = "auto";
            popup[0].children[1].style.position = "absolute";
            popup[0].children[1].style.right = "30px;";
            popup[0].children[1].style.top = "20px;";
        } catch (err) {
        }
        plugin.o.afterOpen.call(plugin);
    };

    this.__getContentAppearance = function (plugin) {
        var cssConfig;
        if (this.contentSize) {
            cssConfig = {
                top: this.contentSize.top,
                height: this.contentSize.height,
                left: 0,
                right: 0,
                opacity: 1
            }
        } else {
            cssConfig = {
                "width": "450px",
                "text-align": "center",
                "top": plugin.getCenter().top,
                "left": plugin.getCenter().left,
                "opacity": 1
            };
        }
        return cssConfig;
    };

    this.disableDocumentScrollBar = function () {
        $("html").css("overflow-y", "hidden");
    };

    this.enableDocumentScrollBar = function () {
        $("html").css("overflow-y", "auto");
    };

    this.__configNavigationBehaviour = function (scope) {
        this.offCallMeFn = scope.$on("$locationChangeStart", function (event, newUrl, oldUrl) {
            event.preventDefault();
            self.close();
        });
    };

    this.__configClickBubling = function(){
        $(this.popupBack).click(function ($event) {
            $event.stopPropagation();
        });

        $(this.popupContainer).click(function ($event) {
            $event.stopPropagation();
        });
    }
}]);