"use strict";

app.service("AppointmentResourceService", ["Configuration", "AjaxService", function (Configuration, AjaxService) {
    var self = this;
    var API_URL = {
        getShopScheduleDetails: Configuration.api.getShopScheduleDetails,
        bookNewAppointment: Configuration.api.bookNewAppointment,
        updateAppointment:  Configuration.api.updateAppointment,
        getAllAppointments: Configuration.api.getAllAppointments

    };


    this.data = {};
    this.ANY_ONE_ID = '00000000-0000-0000-0000-000000000000';
    this.getAllAppointments = function (params, callBack) {
        var email = params.email;
        var customerId = params.customerId;
        var deferred = Q.defer();
        var url = API_URL.getAllAppointments ;

        if(customerId && customerId!='')
            url+='customerId='+customerId;
        else if(email && email!='')
            url+='email='+email;


        console.log(url);
        AjaxService.ajax({
            method: "GET",
            url: url
        }).then(function (data) {
            console.log(data);
            self.data = data;
            deferred.resolve(data);
            if (callBack)
                callBack(data);

        }, function (error) {

        });

        return deferred.promise;
    };
    this.getShopScheduleDetails = function (params, callBack) {
        var shopId = params.shopId;
        var date = params.date;
        var appointmentId = params.appointmentId;
        var deferred = Q.defer();
        var url = API_URL.getShopScheduleDetails + shopId + "&queryDate=" + date;
        if(appointmentId)
            url +="&appointmentId="+appointmentId;

        console.log(url);
        AjaxService.ajax({
            method: "GET",
            url: url
        }).then(function (data) {
            console.log(data);
            self.data = data;
            deferred.resolve(data);
            if (callBack)
                callBack(data);

        }, function (error) {

        });

        return deferred.promise;
    };

    this.bookNewAppointment = function (dataInfo, callBack) {

        var deferred = Q.defer();
        var url = API_URL.bookNewAppointment;

        AjaxService.ajax({
            method: "POST",
            url: url,
            data: dataInfo,
            contentType: "application/json"
        }).then(function (response) {
            deferred.resolve(response);
            if (callBack)
                callBack(response);

        }, function (error) {

        });

        return deferred.promise;
    };
    this.updateAppointment = function (dataInfo, callBack) {

        var deferred = Q.defer();
        var url = API_URL.updateAppointment;

        AjaxService.ajax({
            method: "POST",
            url: url,
            data: dataInfo,
            contentType: "application/json"
        }).then(function (response) {
            deferred.resolve(response);
            if (callBack)
                callBack(response);

        }, function (error) {

        });

        return deferred.promise;
    };

    this.isEmployeeAndProductHaveRelation = function (empId, proId) {
        var res = false;
        this.data.employeeProductRelations.forEach(function (relation) {
            if (empId == relation.employeeId && proId == relation.productId) {
                res = true;
                return true;
            }
        });

        return res;
    };


    this.getBookeTimeskOfAnyOne = function (shopInfo,employeeWorkingTimes) {
        var response = [];
        if (shopInfo.dayOff)
        {
            return response;
        }

        var i = 0;
        var fromTime;
        var toTime;
        var timeBlock = shopInfo.timeBlock;

        var startTime = moment(shopInfo.startTime, 'HH:mm');
        var endTime = moment(shopInfo.endTime, 'HH:mm');

        fromTime = startTime;
        while(fromTime.isBefore(endTime))
        {
            toTime = fromTime.clone();
            toTime.add(timeBlock, "minutes");
            if(endTime.isBefore(toTime))
                toTime=endTime

            var tmp = self.getFirstEmployeeCanBookOnTimes(employeeWorkingTimes, fromTime.format("HH:mm"), toTime.format("HH:mm"));
            if (!tmp)
            {
                var bookedTime = {fromTime: fromTime.format("HH:mm"), toTime: toTime.format("HH:mm")};
                response.push(bookedTime);
            }
            fromTime.add(timeBlock, "minutes");
        }
        return response;
    }

    this.getBookeTimesRange = function (shopInfo,employeeWorkingTimes) {
        var response = [shopInfo.startTime,shopInfo.endTime];
        employeeWorkingTimes.forEach(function (employee) {

            if (!employee.dayOff && !employee.disable && employee.id != self.ANY_ONE_ID) {
                var timesemployee = [employee.fromTime, employee.toTime];
                response = _.union(response, timesemployee);
                employee.booktimes.forEach(function (item) {
                    var times = [item.fromTime, item.toTime];
                    response = _.union(response, times);
                });
            }
        });
        response = _.sortBy(response, function (time) {
            return time;
        });
        return response;
    }


    this.getFirstEmployeeCanBookOnTimes = function (employeeWorkingTimes, fromTime, toTime) {
        var result= false;
        employeeWorkingTimes.forEach(function (item) {
            if (!item.dayOff && !item.disable && item.id != self.ANY_ONE_ID) {
                var canBookOnTimes = self.isEmployeeCanBookOnTimes(item, fromTime, toTime);
                if (canBookOnTimes)
                {
                    result =item;
                    return result;
                }
            }
        });
        return result;
    }

    this.isTimesIntersect = function (fromTime, toTime, fromTime1, toTime1) {
        if (fromTime >= fromTime1 && fromTime < toTime1) {

            return true;
        }

        if (toTime > fromTime1 && toTime <= toTime1) {

            return true;
        }

        if (fromTime <= fromTime1 && toTime > fromTime1) {

            return true;
        }

        if (fromTime <toTime1 && toTime >= toTime1) {

            return true;
        }

        return false;
    }

    this.isEmployeeCanBookOnTimes = function (employee, fromTime, toTime,appointmentTime) {

        var result = true;
        var staffWorkingFromTime = employee.fromTime;
        var staffWorkingToTime = employee.toTime;
        if (staffWorkingFromTime > fromTime || staffWorkingToTime <= fromTime) {
            return false;
        }
        if (staffWorkingFromTime >= toTime || staffWorkingToTime < toTime) {
            return false;
        }
        employee.booktimes.forEach(function (item) {
            if(self.isTimesIntersect(fromTime, toTime, item.fromTime, item.toTime)){
                result = false;
                return result;
            }

        });

        if(appointmentTime){
            appointmentTime.forEach(function (item) {
                if(self.isTimesIntersect(fromTime, toTime, item.timeFrom, item.timeTo)){
                    result = false;
                    return result;
                }
            });
        }

        return result;
    }




}]);