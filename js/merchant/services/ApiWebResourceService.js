"use strict";

app.service("ApiWebResourceService", ["Configuration","AjaxService", function (Configuration, AjaxService) {
    var self = this;
    var API_URL = {
        findShopInfoList :Configuration.api.findShopInfoList,
        authenticate: Configuration.api.authenticate,
        getRegistrationInfo: Configuration.api.getRegistrationInfo,

    };

    this.getRegistrationInfo = function (params, callBack) {
        var shopId = params.shopId;
        var customerId = params.customerId;
        var deferred = Q.defer();
        var url = API_URL.getRegistrationInfo+"&customerId="+customerId+"&shopId="+shopId;
        console.log(url);
        AjaxService.ajax({
            method: "GET",
            url: url
        }).then(function (data) {
            deferred.resolve(data);
            console.log(data);
            if (callBack)
                callBack(data);
        }, function (error) {

        });

        return deferred.promise;
    };
    this.findShopInfoList = function () {
        var deferred = Q.defer();
        var url = API_URL.findShopInfoList+"&appointmentEnabled=true&onlineEnabledType=2&pageSize=100";
        console.log(url);
        AjaxService.ajax({
            method: "GET",
            url: url
        }).then(function (data) {
            deferred.resolve(data);
        }, function (error) {

        });

        return deferred.promise;
    };
    this.authenticate = function (authData,callBack) {

        var deferred = Q.defer();
        var url =  API_URL.authenticate    + authData;
        AjaxService.ajax({
            method: "GET",
            url: url
        }, true).then(function (data) {
            deferred.resolve(data);
            if (callBack)
                callBack(data);
        }, function (error) {

        });

        return deferred.promise;
    };
}]);