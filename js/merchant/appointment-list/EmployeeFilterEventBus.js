/**
 * Created by sangdao on 12/7/15.
 */
"use strict";

(function (app) {
    app.service("EmployeeFilterEventBus", [EmployeeFilterEventBus]);

    function EmployeeFilterEventBus() {
        var self = this;
        this.selectSignal = 'select';
        this.channel = postal.channel();
        this.registeredSubriptions = [];

        this.publishSelectingEvent = function (data) {
            this.channel.publish(this.selectSignal, data);
        };

        /**
         * register function to execute when receiving signal
         * @param {string} subscriber The name to register and use for unsubscription
         * @param callback
         * @returns {Number} the index of subscription
         */
        this.subscribeSelectingEvent = function (subscriber, callback) {
            var subscription = this.channel.subscribe(this.selectSignal, function (data) {
                console.log('Receive employee selecting event; Data: ', data);
                callback(data);
            });
            subscription.subscriber = subscriber;
            return this.registeredSubriptions.push(subscription);
        };

        /**
         * register function to execute once when receiving signal
         * @param callback
         */
        this.subscribeSelectingEventOnce = function (callback) {
            var subscription = this.channel.subscribe(this.selectSignal, function (data) {
                console.log('Receive employee selecting event; Data: ', data);
                callback(data);
                subscription.unsubscribe();
            });
        };

        this.unsubscribeAll = function () {
            this.registeredSubriptions.forEach(function (sub) {
                sub.unsubscribe();
            });
            this.registeredSubriptions.clear();
        };

        this.unsubscribeByName = function (subscriber) {
            this.registeredSubriptions.forEach(function (sub) {
                if (subscriber == sub.subscriber) {
                    sub.unsubscribe();
                }
            });

            _.remove(this.registeredSubriptions, function (sub) {
                return subscriber == sub.subscriber;
            })
        }
    }
})(app);