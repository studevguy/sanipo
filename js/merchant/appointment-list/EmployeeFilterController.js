"use strict";

(function (app) {
    app.controller("EmployeeFilterController", [
        '$scope',
        'EmployeeFilterView',
        EmployeeFilterController]);

    function EmployeeFilterController($scope,
                                      EmployeeFilterView) {
        var view = EmployeeFilterView.newInstance($scope);
        view.show();
    }
})(app);