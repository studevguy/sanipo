/**
 * Created by tiger on 03/07/2015.
 */
"use strict";

app.directive('ngContentSize', [function () {
    return {
        restrict: 'A',
        scope: false,
        compile: function (scope, elm, attr) {
            function postLink(scope, elm, attr) {
                var trigger = attr.trigger;
                var $bodyHeader = $('.body_header');
                var $scrollWrapper = $('.scroll_wrapper');
                var $date_anchor_sticky = $('.date_anchor_sticky');
                var $weekDays = $('.week_days');
                var bodyHeaderHeight, stickyHeight;
                scope.$watch(trigger, function (newVal, oldVal) {
                    bodyHeaderHeight = $bodyHeader.outerHeight();
                    stickyHeight = $date_anchor_sticky.height();
                    $scrollWrapper.css({
                        'top': bodyHeaderHeight + 'px'
                    });
                })
            }

            return {
                post: postLink
            }

        }
    }
}]);