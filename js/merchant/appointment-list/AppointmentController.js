'use strict';
app.controller('AppointmentController',
    ['$scope',
        '$location',
        'Constants',
        'WeekdaysService',
        'ShopResourceService',
        'LocalStorageService',
        'DateTimePickerService',
        'InfoStickyService',
        'PopupService',
        'i18nService',
        'Configuration',
        'Sidebar',
        'EmployeeFilterEventBus',
        'AppointmentEventBus',
        '$route',
        'UtilityService',
        function ($scope,
                  $location,
                  Constants,
                  WeekdaysService,
                  ShopResourceService,
                  localStorageService,
                  dateTimePickerService,
                  infoStickyService,
                  PopupService,
                  i18nService,
                  Configuration,
                  Sidebar,
                  EmployeeFilterEventBus,
                  AppointmentEventBus,
                  $route,
                  UtilityService) {
            var i18n = {};
            var data = {};
            var flags = {};
            var fn = {};
            var shopId;

            $scope.data = data;
            $scope.fn = fn;
            $scope.i18n = i18n;
            $scope.flags = flags;
            $scope.APPOINTMENT_CODE = Configuration.APPOINTMENT_CODE;

            fn.goToThisWeek = function () {
                if (!infoStickyService.isScrolling()) {
                    dateTimePickerService.setSelectedDate(moment().format('YYYY-MM-DD'));
                    if (dateTimePickerService.isPickerStarted()) {
                        dateTimePickerService.stopPicker();
                    }
                }
            };

            fn.navigateToAddNewAppointment = function () {
                if (infoStickyService.isScrolling()) {
                    return false;
                }
                $location.path('/appointment/create');
            };

            fn.navigateToEditAppointment = function (id) {
                if (infoStickyService.isScrolling()) {
                    return false;
                }
                $location.path('/appointment/edit/' + id);
            };

            fn.togglePicker = function () {
                dateTimePickerService.togglePicker();
            };

            fn.stopPicker = function () {
                $scope.isPickerShow = false;
                dateTimePickerService.stopPicker();
            };

            fn.startPicker = function () {
                $scope.isPickerShow = true;
                dateTimePickerService.startPicker();
            };

            fn.scrollToDate = function (event) {
                var $target = $(event.currentTarget);
                var selectedDate = $target.attr('data-date');
                scrollToDate(selectedDate);
            };

            fn.showCancelButton = function (event) {
                var target = event.currentTarget;
                showCancelButton(target);
            };

            fn.hideCancelButton = function (event) {
                var target = event.currentTarget;
                hideCancelButton(target);
            };

            fn.showCancelPopup = function ($event, dayOfList, id, toTime, appt) {
                var appointmentTime = moment(dayOfList + ' ' + toTime, "MMMM DD HH:mm");
                var target = $event.currentTarget;
                if (appointmentTime.isBefore(moment())) {
                    toastr.warning(i18n.msg_appointment_cannot_canceled);
                    hideCancelButton(target);
                    return false;
                }

                var products = [];
                appt.products.forEach(function (prod) {
                    var tempProd = {};
                    tempProd.id = prod.id;
                    tempProd.name = prod.productName;
                    tempProd.duration = prod.duration.toString();
                    tempProd.price = prod.price.toString();
                    products.push(tempProd);
                });

                var canceledAppointment = {
                    appointmentId: appt.id,
                    dayOfList: dayOfList,
                    confirmType: Constants.EMAIL_CONFIRM_TYPE.CONFIRM_TYPE_CANCELED,
                    bookingDate: UtilityService.convertDateFormat(appt.scheduleDate, 'YYYY/MM/DD', 'YYYY-MM-DD'),
                    startTime: appt.fromTime,
                    notes: appt.notes,
                    duration: UtilityService.calculateTimeDurationByMinute(appt.fromTime, appt.toTime),
                    bookerName: appt.bookerInfo.fullName || appt.bookerInfo.displayName || appt.bookerInfo.shopperCode,
                    emailAddress: appt.bookerInfo.emailAddress,
                    mobilePhone: appt.bookerInfo.phoneNumber,
                    shopId: appt.shopId,
                    shopName: localStorageService.retrieveString(Constants.LOCAL_STORAGE_KEY.SHOP_NAME),
                    shopTel: localStorageService.retrieveString(Constants.LOCAL_STORAGE_KEY.SHOP_PHONE),
                    employeeId: appt.employees[0].id,
                    employeeName: appt.employees[0].fullName,
                    requestStaff: appt.employees[0].employeeRequested,
                    productName: products[0].name,
                    productId: products[0].id,
                    productJson: JSON.stringify(products),
                    channel: appt.channel
                };

                var newScope = $scope.$new();
                newScope.canceledAppointment = canceledAppointment;

                var options = {
                    templateUrl: '../templates/merchant/cancel_popup.html',
                    size: {
                        top: '30%',
                        height: '250px'
                    },
                    modal: true
                };

                PopupService.open(options, newScope);
            };

            fn.showSidebar = function () {
                Sidebar.open($scope, data.currentEmployee, '../templates/merchant/employee_filter.html');
            };

            fn.closePopup = function () {
                PopupService.close();
            };

            fn.signout = function () {
                PopupService.close(performSignoutTask);
            };

            getLanguage();
            retrieveShopId();
            renderWeekDay();
            renderDateTimePicker();
            registerInfoStickyScrollEvent();
            getShopSettingsInfos();
            registerEvents();

            function getShopSettingsInfos() {
                ShopResourceService.getShopSettingsInfos({shopId: shopId})
                    .then(function () {
                        getShopWorkingSchedule();
                    }, showError);
            }

            function getShopWorkingSchedule() {
                var startShowingDay = WeekdaysService.getStartShowingDay("YYYY/MM/DD");
                var endShowingDay = WeekdaysService.getEndShowingDay("YYYY/MM/DD");
                ShopResourceService.getShopWorkingSchedule(shopId, startShowingDay, endShowingDay)
                    .then(showTask, showError);
            }

            function showTask() {
                var storedSelectedDate = getStoredSelectedDate();
                WeekdaysService.highlightActiveDate(storedSelectedDate);
                infoStickyService.setFirstScrollDate(storedSelectedDate);
                data.appointmentPerDayList = ShopResourceService.getAppointmentsPerDayList();
                data.currentEmployee = ShopResourceService.getCurrentEmployee();
                $scope.$digest();
            }


            function retrieveShopId() {
                shopId = localStorageService.retrieve(Constants.SHOP_ID);
                if (!shopId) {
                    $location.path('/login');
                }
            }

            function renderWeekDay() {
                var selectedDate = getStoredSelectedDate();
                var options = {
                    selectedDate: selectedDate,
                    onWeekDayChanged: weekdayChangedCallback
                };
                WeekdaysService.renderWeekDays($scope, options);
            }

            function weekdayChangedCallback() {
                getShopSettingsInfos();
                setTimeout(function () {
                    $scope.$digest();
                }, 0)
            }

            function renderDateTimePicker() {
                var options = {
                    weekdaysShort: i18nService.getLanguage('common').weekdayNamesShort,
                    today: '',
                    clear: '',
                    close: '',
                    closeOnDateSelection: true
                };

                var callbacks = {
                    onStart: datePickerStartCallback,
                    onStop: datePickerStopCallback,
                    onSet: datePickerDateSetCallback,
                    onChange: datePickerViewChangeCallback
                };
                dateTimePickerService.renderPicker(options, callbacks);
            }

            function datePickerStopCallback() {
                $scope.isPickerShow = false;
                showCurrentMonth();

                setTimeout(function () {
                    $scope.$digest();
                }, 0)
            }

            function datePickerStartCallback() {
                $scope.isPickerShow = true;
                setTimeout(function () {
                    $scope.$digest();
                }, 0)
            }

            function datePickerDateSetCallback(thingSet) {
                var selectedDate = dateTimePickerService.getSelectedDate();
                WeekdaysService.goToWeekOfDate(selectedDate);
                displaySpecialDate(selectedDate);
                infoStickyService.scrollToDate((selectedDate));
                if (!WeekdaysService.isDayInShowingRange(selectedDate)) {
                    storeSelectedDate(selectedDate);
                } else {
                    WeekdaysService.highlightActiveDate(selectedDate);
                }
                setTimeout(function () {
                    $scope.$digest();
                }, 0);
            }

            function datePickerViewChangeCallback() {
                var format = getCurrentMonthFormat();
                displaySpecialDate(null);
                $scope.data.thisMonth = dateTimePickerService.getViewingMonth(format);
                $scope.$evalAsync();
            }

            function removeCanceledAppointmentFromList(res) {
                var id = res.appointmentId;
                var dayOfList = res.dayOfList;
                var apptList = _.find(data.appointmentPerDayList, function (item) {
                    return item.dateOfList == dayOfList;
                });

                _.remove(apptList.appointmentsList, function (appt) {
                    return appt.id == id;
                });

                $scope.$digest();
            }

            function scrollStickyToDate(date) {
                if (date) {
                    infoStickyService.scrollToDate(date);
                }
            }

            function registerInfoStickyScrollEvent() {
                infoStickyService.scrollingStopCallback(function (dateScrollTo) {
                    highlightDateScrollTo(dateScrollTo);
                    storeSelectedDate(dateScrollTo);
                    showCurrentMonth();
                    $scope.$evalAsync();
                });

                infoStickyService.scrollingStartCallback(function () {
                    closeDatePicker();
                });
            }

            function closeDatePicker() {
                dateTimePickerService.stopPicker(true);
            }

            function highlightDateScrollTo(dateScrollTo) {
                WeekdaysService.highlightActiveDate(dateScrollTo);
                dateTimePickerService.setSelectedDate(dateScrollTo, null, true);
                displaySpecialDate(dateScrollTo);
            }

            function showError(res) {
                if (res.status == Configuration.ERROR_CODE.SERVER_ERROR) {
                    toastr.error(i18nService.translate('merchant_mobile', 'msg_server_error'));
                } else if (res.status == Configuration.ERROR_CODE.UNAUTHORIZED) {
                    toastr.error(res.message);
                }
            }

            function getLanguage() {
                _.assign(i18n, i18nService.getLanguage("merchant_mobile"));
            }

            function storeSelectedDate(selectedDate) {
                var storedDateSelectionName = Constants.LOCAL_STORAGE_KEY.SELECTED_DATE;
                localStorageService.store(storedDateSelectionName, selectedDate);
            }

            function getStoredSelectedDate() {
                var storedSelectedDateName = Constants.LOCAL_STORAGE_KEY.SELECTED_DATE;
                return localStorageService.retrieveString(storedSelectedDateName);
            }

            function performSignoutTask() {
                localStorageService.remove(Constants.LOCAL_STORAGE_KEY.SHOP_ID);
                localStorageService.remove(Constants.LOCAL_STORAGE_KEY.SHOP_DATA);
                localStorageService.remove(Constants.LOCAL_STORAGE_KEY.SHOP_PHONE);
                localStorageService.remove(Constants.LOCAL_STORAGE_KEY.SHOP_NAME);
                localStorageService.remove(Constants.LOCAL_STORAGE_KEY.CONTACT_MAIL);
                localStorageService.remove(Constants.LOCAL_STORAGE_KEY.USER_INFO);
                window.location = Constants.PATH.LOGIN_HASH;
            }

            function showCurrentMonth() {
                var format = getCurrentMonthFormat();
                var selectedDate = dateTimePickerService.getSelectedDate();
                displaySpecialDate(selectedDate);
                data.thisMonth = dateTimePickerService.getSelectedDate(format);

            }

            function getCurrentMonthFormat() {
                var locale = moment.locale();
                var format = 'YYYY MMMM';
                if (locale == Constants.LANGUAGE.JAPANESE) {
                    format = 'YYYY' + i18n.text_year + 'MMMM';
                }
                return format;
            }

            function isToday(date) {
                var date_m = moment(date, 'YYYY-MM-DD');
                return date_m.isSame(moment(), 'day');
            }

            function isYesterday(date) {
                var date_m = moment(date, 'YYYY-MM-DD');
                date_m.add(1, 'days');
                return date_m.isSame(moment(), 'day');
            }

            function isTomorrow(date) {
                var date_m = moment(date, 'YYYY-MM-DD');
                date_m.subtract(1, 'days');
                return date_m.isSame(moment(), 'day');
            }

            function displaySpecialDate(selectedDate) {
                if (!selectedDate) {
                    flags.isToday = false;
                    flags.isYesterday = false;
                    flags.isTomorrow = false;

                    return false;
                }
                flags.isToday = isToday(selectedDate);
                flags.isYesterday = isYesterday(selectedDate);
                flags.isTomorrow = isTomorrow(selectedDate);
            }

            function scrollToDate(selectedDate) {
                WeekdaysService.highlightActiveDate(selectedDate);
                displaySpecialDate(selectedDate);
                scrollStickyToDate(selectedDate);
            }

            function showCancelButton(target) {
                var $cancelButton;
                $('.cancel_appointment').hide('slide', {direction: 'right'}, 200);

                if (target.className.contains('cancel_appointment')) {
                    $cancelButton = $(target);
                } else {
                    $cancelButton = $(target).find('.cancel_appointment')
                }

                $cancelButton.show('slide', {direction: 'right'}, 200);
            }

            function hideCancelButton(target) {
                var $cancelButton;

                if (target.className.contains('cancel_appointment')) {
                    $cancelButton = $(target);
                } else {
                    $cancelButton = $(target).find('.cancel_appointment')
                }
                $cancelButton.hide('slide', {direction: 'right'}, 200);
            }

            function registerEvents() {
                EmployeeFilterEventBus.subscribeSelectingEvent('AppointmentController', function (empId) {
                    ShopResourceService.setEmployeeFilter(empId);
                    $route.reload();
                });

                AppointmentEventBus.subscribeCancel('AppointmentController', function (res) {
                    removeCanceledAppointmentFromList(res);
                });

                $scope.$on('$destroy', function () {
                    AppointmentEventBus.unsubscribeAll();
                })
            }
        }
    ])
;
