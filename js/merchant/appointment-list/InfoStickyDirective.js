app.directive("infoStickyDirective", ['InfoStickyService', function (infoStickyService) {
    return {
        restrict: 'A',
        scope: false,
        compile: function (scope, elm, attr) {
            function post(scope, elm, attr){
                infoStickyService.load(elm);
            }
            return {
                post: post
            }
        }
    }
}]);