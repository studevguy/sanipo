/**
 * Created by tiger on 09/07/2015.
 */
"use strict";

app.service('InfoStickyService', ['Constants', 'LocalStorageService', function (Constants, LocalStorageService) {
    var CLASS = Constants.CLASS;

    var self = this;
    var $bodyContent;
    var $scrollWrapper;
    var $sticky;
    var $date_anchors, $anchor, $nextAnchor, scrollTop, anchorPosition, nextAnchorPosition,
        stickyHeight, todayScrolling = false, observer,
        dateScrollTo, firstScrollDate, scrollStopEvent, scrollStartEvent,
        isScrolling, $appointmentList, isScrollWrapperChanged;

    this.load = function (elm) {
        if ($(elm).hasClass('body_content')) {
            $bodyContent = $(elm);
            $scrollWrapper = $bodyContent.find('.scroll_wrapper');
            $sticky = $bodyContent.find('.date_anchor_sticky');
            observer = new MutationObserver(__renderSticky);
            observer.observe($scrollWrapper[0], {childList: true});
            __registerEvent();
            return false;
        }

        if ($(elm).hasClass('appointment_list')
            && $bodyContent.length
            && observer
            && $scrollWrapper.length
            && $sticky) {
            observer.observe(elm[0], {childList: true});
        }
    };

    this.isScrolling = function () {
        return isScrolling;
    };

    this.scrollToDate = function (date) {
        if (!date) {
            return;
        }

        if (!__isDateInDisplayRange(date)) {
            return;
        }
        $scrollWrapper = $bodyContent.find('.scroll_wrapper');
        $scrollWrapper.stop();

        var MAX_INCREMENT = 21;
        var $destAnchor, destAnchorPosition, dateIncrement = -1, date_m, destDate_ISO;
        do {
            dateIncrement++;
            date_m = moment(date, 'YYYY-MM-DD');
            date_m.add(dateIncrement, 'days');
            destDate_ISO = date_m.format('YYYY-MM-DD');
            $destAnchor = $date_anchors.filter("div[date='" + destDate_ISO + "']");
        } while (!$destAnchor.length && dateIncrement < MAX_INCREMENT);
        destAnchorPosition = parseInt($destAnchor.data('originalPosition'));
        $scrollWrapper.animate({scrollTop: destAnchorPosition}, 500);

        __storeDateScrollTo(date);
    };

    this.setFirstScrollDate = function (date) {
        if (date) {
            firstScrollDate = date
        }
    };

    this.scrollingStopCallback = function (callback) {
        if (typeof (callback) == 'function') {
            scrollStopEvent = callback
        }
    };

    this.scrollingStartCallback = function (callback) {
        if (typeof (callback) == 'function') {
            scrollStartEvent = callback
        }
    };

    function __renderSticky(mutation) {
        isScrollWrapperChanged = false;
        $scrollWrapper = $bodyContent.find('.scroll_wrapper');
        $appointmentList = $bodyContent.find('.appointment_list');

        //check if bodyContent is being changed then scroll point
        mutation.forEach(function (item) {
            isScrollWrapperChanged = isScrollWrapperChanged || $(item.target).hasClass('scroll_wrapper');
        });

        __bindDateToAnchor();

        if (isScrollWrapperChanged) {
            $scrollWrapper.scrollTop(1);
            $sticky.html($($date_anchors[0]).html());
            if (firstScrollDate) {
                self.scrollToDate(firstScrollDate);
                firstScrollDate = undefined;
            }
        }

        setTimeout(function () {
            __addExtraSpaceToWrapper();
        }, 100);
    }

    function __scrollToToday() {
        var today = moment().format('YYYY-MM-DD');
        self.scrollToDate(today);
    }

    function __bindDateToAnchor() {
        var currentScrollPoint = $scrollWrapper.scrollTop();
        $scrollWrapper = $bodyContent.find('.scroll_wrapper');
        $scrollWrapper.scrollTop(0);

        $date_anchors = $scrollWrapper.find('.date_anchor');
        stickyHeight = $date_anchors.first().height();

        $date_anchors.each(function () {
            $anchor = $(this);
            $anchor.data('originalPosition', $anchor.position().top)
                .data('originalHeight', $anchor.outerHeight());
        });

        if (!isScrollWrapperChanged) {
            $scrollWrapper.scrollTop(currentScrollPoint);
        }
    }

    function __whenScrolling() {
        var diffHeight;
        $scrollWrapper = $bodyContent.find('.scroll_wrapper');
        scrollTop = $scrollWrapper.scrollTop();

        $date_anchors.each(function (i) {
            $anchor = $date_anchors.eq(i);
            $nextAnchor = $date_anchors.eq(i + 1);
            anchorPosition = $anchor.data('originalPosition');
            nextAnchorPosition = $nextAnchor.length ? $nextAnchor.data('originalPosition') : anchorPosition + stickyHeight;
            if (scrollTop >= (anchorPosition - stickyHeight) && scrollTop < anchorPosition) {
                diffHeight = anchorPosition - stickyHeight - scrollTop;
                $sticky.css({top: diffHeight});
            }

            else if (scrollTop >= anchorPosition && scrollTop < nextAnchorPosition) {
                $sticky.html($anchor.html());
                $sticky.height(stickyHeight);
                $sticky.css({top: 0});
                __storeDateScrollTo($anchor.attr('date'));
            }
        });
    }

    function __isDateInDisplayRange(date) {
        var date_m = moment(date, 'YYYY-MM-DD');

        var $firstAnchor = $date_anchors.first();
        var $lastAnchor = $date_anchors.last();

        var startRange = $firstAnchor.attr('date');
        var startRange_m = moment(startRange, 'YYYY-MM-DD');
        var endRange = $lastAnchor.attr('date');
        var endRange_m = moment(endRange, 'YYYY-MM-DD');

        return date_m.isSame(startRange_m)
            || date_m.isSame(endRange_m)
            || date_m.isBetween(startRange_m, endRange_m);
    }

    function __storeDateScrollTo(date) {
        dateScrollTo = date;
    }

    function __getDateScrollTo() {
        return dateScrollTo;
    }

    function __addExtraSpaceToWrapper() {
        var $lastAppointmentListPerDate = $scrollWrapper.find(CLASS.APPOINTMENT_LIST).last();
        var lastAppointmentListElementHeight = $lastAppointmentListPerDate.outerHeight();
        var wrapperHeight = $scrollWrapper.outerHeight();

        var differentHeight = wrapperHeight - lastAppointmentListElementHeight;

        var $additionalElement = __getAdditionalContent();

        $additionalElement.height(differentHeight);
    }

    function __getAdditionalContent() {
        return $scrollWrapper.find(CLASS.EXTRA_SPACE);
    }

    function __registerEvent() {
        $scrollWrapper = $bodyContent.find('.scroll_wrapper');
        $scrollWrapper.off("scroll.stickies");
        $scrollWrapper.off("scrollstop.stickies");
        $scrollWrapper.off("scrollstart.stickies");

        $scrollWrapper.on("scroll.stickies", function () {
            __whenScrolling();
        });

        $scrollWrapper.on("scrollstop.stickies", {latency: 650}, function () {
            isScrolling = false;
            __triggerScrollStopEvent(__getDateScrollTo());
        });

        $scrollWrapper.on("scrollstart.stickies", function () {
            isScrolling = true;
            __triggerScrollStartEvent();
        });
    }


    function __triggerScrollStopEvent(dateScrollTo) {
        if (scrollStopEvent) {
            scrollStopEvent(dateScrollTo);
        }
    }

    function __triggerScrollStartEvent() {
        if (scrollStartEvent) {
            scrollStartEvent();
        }
    }

}]);