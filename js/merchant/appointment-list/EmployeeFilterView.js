"use strict";

(function (app) {
    app.factory("EmployeeFilterView", [
        'Constants',
        'ShopResourceService',
        'PopupService',
        'LocalStorageService',
        'EmployeeFilterEventBus',
        'Sidebar',
        EmployeeFilterView]);

    function EmployeeFilterView(Constants,
                                ShopResourceService,
                                PopupService,
                                LocalStorageService,
                                EmployeeFilterEventBus,
                                Sidebar) {

        function View($scope) {
            this.scope = $scope;
            this.data = {};
            this.fn = {};
            this.scope.data = this.data;
            this.scope.fn = this.fn;
        }

        View.prototype.show = function () {
            this.data.employeeList = ShopResourceService.getEmployeeList();
            this.data.currentEmployee = this.scope.initialData;
            this.bindEvents();
        };

        View.prototype.bindEvents = function () {
            var self = this;

            this.fn.selectEmployee = function (employeeId) {
                self.publishEmployeeSelectedEvent(employeeId);
                Sidebar.close();
            };

            this.fn.openSignoutPopup = function () {
                var options = {
                    templateUrl: '../templates/merchant/signout_popup.html',
                    size: {
                        top: '30%'
                    },
                    modal: true
                };

                PopupService.open(options, self.scope)
            };

            this.fn.closePopup = function () {
                PopupService.close();
            };

            this.fn.signout = function () {
                PopupService.close(self.performSignoutTask.bind(self));
            };

            this.fn.close = function () {
                Sidebar.close();
            }
        };

        View.prototype.publishEmployeeSelectedEvent = function (employeeId) {
            EmployeeFilterEventBus.publishSelectingEvent(employeeId);
        };

        View.prototype.performSignoutTask = function () {
            LocalStorageService.remove(Constants.LOCAL_STORAGE_KEY.SHOP_ID);
            LocalStorageService.remove(Constants.LOCAL_STORAGE_KEY.SHOP_DATA);
            LocalStorageService.remove(Constants.LOCAL_STORAGE_KEY.SHOP_PHONE);
            LocalStorageService.remove(Constants.LOCAL_STORAGE_KEY.SHOP_NAME);
            LocalStorageService.remove(Constants.LOCAL_STORAGE_KEY.CONTACT_MAIL);
            LocalStorageService.remove(Constants.LOCAL_STORAGE_KEY.USER_INFO);
            window.location = Constants.PATH.LOGIN_HASH;
        };

        function newInstance($scope) {
            return new View($scope);
        }

        return {
            newInstance: newInstance
        };
    }
})(app);