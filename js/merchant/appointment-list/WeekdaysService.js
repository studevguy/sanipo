/**
 * Created by tiger on 21/06/2015.
 */
"use strict";

app.service("WeekdaysService", ["$templateCache", '$compile', function ($templateCache, $compile) {
    var self = this;
    var weekListTemplate = $templateCache.get("../templates/merchant/week_list.html");
    var selectedDate;

    this.slider = {};
    this.events = {
        onWeekDayChanged: function () {
        }
    };

    this.renderWeekDays = function ($scope, options) {
        this.__initData(options);
        this.__populateDateForThreeWeekLists();
        this.__renderSlider($scope);
        this.__applyCurrentTime();
        if (options.onWeekDayChanged) {
            this.events.onWeekDayChanged = options.onWeekDayChanged;
        }
        if (options.activeDate) {
            this.highlightActiveDate(options.activeDate);
        }
    };

    this.__initData = function (options) {
        if (options.selectedDate) {
            this.showingDate = moment(options.selectedDate);
        } else {
            this.showingDate = moment();
        }
        this.targetDate = this.showingDate.clone();
        this.$weekDays = $("#week_days");
        this.$weekList = $(weekListTemplate);
    };

    this.__renderSlider = function ($scope) {
        this.$weekList.appendTo(this.$weekDays);
        $compile(this.$weekList)($scope);
        this.slider = $('.bxslider').bxSlider({
            swipeThreshold: 100,
            controls: false,
            startSlide: 1,
            pager: false,
            speed: 100,
            onSlideAfter: this.__onSlideAfterHandler.bind(this)
        });
    };

    this.__calculateDateRangeForThreeWeeks = function () {
        var startDate = this.showingDate.clone().subtract(1, 'weeks').startOf("week");
        var endDate = this.showingDate.clone().add(1, 'weeks').endOf("week");

        var dateRange = [];
        for (var i = startDate; (i.isBefore(endDate)) || (i.isSame(endDate)); i.add(1, "days")) {
            dateRange.push(i.clone());
        }
        return dateRange
    };

    this.__populateDateForWeek = function ($weekElement, $startIndexOfDayRange) {
        var date;

        var dayRange = this.__calculateDateRangeForThreeWeeks();

        $($weekElement).each(function () {
            var startIndexOfDayRange = $startIndexOfDayRange;
            $(this).find('li').each(function () {
                date = dayRange[startIndexOfDayRange];
                $(this).find('.day_of_week').text(date.format("ddd"));
                $(this).find('.date_of_month').text(date.date());
                $(this).attr("data-date", date.format("YYYY-MM-DD"));
                startIndexOfDayRange++
            });
        });
    };

    this.__applyCurrentTime = function () {
        var today = moment().format("YYYY-MM-DD");
        $('.day_list').find('li').removeClass('today');
        $('.day_list').find("li[data-date='" + today + "']").addClass('today');
    };

    this.__populateDateForThreeWeekLists = function () {
        var $firstWeek = this.$weekList.find(".first_week");
        var $secondWeek = this.$weekList.find(".second_week");
        var $thirdWeek = this.$weekList.find(".third_week");
        this.__populateDateForWeek($firstWeek, 0);
        this.__populateDateForWeek($secondWeek, 7);
        this.__populateDateForWeek($thirdWeek, 14);
    };

    this.__onSlideAfterHandler = function ($slideElement, oldIndex, newIndex) {
        if (this.__isDecreaseDirection(oldIndex, newIndex)) {
            this.__decreaseShowingWeek();
        } else {
            this.__increaseShowingWeek();
        }
        this.__rePopulateDateForNextAndPrevWeek($slideElement);
        this.__applyCurrentTime();
        this.events.onWeekDayChanged();
    };

    this.__rePopulateDateForNextAndPrevWeek = function ($slideElement) {
        var weekListClass = $slideElement.attr('class');
        var $nextSlide, $prevSlide;
        var $firstWeek = this.$weekList.find(".first_week");
        var $secondWeek = this.$weekList.find(".second_week");
        var $thirdWeek = this.$weekList.find(".third_week");

        switch (weekListClass) {
            case 'first_week':
                $nextSlide = $secondWeek;
                $prevSlide = $thirdWeek;
                break;
            case 'second_week':
                $nextSlide = $thirdWeek;
                $prevSlide = $firstWeek;
                break;
            case 'third_week':
                $nextSlide = $firstWeek;
                $prevSlide = $secondWeek;
                break;
        }

        self.__populateDateForWeek($slideElement, 7);
        self.__populateDateForWeek($nextSlide, 14);
        self.__populateDateForWeek($prevSlide, 0);
    };

    this.__increaseShowingWeek = function () {
        if (this.targetDate.isSame(this.showingDate)) {
            this.showingDate.add(1, 'weeks');
            this.targetDate = this.showingDate.clone();
        } else {
            this.showingDate = this.targetDate.clone();
        }
    };

    this.__decreaseShowingWeek = function () {
        if (this.targetDate.isSame(this.showingDate)) {
            this.showingDate.subtract(1, 'weeks');
            this.targetDate = this.showingDate.clone();
        } else {
            this.showingDate = this.targetDate.clone();
        }

    };

    this.__isDecreaseDirection = function (oldIndex, newIndex) {
        return (oldIndex == 1 && newIndex == 0) || (oldIndex == 0 && newIndex == 2) || (oldIndex == 2 && newIndex == 1)
    };

    this.__refreshSlider = function () {
        this.slider.reloadSlider();
    };

    this.goToThisWeek = function () {
        this.goToWeekOfDate(moment().format('YYYY-MM-DD'));
    };

    this.goToWeekOfDate = function (date) {
        var date_m = moment(date, 'YYYY-MM-DD');
        if (this.isDayInShowingRange(date)) {
            return;
        }

        if (date_m.isBefore(this.showingDate)) {
            this.slider.goToPrevSlide();
        } else if (date_m.isAfter(this.showingDate)) {
            this.slider.goToNextSlide();
        }

        this.targetDate = date_m;
    };

    this.isDayInShowingRange = function (date) {
        var date_m = moment(date, 'YYYY-MM-DD');
        var targetWeek = date_m.week();
        var showingWeek = this.showingDate.week();
        return (date_m.isSame(this.showingDate) || targetWeek == showingWeek)
    };

    this.getStartShowingDay = function (format) {
        var fmt = format || 'YYYY-MM-DD';
        return this.showingDate.clone().startOf("week").format(fmt);
    };

    this.getEndShowingDay = function () {
        return this.showingDate.clone().endOf("week").format("YYYY/MM/DD");
    };

    this.getSelectedDate = function () {
        return this.targetDate.format('YYYY-MM-DD');
    };

    this.highlightActiveDate = function (activeDate) {
        this.$weekDays.find('li').removeClass('active');
        this.$weekDays.find("li[data-date='" + activeDate + "']").addClass('active');
    }
}]);
