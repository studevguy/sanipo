/**
 * Created by sangdao on 13/07/2015.
 */
"use strict";

app.directive("ngSelectedDate", ['WeekdaysService',function (weekdaysService) {
    return {
        restrict: "A",
        scope: false,
        link: function (scope, elm, attr) {
            var $elm = $(elm);

            $elm.click(function ($event) {
                var $currentTarget = $($event.currentTarget);
                var activeDate = $currentTarget.attr('data-date');
                weekdaysService.highlightActiveDate(activeDate);
            })
        }
    }
}]);