"use strict";

(function (app) {
    app.directive("spVisible", [VisibleDirective]);

    function VisibleDirective() {
        return {
            restrict: "A",
            scope: false,
            compile: function (elm) {
                return {
                    pre: preLink
                }
            }
        };

        function preLink(scope, elm, attr) {
            var trigger = attr.spVisible;
            var $elm = $(elm);
            scope.$watch(trigger, function (newVal, oldVal) {
                if (newVal) {
                    $elm.css({
                        'position': 'static'
                    });
                } else {
                    $elm.css({
                        'position': 'absolute',
                        'top': '-1000px'
                    });

                }
            })
        }
    }
})(app);