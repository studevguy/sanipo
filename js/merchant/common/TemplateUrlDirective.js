"use strict";

app.directive("ngTemplateUrl", ['$templateCache', 'Constants', function ($templateCache, constants) {
    return {
        restrict: "A",
        scope: false,
        compile: function (elm, attr) {
            var url = attr.ngTemplateUrl.replace(/^'|'$/g, '');
            var html = $templateCache.get(url);
            $(elm).css("height", "100%").html(html);
        }
    }
}]);