/**
 * Created by sangdao on 02/07/2015.
 */
"use strict";

app.service("DateTimePickerService", [function () {
    var MOMENT_INFINITY = 'Infinity-Infinity-Infinity';
    var $elm, picker;
    var spOptions = {}, isSwiping = false, isChangeView = false, isPickerReady = false;
    this.picker = {};
    this.__triggerStopEvent = function () {
    };
    this.__triggerStartEvent = function () {
    };
    this.__triggerDateSetEvent = function () {
    };
    this.__triggerViewChangeEvent = function () {
    };
    this.registerElement = function (elm) {
        $elm = $(elm);
    };
    this.renderPicker = function (options, callbacks) {
        isPickerReady = false;
        if (callbacks) {
            if (typeof (callbacks.onStart) == 'function') {
                this.__triggerStartEvent = callbacks.onStart;
            }

            if (typeof (callbacks.onStart) == 'function') {
                this.__triggerStopEvent = callbacks.onStop;
            }

            if (typeof (callbacks.onChange) == 'function') {
                this.__triggerViewChangeEvent = callbacks.onChange;
            }

            if (typeof (callbacks.onSet) == 'function') {
                this.__triggerDateSetEvent = callbacks.onSet;
            }
        }

        spOptions.selected = options.selected;
        spOptions.closeOnDateSelection = options.closeOnDateSelection;

        options.onStart = this.onStartHandler.bind(this);
        options.onStop = this.onStopHandler.bind(this);
        options.onSet = this.onDateSetHandler.bind(this);

        picker = $elm.pickadate(options).pickadate('picker');

        if (spOptions.selected) {
            this.setSelectedDate(spOptions.selected);
        }
        this.stopPicker();
    };

    this.isPickerStarted = function () {
        return picker.get('start');
    };

    this.isPickerStop = function () {
        return !picker.get('start');
    };

    this.togglePicker = function () {
        var isStart = this.isPickerStarted();
        if (isStart) {
            this.stopPicker()
        } else {
            this.startPicker();
        }
    };

    this.onStartHandler = function () {
        if(!isPickerReady){
            return false;
        }
        this.__triggerStartEvent();
        this.__registerSwipeEvent();
        this.__reRenderPickerHeader();
    };

    this.onStopHandler = function () {
        if (isPickerReady) {
            this.__triggerStopEvent();
        } else {
            isPickerReady = true;
        }

    };

    this.onDateSetHandler = function (thingSet) {
        if(!isPickerReady){
            return false;
        }
        if (!isSwiping) {
            this.__triggerDateSetEvent(thingSet);
        }

        if (spOptions.closeOnDateSelection && !isChangeView) {
            this.stopPicker();
        }

        isSwiping = false;
        isChangeView = false;
    };

    this.__registerSwipeEvent = function () {
        var self = this;
        if (picker) {
            $(picker.$root).swipe({
                swipe: function (event, direction, distance) {
                    if ((direction != 'right') && (direction != 'left')) {
                        return false
                    }
                    self.__slidePicker(direction);
                    isSwiping = true;
                }
            });
        }
    };

    this.__slidePicker = function (direction) {
        var self = this;
        var $pickerTable = picker.$holder.find('.picker__table').not('.clone_picker__table');
        var inverseDirection;
        if (direction == 'right') {
            inverseDirection = 'left';

            if (this.__isMinDateAtCurrentView()) {
                return false
            }

            $pickerTable.hide('slide', {direction: direction}, 300, function () {
                self.__decreaseViewingMonth();
                self.__reRenderPickerHeader();
                self.__triggerViewChangeEvent();
                $pickerTable.show('slide', {direction: inverseDirection}, 100);
            });
        } else if (direction == 'left') {
            inverseDirection = 'right';
            $pickerTable.hide('slide', {direction: direction}, 500, function () {
                self.__increaseViewingMonth();
                self.__reRenderPickerHeader();
                self.__triggerViewChangeEvent();
                $pickerTable.show('slide', {direction: inverseDirection}, 500);
            });
        }

    };

    this.__increaseViewingMonth = function () {
        var viewingMonth_m = moment(picker.get('view', 'yyyy-mm-dd'), 'YYYY-MM-DD');
        var nextMonth = viewingMonth_m.add(1, 'months').format('YYYY-MM-DD');
        isChangeView = true;
        picker.set('view', nextMonth, {format: 'yyyy-mm-dd'});
    };

    this.__decreaseViewingMonth = function () {
        var viewingMonth_m = moment(picker.get('view', 'yyyy-mm-dd'), 'YYYY-MM-DD');
        var preMonth = viewingMonth_m.subtract(1, 'months').format('YYYY-MM-DD');
        isChangeView = true;
        picker.set('view', preMonth, {format: 'yyyy-mm-dd'});
    };

    this.__reRenderPickerHeader = function () {
        if (picker) {
            if (picker.$holder) {
                var $pickerBox = picker.$holder.find('.picker__box');
                var $table = picker.$holder.find('table');
                var $thead = $table.find('thead');
                var $cloneTable = $table.clone();
                var $clone_tbody = $cloneTable.find('tbody');

                $clone_tbody.remove();

                $thead.hide();

                $table.css({'margin-top': 0, 'margin-bottom': 0});
                $cloneTable.css({'margin-bottom': 0});
                $cloneTable.addClass('clone_picker__table');
                $cloneTable.prependTo($pickerBox);
            }
        }
    };

    this.stopPicker = function (isAnimation) {
        if (this.isPickerStarted()) {
            picker.stop();
        }

    };

    this.startPicker = function (selectedDate) {
        picker.start();
    };

    this.increaseSelectedDate = function () {
        var selectedDate = picker.get('select', 'yyyy-mm-dd');
        var selectedDate_m = moment(selectedDate, 'YYYY-MM-DD');
        selectedDate_m.add(1, 'days');
        selectedDate = selectedDate_m.format('YYYY-MM-DD');
        this.setSelectedDate(selectedDate);

    };

    this.decreaseSelectedDate = function () {
        var selectedDate = picker.get('select', 'yyyy-mm-dd');
        var selectedDate_m = moment(selectedDate, 'YYYY-MM-DD');
        selectedDate_m.subtract(1, 'days');
        selectedDate = selectedDate_m.format('YYYY-MM-DD');
        this.setSelectedDate(selectedDate);
    };

    this.setSelectedDate = function (date, format, notTrigerEvent) {
        if(!date) {
            return;
        }
        var fmt = format || 'YYYY-MM-DD';
        var minDate = picker.get('min', 'yyyy-mm-dd');
        var minDate_m = moment(minDate, 'YYYY-MM-DD');
        var maxDate = picker.get('max', 'yyyy-mm-dd');
        var maxDate_m = moment(maxDate, 'YYYY-MM-DD');
        var date_m = moment(date, fmt);
        var selectedDate = date_m.format('YYYY-MM-DD');
        if (minDate) {
            if (date_m.isBefore(minDate_m)) {
                selectedDate = minDate
            }
        }
        if (maxDate != MOMENT_INFINITY) {
            if (date_m.isAfter(maxDate_m)) {
                selectedDate = maxDate;
            }
        }
        picker.set('select', selectedDate, {format: 'yyyy-mm-dd'});

        if (this.isPickerStop() && !notTrigerEvent) {
            this.__triggerDateSetEvent();
        }
    };

    this.getSelectedDate = function (format) {
        var fmt = format || 'YYYY-MM-DD';
        var selectedDate = picker.get('select', 'yyyy-mm-dd');
        if (!selectedDate) {
            selectedDate = moment();
        }
        return moment(selectedDate, 'YYYY-MM-DD').format(fmt);
    };


    this.getViewingMonth = function (format) {
        var fmt = format || 'MM-YYYY';
        return moment(picker.get('view').obj).format(fmt);
    };

    this.__getMinDate = function () {
        return picker.get('min', 'yyyy-mm-dd');
    };

    this.__isMinDateAtCurrentView = function () {
        var minDate_m = moment(this.__getMinDate(), 'YYYY-MM-DD');

        if (!minDate_m) {
            return false;
        }

        var firstDateOfView = picker.get('view', 'yyyy-mm-dd');
        var firstDateOfView_m = moment(firstDateOfView, 'YYYY-MM-DD');
        return (minDate_m.isSame(firstDateOfView_m) || minDate_m.isAfter(firstDateOfView_m));
    };

    this.__isSelectedDateOnView = function () {
        var selectedDate = picker.get('select', 'yyyy-mm-dd');
        if (!selectedDate) {
            return false;
        }
        var selectedDate_m = moment(selectedDate);
        var firstDateOfView = picker.get('view', 'yyyy-mm-dd');
        var firstDateOfView_m = moment(firstDateOfView, 'YYYY-MM-DD');
        var lastDateOfView_m = moment(firstDateOfView, 'YYYY-MM-DD').endOf('month');
        return selectedDate_m.isSame(firstDateOfView)
            || selectedDate_m.isSame(lastDateOfView_m)
            || selectedDate_m.isBetween(firstDateOfView_m, lastDateOfView_m);
    };
}]);
