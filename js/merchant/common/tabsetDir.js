"use strict";

(function (app) {
    app.directive("tabset", ['TabsetSvc', tabsetDirDirective]);

    function tabsetDirDirective(TabsetSvc) {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            controller: ['$scope', function ($scope) {
                $scope.activeTab = TabsetSvc.getActiveTab();
            }],
            template:
            '<div class="tab_container">' +
            '<div class="tab_source" hidden ng-transclude></div>' +
            '<ng-include src="activeTab.templateUrl"></ng-include>' +
            '</div>'
        };
    }

    app.directive("tab", ['TabsetSvc', tabDirDirective]);

    function tabDirDirective(TabsetSvc) {
        return {
            restrict: 'E',
            scope: {
                tabId: '@',
                templateUrl: '@'
            },
            link: function (scope, element, attrs) {
                TabsetSvc.addTab(scope);
            }
        }
    }
})(app);