"use strict";

app.directive("ngBindingAttribute", ['Constants','$compile',function (constants,$compile) {
    return {
        restrict: "A",
        scope: false,
        priority: constants.DIRECTIVE_PRIORITY.PHRASE_PROCESS_DOM,
        link: {
            pre:function (scope,elm, attr) {
                var $elm = $(elm);
                var processedAttrs = getAttrsForProcessing(attr);
                var newAttrValue, targetAttrValue;
                processedAttrs.forEach(function (item) {
                    targetAttrValue = $elm.attr(item.targetAttr);
                    newAttrValue = scope.$eval(targetAttrValue);
                    $elm.attr(item.newAttrName, newAttrValue);
                });
            }
        }
    };
    function getAttrsForProcessing(attrs){
        var ngProcess = 'ngProcess';
        var result = [];
        var targetAttr, newAttrName, indexToCut = ngProcess.length;
        for (var prop in attrs){
            if(prop.indexOf(ngProcess) != -1){
                targetAttr = prop;
                newAttrName = targetAttr.substring(indexToCut);
                result.push({
                    targetAttr: transformCamelCaseToDashLowerCaseString(targetAttr),
                    newAttrName: transformCamelCaseToDashLowerCaseString(newAttrName)
                })
            }
        }
        return result;
    }

    function transformCamelCaseToDashLowerCaseString(str){
        return str.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
    }
}]);