"use strict";
app.run(['$rootScope', '$location', 'LocalStorageService', 'Constants',
    function ($rootScope, $location, localStorageService, Constants) {

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var userInfo = localStorageService.retrieve(Constants.LOCAL_STORAGE_KEY.USER_INFO);
            var username;

            if(userInfo){
                username = userInfo.username;
            }

            var path=$location.path();

            var isLoginPage = $.inArray(path, ['/login']) !== -1;
            var isNotCheckLoginPath = (path=='/m') || (path.indexOf('/m/') !== -1);

            if (!isNotCheckLoginPath && !username && !isLoginPage) {
               $location.path('/login');
            }
        });
    }
]);


