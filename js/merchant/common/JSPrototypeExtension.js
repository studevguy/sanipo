if (!('contains' in String.prototype)) {
    String.prototype.contains = function (str, startIndex) {
        return -1 !== String.prototype.indexOf.call(this, str, startIndex);
    };
}

if (!('clear' in Array.prototype)) {
    Array.prototype.clear = function() {
        while (this.length) {
            this.pop();
        }
    };
}

