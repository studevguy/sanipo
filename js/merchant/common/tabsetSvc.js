"use strict";

(function (app) {
    app.service("TabsetSvc", [TabsetSvc]);

    function TabsetSvc() {
        var tabs = [], activeTab = {};

        this.showFirstTab = function () {
            this.selectTab(tabs[0].tabId);
        };

        this.getActiveTab = function () {
            return activeTab;
        };

        this.getActiveTabOrder = function () {
            var activeTabIndex = this.getActiveTabIndex();
            return activeTabIndex + 1;
        };

        this.selectTab = function (tabId) {
            var newActiveTab = _.find(tabs, function (tab) {
                return tab.tabId == tabId
            });
            _.assign(activeTab, newActiveTab);
        };

        this.nextTab = function () {
            var activeTabIndex = this.getActiveTabIndex();
            if (tabs.length - 1 <= activeTabIndex) {
                console.log('can not navigate to tab');
                return false;
            }
            var nextTabId = tabs[activeTabIndex + 1].tabId;
            this.selectTab(nextTabId)
        };

        this.prevTab = function () {
            var activeTabIndex = this.getActiveTabIndex();
            if (0 >= activeTabIndex) {
                console.log('can not navigate to tab');
                return false;
            }
            var prevTabId = tabs[activeTabIndex + -1].tabId;
            this.selectTab(prevTabId)
        };

        this.goToTab = function (tabOrder) {
            if ((tabOrder > tabs.length + 1) && (tabOrder < 1)) {
                console.log('can not navigate to tab');
                return false
            }

            var tabId = tabs[tabOrder - 1].tabId;
            this.selectTab(tabId);
        };

        this.getActiveTabIndex = function () {
            var activeTabIndex = _.findIndex(tabs, function (tab) {
                return tab.tabId == activeTab.tabId;
            });

            return activeTabIndex;
        };


        this.addTab = function (tab) {
            var existingTabIndex = _.findIndex(tabs, function (item) {
                return tab.tabId == item.tabId;
            });

            if (existingTabIndex == -1) {
                tabs.push(tab);
            }
        };

        this.clearActiveTabs = function () {
            activeTab = {};
        }

    }
})(app);