"use strict";
app.run(['i18nService',
    function (i18nService) {
        i18nService.registerInitCallback().then(function (data) {
            moment.locale(i18nService.locale(), {
                    months: data.common.monthNames,
                    monthsShort: data.common.monthNames,
                    weekdays: data.common.weekdayNamesShort,
                    weekdaysShort: data.common.weekdayNamesShort,
                    weekdaysMin: data.common.weekdayNamesShort
                }
            );
        });
    }
]);


