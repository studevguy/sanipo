"use strict";

(function (app) {
    app.factory("Sidebar", ['$templateCache', '$compile', '$rootScope', Sidebar]);

    function Sidebar($templateCache, $compile, $rootScope) {
        var service = {};
        var sidebarId = 'sidr';
        var viewId = 'ng-view';
        var $container = $('#container');
        var toggleSidebarId = 'toggle-sidebar';
        var sidrOpenClass = 'sidr-open';
        var $sidebar, newScope = {};
        var $toggleSidebar = $('<a></a>').appendTo($container);

        $toggleSidebar.attr({
            id: toggleSidebarId
        });

        /**
         * open sidebar
         * @param $scope
         * @param {object} data The passed to child controller
         * @param {string }sidebarContentUrl The url to load html for sidebar content
         * @param options
         */
        service.open = function ($scope, data, sidebarContentUrl, options) {
            renderSidebar(sidebarContentUrl, data, options);
            jQuery.sidr('toggle', 'sidr', onCompleteRender.call(this, $scope, data));
        };

        service.close = function () {
            jQuery.sidr('close', sidebarId, onCloseHandler);
        };

        function renderSidebar(sidebarContentUrl, options) {
            var content = $templateCache.get(sidebarContentUrl);
            var direction = 'left';
            if (options) {
                direction = options.direction ? options.direction : direction;
            }
            //add overlay layer
            if (!$('#' + viewId + ' .overlay').length) {
                $('#' + viewId).append('<div class="overlay"></div>');
            }

            $toggleSidebar.sidr({
                name: sidebarId,
                body: '#' + viewId,
                side: direction,
                source: function () {
                    return content;
                },
                onOpen: onOpenHandler,
                onClose: onCloseHandler
            });
        }

        function bootstrapNewElement($scope, data) {
            if (newScope.$destroy) {
                newScope.$destroy();
            }
            $sidebar = $('#' + sidebarId);
            newScope = $scope.$new();
            newScope.initialData = data;
            $compile($sidebar[0])(newScope);
        }

        function onOpenHandler() {
            //registerClickOffHandler(function () {
            //    service.close();
            //});
            configNavigationBehaviour();
        }

        function isSidebarOpen() {
            return $('body').hasClass(sidrOpenClass);
        }

        function onCloseHandler() {
            unbindEvents();
            removeSidebarContent();
        }

        function onCompleteRender($scope, data) {
            bootstrapNewElement($scope, data);
        }

        //To close slide when user click outside
        function registerClickOffHandler(callback) {
            $sidebar = $('#' + sidebarId);

            $(document).on('click.sidr', function (event) {
                var target = event.target;
                var isSidebarTarget = false;
                //check if target is sidebar
                if ((sidebarId == target.id) || jQuery.contains($sidebar[0], target)) {
                    isSidebarTarget = true;
                }
                if (isSidebarTarget) {
                    return false;
                }
                callback(event);
            });
        }

        function unbindEvents() {
            $sidebar.off('click.sidr');
            $(document).off('click.sidr');
        }

        function removeSidebarContent() {
            if ($sidebar.length) {
                $sidebar.empty();
            }
        }


        function configNavigationBehaviour() {
            var offCallMeFn = $rootScope.$on("$locationChangeStart", function (event, newUrl, oldUrl) {
                offCallMeFn();
                service.close();
            });
        }

        return service;
    }
})(app);
