"use strict";
app.run([
    'LocalStorageService',
    'Configuration',
    function (LocalStorageService,
              Configuration) {
        LocalStorageService.store(
            Configuration.localStorageName.selectedDate,
            moment().format('YYYY-MM-DD'));
    }
]);


