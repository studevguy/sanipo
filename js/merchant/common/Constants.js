"use strict";

app.constant("Constants", (function () {
    var DIRECTIVE_PRIORITY = {
        PHRASE_RENDER_LOGIC: 999,
        PHRASE_RENDER_TEMPLATE: 99,
        PHRASE_PROCESS_DOM: 1
    };

    var CLASS = {
        APPOINTMENT_LIST: '.appointment_list',
        EXTRA_SPACE: '.extra_space'
    };
    var STATUS_KEY_TRANSLATE = {
        "CANCEL": "label_status_cancel",
        "UNAPPROVED": "label_status_unapproved",
        "APPROVED": "label_status_approved",
        "ATSTORE": "label_status_atstore"
    };

    var DATE_STATUSES = {
        DATE_OFF: 'DATE_OFF',
        WORKING: 'WORKING',
        NO_SCHEDULE_SETTING: 'NO_SCHEDULE'
    };

    var APPOINTMENT_STATUS = {
        APPROVED: 11,
        UNAPPROVED: 10,
        AT_STORE: 21
    };

    var PATH = {
        LOGIN: '/login',
        LOGIN_HASH: '#/login',
        APPOINTMENTS: '/appointments',
        APPOINTMENTS_HASH: '#/appointments'
    };

    var LANGUAGE = {
        JAPANESE: 'ja',
        ENGLISH: 'en'
    };

    var LOCAL_STORAGE_KEY = {
        SHOP_ID: "shopId",
        SHOP_DATA: "shopData",
        SHOP_NAME: "shopName",
        SHOP_PHONE: "shopPhone",
        CONTACT_MAIL: "contactEmail",
        BOOKER_INFO: "BOOKER_INFO",
        USER_INFO: 'sunnypoint_user_info',
        SELECTED_DATE: 'sp_selected_date'
    };

    var VALIDATION_RULE = {
        SHOPPER_CODE: {
            MAX: 20
        },
        FULL_NAME: {
            MAX: 30
        },
        DISPLAY_NAME: {
            MAX: 20
        },
        EMAIL: {
            REGEX: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
            MAX: 200
        },
        PHONE_NUMBER: {
            REGEX: /^(\+?([0-9]+$))/i,
            MAX: 17
        },
        NOTE: {
            MAX: 200
        },
        SPECIAL_CHARACTER: {
            REGEX: /[\<\>?~!@#\$%^&\*,\s]+/i
        }
    };

    var EMPLOYEE_COLOR_RANGE = [
        'text_orange'
        , 'text_light_orange'
        , 'text_blue_green'
        , 'text_extreme_pink'
        , 'text_blue'
        , 'text_brown'
    ];


    var NO_EMPLOYEE_FILTER = -1;

    var CHANNEL_TYPE = {
        TABLET: 1,
        HOT_PEPPER: 7
    };

    var EMAIL_CONFIRM_TYPE = {
        CONFIRM_TYPE_CANCELED: 0,
        CONFIRM_TYPE_APPROVED: 1,
        CONFIRM_TYPE_CHANGED: 2
    };

    return {
        SHOP_ID: "shopId",
        SHOP_DATA: "shopData",
        SHOP_NAME: "shopName",
        SHOP_PHONE: "shopPhone",
        CONTACT_MAIL: "contactEmail",
        BOOKER_INFO: "BOOKER_INFO",
        DIRECTIVE_PRIORITY: DIRECTIVE_PRIORITY,
        CLASS: CLASS,
        STATUS_KEY_TRANSLATE: STATUS_KEY_TRANSLATE,
        DATE_STATUSES: DATE_STATUSES,
        APPOINTMENT_STATUS: APPOINTMENT_STATUS,
        PATH: PATH,
        LANGUAGE: LANGUAGE,
        LOCAL_STORAGE_KEY: LOCAL_STORAGE_KEY,
        VALIDATION_RULE: VALIDATION_RULE,
        EMPLOYEE_COLOR_RANGE: EMPLOYEE_COLOR_RANGE,
        NO_EMPLOYEE_FILTER: NO_EMPLOYEE_FILTER,
        CHANNEL_TYPE: CHANNEL_TYPE,
        EMAIL_CONFIRM_TYPE: EMAIL_CONFIRM_TYPE
    }
})());

