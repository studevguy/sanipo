"use strict";

(function (app) {
    app.run(['Configuration', PreloadIconImage]);

    function PreloadIconImage(Configuration) {
            var images = Configuration.PRELOADED_ICON_IMAGES;
            for (var i = 0; i < images.length; ++i) {
                var img = new Image();
                img.src = images[i];
            }
    }
})(app);