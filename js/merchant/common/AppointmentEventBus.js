/**
 * Created by sangdao on 12/7/15.
 */
"use strict";

(function (app) {
    app.service("AppointmentEventBus", [EmployeeFilterEventBus]);

    function EmployeeFilterEventBus() {
        var self = this;
        this.selectAppointmentScheduleSignal = 'appointment-schedule';
        this.selectDateTimeSignal = 'date-time';
        this.selectDetailsSignal = 'details';
        this.cancelSignal = 'cancel';
        this.channel = postal.channel();
        this.registeredSubriptions = [];

        this.publishScheduleSelectingEvent = function (data) {
            this.channel.publish(this.selectAppointmentScheduleSignal, data);
        };

        /**
         * register function to execute when receiving signal
         * @param {string} subscriber The name to register and use for unsubscription
         * @param callback
         * @returns {Number} the index of subscription
         */
        this.subscribeScheduleSelectingEvent = function (subscriber, callback) {
            var subscription = this.channel.subscribe(this.selectAppointmentScheduleSignal, function (data) {
                console.log('Receive schedule selecting event; Data: ', data);
                callback(data);
            });
            subscription.subscriber = subscriber;
            return this.registeredSubriptions.push(subscription);
        };

        /**
         * register function to execute once when receiving signal
         * @param callback
         */
        this.subscribeScheduleSelectingEventOnce = function (callback) {
            var subscription = this.channel.subscribe(this.selectAppointmentScheduleSignal, function (data) {
                console.log('Receive schedule selecting event; Data: ', data);
                callback(data);
                subscription.unsubscribe();
            });
        };

        this.publishDateTimeSelectingEvent = function (data) {
            this.channel.publish(this.selectDateTimeSignal, data);
        };

        /**
         * register function to execute when receiving signal
         * @param {string} subscriber The name to register and use for unsubscription
         * @param callback
         * @returns {Number} the index of subscription
         */
        this.subscribeDateTimeSelectingEvent = function (subscriber, callback) {
            var subscription = this.channel.subscribe(this.selectDateTimeSignal, function (data) {
                console.log('Receive date time selecting event; Data: ', data);
                callback(data);
            });
            subscription.subscriber = subscriber;
            return this.registeredSubriptions.push(subscription);
        };

        /**
         * register function to execute once when receiving signal
         * @param callback
         */
        this.subscribeDateTimeSelectingEventOnce = function (callback) {
            var subscription = this.channel.subscribe(this.selectDateTimeSignal, function (data) {
                console.log('Receive date time selecting event; Data: ', data);
                callback(data);
                subscription.unsubscribe();
            });
        };

        this.publishDetailsSelectingEvent = function (data) {
            this.channel.publish(this.selectDetailsSignal, data);
        };

        /**
         * register function to execute when receiving signal
         * @param {string} subscriber The name to register and use for unsubscription
         * @param callback
         * @returns {Number} the index of subscription
         */
        this.subscribeDetailsSelectingEvent = function (subscriber, callback) {
            var subscription = this.channel.subscribe(this.selectDetailsSignal, function (data) {
                console.log('Receive appointment details selecting event; Data: ', data);
                callback(data);
            });
            subscription.subscriber = subscriber;
            return this.registeredSubriptions.push(subscription);
        };

        /**
         * register function to execute once when receiving signal
         * @param callback
         */
        this.subscribeDetailsSelectingEventOnce = function (callback) {
            var subscription = this.channel.subscribe(this.selectDetailsSignal, function (data) {
                console.log('Receive appointment canceling event; Data: ', data);
                callback(data);
                subscription.unsubscribe();
            });
        };

        this.publishCancel = function (data) {
            this.channel.publish(this.cancelSignal, data);
        };

        /**
         * register function to execute when receiving signal
         * @param {string} subscriber The name to register and use for unsubscription
         * @param callback
         * @returns {Number} the index of subscription
         */
        this.subscribeCancel= function (subscriber, callback) {
            var subscription = this.channel.subscribe(this.cancelSignal, function (data) {
                console.log('Receive appointment canceling event; Data: ', data);
                callback(data);
            });
            subscription.subscriber = subscriber;
            return this.registeredSubriptions.push(subscription);
        };

        /**
         * register function to execute once when receiving signal
         * @param callback
         */
        this.subscribeCancelOnce = function (callback) {
            var subscription = this.channel.subscribe(this.cancelSignal, function (data) {
                console.log('Receive appointment details selecting event; Data: ', data);
                callback(data);
                subscription.unsubscribe();
            });
        };

        this.unsubscribeAll = function () {
            this.registeredSubriptions.forEach(function (sub) {
                sub.unsubscribe();
            });
            this.registeredSubriptions.clear();
        };

        this.unsubscribeByName = function (subscriber) {
            this.registeredSubriptions.forEach(function (sub) {
                if (subscriber == sub.subscriber) {
                    sub.unsubscribe();
                }
            });

            _.remove(this.registeredSubriptions, function (sub) {
                return subscriber == sub.subscriber;
            })
        }
    }
})(app);