"use strict";

app.constant("Configuration", (function () {
    var BASE_URL_APPOINTMENT ;
    var BASE_URL;

    var VERSION_API_APPOINTMENT = "v1.2";
    var VERSION = "v1.3";
    var AUTH = "CBHEwrDL71CJzsMo";
    var hostname = window.location.hostname;
    var localStorageName = {
        selectedDate: 'sp_selected_date'
    };

    var ERROR_CODE = {
        EMPLOYEE_FULL_SLOT: 40,
        SERVER_ERROR: 11,
        UNAUTHORIZED: 10
    };

    var APPOINTMENT_CODE = {
        APPROVED: 11,
        UNAPPROVED:10,
        ATSTORE: 21
    };

    var SERVER_SUCCESS_CODE = 0;

    BASE_URL_APPOINTMENT = "http://sunnypoint.jp/appointment/api/";
    BASE_URL = "https://sunnypoint.jp/api/";

    /* start-dev-block */
    if (typeof isDevEnvironment != 'undefined') {
        AUTH = 'abc';
        BASE_URL_APPOINTMENT = "http://test3.sunnypoint.jp/appointment/api/";
        BASE_URL = "https://test.sunnypoint.jp:8183/api/";
    }
    else if (hostname == "test3.sunnypoint.jp") {
        AUTH = "abc";
        BASE_URL_APPOINTMENT = "http://test3.sunnypoint.jp/appointment/api/";
        BASE_URL = "https://test.sunnypoint.jp:8183/api/";
    }
    else if (hostname == "staging.sunnypoint.jp") {
        AUTH = "abc";
        BASE_URL_APPOINTMENT = "http://staging.sunnypoint.jp/appointment/api/";
        BASE_URL = "https://test.sunnypoint.jp:8189/api/";
    }
    else if (hostname == "192.168.1.53") {
        AUTH = "abc";
        BASE_URL_APPOINTMENT = "http://192.168.1.122/appointment/api/";
        BASE_URL = "https://192.168.1.122:8183/api/";
    }
    else {
        AUTH = "abc";
        BASE_URL_APPOINTMENT = "http://" + hostname + "/appointment/api/";
        BASE_URL = "https://" + hostname + ":8183/api/";
    }
    /* end-dev-block */

    var api = {
        shopSettingInfo: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/shop/getShopSettingInfos",
        shopWokingInfo: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/listSchedulersInfosByShop",
        updateAppointmentInfo: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/updateAppointmentInfo",
        getAppointmentInfoById: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/getAppointmentInfoById?id=",
        getShopScheduleDetails: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/getShopScheduleDetails?shopId=",
        bookNewAppointment: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/bookNewAppointment?deviceId=0&channel=2",
        updateAppointment: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/updateAppointment?channel=2",

        cancelAppointment: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/cancelAppointment?appointmentId=",
        getAllAppointments: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/getAllAppointments?",

        findShopInfoList: BASE_URL + VERSION + "/shop/findShopInfoList?auth=" + AUTH,
        authenticate: BASE_URL + VERSION + "/shop/authenticate?auth=" + AUTH + "&authData=",
        getRegistrationInfo: BASE_URL + VERSION + "/registration/getRegistrationInfo?auth=" + AUTH,

        sendEmailInformAppointmentChanged: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + '/booking/announceAppointmentEmail'
    };

    var PRELOADED_ICON_IMAGES = [
        '../assets/images/merchant/no_schedule_setting.png'
        ,'../assets/images/merchant/popup_header.png'
        ,'../assets/images/merchant/day_off.png'
        ,'../assets/images/merchant/close.png'
        ,'../assets/images/merchant/arrow.png'
        ,'../assets/images/merchant/arrow-next.png'
        ,'../assets/images/merchant/CheckSel.png'
        ,'../assets/images/merchant/Check.png'
        ,'../assets/images/merchant/icon_update.png'
        ,'../assets/images/merchant/icon_delete.png'
        ,'../assets/images/merchant/checkbox_checked.png'
        ,'../assets/images/merchant/checkbox_checked_gray.png'
        ,'../assets/images/merchant/btn_next.png'
        ,'../assets/images/merchant/checkbox_unchecked.png'
        ,'../assets/images/merchant/btn_pre.png'
        ,'../assets/images/merchant/popup_check.png'
        ,'../assets/images/merchant/btn_edit.png'
        ,'../assets/images/merchant/btn_signout.png'
        ,'../assets/images/merchant/cross_icon.png'
        ,'../assets/images/merchant/btn_add.png'
        ,'../assets/images/merchant/btn_disable_pre.png'
        ,'../assets/images/merchant/btn_disable_next.png'
        ,'../assets/images/merchant/btn_back.png'
    ];


    return {
        api: api,
        VERSION_API_APPOINTMENT: VERSION_API_APPOINTMENT,
        localStorageName: localStorageName,
        ERROR_CODE:ERROR_CODE,
        SERVER_SUCCESS_CODE: SERVER_SUCCESS_CODE,
        APPOINTMENT_CODE: APPOINTMENT_CODE,
        AUTH: AUTH,
        PRELOADED_ICON_IMAGES:PRELOADED_ICON_IMAGES
    }
})());

