﻿'use strict';

var app = null, templateModule = null;

(function main() {
    templateModule = angular.module('templateModule', []);

    app = angular.module('SunnyPoint',
        [
            'templateModule',
            'ngRoute',
        ]);

    app.register = app;

    app.config(
        ['$routeProvider', config]);

    function config($routeProvider) {
        $routeProvider
            .when('/appointments', controllerResolver('AppointmentController', '../templates/merchant/appointment.html'))
            .when('/appointment/create/:selectedDate', controllerResolver('NewAppointmentController', '../templates/merchant/new_appointment.html'))
            .when('/appointment/create', controllerResolver('NewAppointmentController', '../templates/merchant/new_appointment.html'))
            .when('/appointment/edit/:appointmentId', controllerResolver('NewAppointmentController', '../templates/merchant/new_appointment.html'))
            .when('/login', controllerResolver('LoginController', '../templates/login.html'))
            .otherwise({
                redirectTo: '/login'
            });

        toastr.options = {
            timeOut: 1500,
            showEasing: 'linear',
            hideEasing: 'swing',
            preventDuplicates: false,
            positionClass: "toast-bottom-center"
        };
    }

    function controllerResolver(controller, template) {
        return {
            controller: controller,
            templateUrl: template,
            resolve: {
                i18n: ['i18nService', function (i18nService) {
                    return i18nService.registerInitCallback();
                }]
            }
        }
    }

    if (typeof isDevEnvironment != "undefined") {
        var textPlugin = function (url) {
            var plugin = '../bower_components/text/text!';
            return plugin + url;
        };

        var requireDeferred = Q.defer(), domDeferred = Q.defer(), templateDeferred = Q.defer();
        var promises = [requireDeferred.promise, domDeferred.promise, templateDeferred.promise];

        require(_.map(templates, textPlugin), function () {
            for (var i = 0; i < arguments.length; i++) {
                (function (url, template) {
                    templateModule.run(["$templateCache", function ($templateCache) {
                        $templateCache.put(url, template);
                    }]);
                }(templates[i], arguments[i]))
            }
            templateDeferred.resolve();
        });

        require(manifest, function () {
            requireDeferred.resolve();
        });

        $(document).ready(function () {
            domDeferred.resolve();
        });

        //bootstrap angular application after loading all components
        Q.all(promises).then(function () {
            angular.bootstrap(document, ['SunnyPoint']);
        });
    } else {
        angular.element(document).ready(function () {
            angular.bootstrap(document, ['SunnyPoint']);
        });
    }
}());
