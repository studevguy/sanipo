"use strict";

app.constant("Configuration", (function () {
    var BASE_URL_APPOINTMENT ;
    var BASE_URL;

    var VERSION_API_APPOINTMENT = "v1.1";
    var VERSION = "v1.3";
    var AUTH = "abc";
    var hostname = window.location.hostname;

    var localStorageName = {
        selectedDate: 'sp_selected_date'
    };

    BASE_URL_APPOINTMENT = "http://test3.sunnypoint.jp/appointment/api/";
    BASE_URL = "https://test.sunnypoint.jp:8183/api/";

    if (hostname=='192.168.1.122' || hostname=='vntest2' ) {
        BASE_URL_APPOINTMENT = "http://192.168.1.122/appointment/api/";
        BASE_URL = "https://192.168.1.122:8183/api/";
    }
    else if (hostname=='192.168.1.121' || hostname=='vntest1') {
        BASE_URL_APPOINTMENT = "http://192.168.1.121/appointment/api/";
        BASE_URL = "https://192.168.1.121:8183/api/";
    }
    else if (hostname == "test3.sunnypoint.jp") {
        BASE_URL_APPOINTMENT = "http://test3.sunnypoint.jp/appointment/api/";
        BASE_URL = "https://test.sunnypoint.jp:8183/api/";
    }
    else if (hostname == "staging.sunnypoint.jp") {
        BASE_URL_APPOINTMENT = "http://staging.sunnypoint.jp/appointment/api/";
        BASE_URL = "https://test.sunnypoint.jp:8189/api/";
    }
    else if (hostname == "sunnypoint.jp") {
        AUTH = "CBHEwrDL71CJzsMo";
        BASE_URL_APPOINTMENT = "http://sunnypoint.jp/appointment/api/";
        BASE_URL = "https://sunnypoint.jp/api/";
    }

    var api = {
        getListShopsInfo: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/shop/getListShopsInfo",
        shopSettingInfo: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/shop/getShopSettingInfos",
        shopWokingInfo: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/listSchedulersInfosByShop",
        updateAppointmentInfo: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/updateAppointmentInfo",
        getAppointmentInfoById: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/getAppointmentInfoById?id=",
        getShopScheduleDetails: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/getShopScheduleDetails?shopId=",
        bookNewAppointment: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/bookNewAppointment?deviceId=0&channel=2",
        updateAppointment: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/updateAppointment?channel=2",
        saveAppointment: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booker/saveAppointment",

        cancelAppointment: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/cancelAppointment?appointmentId=",
        getAllAppointments: BASE_URL_APPOINTMENT + VERSION_API_APPOINTMENT + "/booking/getAllAppointments?",

        findShopInfoList: BASE_URL + VERSION + "/shop/findShopInfoList?auth=" + AUTH,
        authenticate: BASE_URL + VERSION + "/shop/authenticate?auth=" + AUTH + "&authData=",
        getRegistrationInfo: BASE_URL + VERSION + "/registration/getRegistrationInfo?auth=" + AUTH,
    };


    return {
        api: api,
        localStorageName: localStorageName
    }
})());

