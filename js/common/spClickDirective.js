/**
 * Created by sangdao on 15/07/2015.
 */
"use strict";

app.directive("spClick", ['$parse', function ($parse) {
    return {
        restrict: "A",
        scope: false,
        compile: function (elm, attr) {
            var fn = $parse(attr.spClick);

            function preLink(scope, elm, attr) {

                function handler(event) {
                    fn(scope, {$event: event});
                    try {
                     scope.$digest();
                    } catch (err){
                    	console.log(err);
                    }
                   
                }

                $(elm).click(handler);
            }

            return {
                pre: preLink
            }
        }
    }
}]);
