/**
 * Created by sangdao on 14/07/2015.
 */
"use strict";

app.controller("AjaxBlockUIController", ['$element','$scope',function ($element, $scope) {
    var isApiCall = false;
    $($element).show();
    $(document).ajaxStop(function () {
        setTimeout(function () {
            $scope.showLoader = false;
            $scope.$digest();
        }, 500);
    });

    $(document).ajaxSend(function () {
        if (isApiCall) {
            $scope.showLoader = true;
        }
       setTimeout(function () {
           $scope.$digest();
       }, 0);
    });

    $.ajaxPrefilter(function (options, originalOptions) {
        isApiCall = false;
        var url = originalOptions.url;
        if (__checkApiCall(url)) {
            isApiCall = true;
        }
    });

    var __checkApiCall = function (url) {
        var isMIMERequest = __checkHTMLRequest(url);
        return !isMIMERequest;
    };

    var __checkHTMLRequest = function (url) {
        var strLength = url.length;
        if (strLength <= 5) {
            return false;
        }
        var lastFiveCharacters = url.substring(strLength - 5);
        return lastFiveCharacters == ".html"
    };
}]);