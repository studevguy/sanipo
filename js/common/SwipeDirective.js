/**
 * Created by sangdao on 15/07/2015.
 */
"use strict";

app.directive("ngSwipe", ['$parse', function ($parse) {
    return {
        restrict: "A",
        scope: false,
        compile: function (elm, attr) {
            var fn_left = $parse(attr.left);
            var fn_right = $parse(attr.right);
            var fn_up = $parse(attr.up);
            var fn_down = $parse(attr.down);

            function preLink(scope, elm, attr) {

                function handler(event, direction) {
                    var callback_left = function () {
                        fn_left(scope, {$event: event});
                    };

                    var callback_right = function () {
                        fn_right(scope, {$event: event});
                    };

                    var callback_up = function () {
                        fn_up(scope, {$event: event});
                    };

                    var callback_down = function () {
                        fn_down(scope, {$event: event});
                    };

                    if (direction == 'left') {
                        scope.$apply(callback_left);
                        return;
                    }

                    if (direction == 'right') {
                        scope.$apply(callback_right);
                        return;
                    }

                    if (direction == 'up') {
                        scope.$apply(callback_up());
                        return;
                    }

                    if (direction == 'down') {
                        scope.$apply(callback_down());
                    }
                }

                $(elm).swipe({
                    allowPageScroll: 'auto',
                    swipe: handler
                });
            }

            return {
                pre: preLink
            }
        }
    }
}]);