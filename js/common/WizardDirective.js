"use strict";

app.directive("ngWizard", ['Constants',function (constants) {
    return {
        restrict: "A",
        scope: false,
        priority: constants.DIRECTIVE_PRIORITY.PHRASE_RENDER_LOGIC,
        compile: function (elm, attr) {
            var $elm = $(elm);
            var $steps = $elm.find("div[wizard-step]");
            var maxStep = parseInt($steps.last().attr("wizard-step"));
            var minStep = parseInt($steps.first().attr("wizard-step"));

            //config diplay
            $elm.css("height","100%");
            $steps.each(function () {
                $(this).css("position", "absolute").css("height", "100%").css("width","100%").css("position", "top:0; left:0;");
                $(this).hide();
            });


            function postLink(scope, elm, attr) {
                var currentStep = attr.ngWizard;
                var stepName;
                var $step;
                var direction, reverseDirection;
                scope.$watch(currentStep, function (newVal, oldVal) {
                    if ((newVal < minStep) || newVal > maxStep) {
                        return false;
                    }

                    determineDirection(newVal, oldVal);

                    $steps.each(function () {
                        $step = $(this);
                        stepName = parseInt($step.attr("wizard-step"));
                        if (stepName == newVal) {
                            $step.show("slide", {direction: direction}, 400)
                        } else {
                            $step.hide("slide", {direction: reverseDirection}, 400);
                        }
                    });
                });

                function determineDirection(newVal, oldVal) {
                    if (newVal >= oldVal || oldVal == undefined) {
                        direction = "right";
                        reverseDirection = 'left';
                    } else {
                        direction = "left";
                        reverseDirection = 'right';
                    }
                }
            }

            return {
                post: postLink
            };
        }
    }
}]);