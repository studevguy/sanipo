"use strict";

app.directive("dateTimePicker", ['Constants','DateTimePickerService',function (constants, dateTimePickerService) {
    return {
        restrict: "A",
        compile: function (elm) {
            dateTimePickerService.registerElement(elm);
            $(elm).hide();
        }
    }
}]);