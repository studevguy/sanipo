"use strict";

(function (app) {
    app.directive("spFineTuneScroll", [FineTuneScrollDirective]);

    function FineTuneScrollDirective() {
        return {
            restrict: "A",
            scope: false,
            compile: function (elm) {
                return {
                    post: postLink
                }
            }
        };

        function postLink(scope, elm, attr) {
            var $elm, maxScroll, containerHeight, listHeight, scrollCursor;
            $elm = $(elm);

            $elm.scroll(function ($event) {
                containerHeight = $elm.height();
                listHeight = $elm.find('ul').height();
                scrollCursor = $elm.scrollTop();
                maxScroll = listHeight - containerHeight;
                if ((scrollCursor <= 0) && (maxScroll > 3)) {
                    $elm.scrollTop(3);
                    $event.preventDefault();
                } else if ((scrollCursor >= maxScroll) && (maxScroll > 3)){
                    $elm.scrollTop(maxScroll - 3);
                    $event.preventDefault();
                }
            });
        }
    }
})(app);