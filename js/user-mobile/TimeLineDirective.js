/**
 * Created by sangdao on 10/07/2015.
 */
"use strict";

app.directive("ngTimeLine", ['TimeLineService', function (timeLineService) {
    return {
        restrict: 'A',
        scope: false,
        compile: function (elm) {
            timeLineService.load(elm);

            function postLink(scope, elm, attr) {
                $(elm).click(function () {
                        timeLineService.removeHighlight();
                        timeLineService.highlightSelectedRange();
                        timeLineService.triggerSelectedEvent();
                });
            }

            return {
                post: postLink
            }
        }
    }
}]);