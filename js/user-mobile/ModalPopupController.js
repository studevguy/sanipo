"use strict";

app.controller("ModalPopupController", ['$scope','$modalInstance',   'i18nService' ,'params'  , function ($scope
    ,$modalInstance, i18nService,params) {

    $scope.i18n = i18nService.getLanguage("calendar");
    $scope.message = params.message;
    $scope.title = params.title;
    $scope.showConfirm = params.showConfirm;

    $scope.ok = function () {
        $modalInstance.close('ok');
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


