/**
 * Created by sangdao on 10/07/2015.
 */
"use strict";

app.service("TimeLineService", ['$templateCache', '$compile','AppointmentResourceService', function ($templateCache, $compile, appointmentResourceService) {
    var self = this, $ul, intervalPromise;
    var selectedEvent = function () {
    };
    var $myscope, $elm, shopWorkingTimeFrom_m, shopWorkingTimeTo_m, timeScale,
        staffWorkingFromTime_m, staffWorkingToTime_m, duration,
        selectedTimeFrom, selectedTimeTo, appointmentTime,
        lastApptTime, scheduleDate,employeeWorkingTimes;
    var time_block_partial = $templateCache.get('../templates/user-mobile/time_block_partial.html');

    var DATA_TIME_ATTR = 'data-time';

    this.load = function (elm) {
        $elm = $(elm);
    };

    this.render = function (options, $scope) {
        //clear content
        $elm.empty();
        $myscope =$scope;
        appointmentTime = options.appointmentTime;
        timeScale = options.timeScale;
        duration = options.serviceDuration;
        lastApptTime = options.lastApptTime;
        scheduleDate = options.scheduleDate;
        selectedEvent = options.callback;
        selectedTimeFrom = options.selectedTimeFrom || undefined;
        employeeWorkingTimes = options.employeeWorkingTimes;
        shopWorkingTimeFrom_m = moment(options.shopWorkingFromTime, 'HH:mm');
        shopWorkingTimeTo_m = moment(options.shopWorkingToTime, 'HH:mm');
        staffWorkingFromTime_m = moment(options.staffWorkingFromTime, 'HH:mm');
        staffWorkingToTime_m = moment(options.staffWorkingToTime, 'HH:mm');

        $ul = $('<ul></ul>');
        var $li;
        for (var i = shopWorkingTimeFrom_m.clone(); i.isSame(shopWorkingTimeTo_m) || i.isBefore(shopWorkingTimeTo_m); i.add(timeScale, 'minutes')) {
            $li = this.__renderTimeBlock(i.format('HH:mm'));
            $li.appendTo($ul);
        }

        $compile($ul)($scope);
        this.__disableAssignedTimeRegion();
        this.__disableUnavailableTime();
        this.__applyCurrentTime();
        this.__emphasizeLastApptTime();

        if (selectedTimeFrom) {
            this.initSelectedTimeRange(selectedTimeFrom);
            this.highlightSelectedRange();
        }

        $ul.appendTo($elm);
    };

    this.__disableUnavailableTime = function () {
        var $this, timeData_m;
        $ul.find('li').each(function () {
            $this = $(this);
            timeData_m = moment($this.attr('data-time'), 'HH:mm');
            if (timeData_m.isBefore(staffWorkingFromTime_m) || timeData_m.isSame(staffWorkingToTime_m) || timeData_m.isAfter(staffWorkingToTime_m)) {
                $this.addClass('disabled');
            }


            if (timeData_m.isBefore(shopWorkingTimeFrom_m) || timeData_m.isSame(shopWorkingTimeTo_m) || timeData_m.isAfter(shopWorkingTimeTo_m)) {
                $this.addClass('disabled');
            }
        });
    };

    this.__disableAssignedTimeRegion = function () {
        var $this, timeData_m;
        appointmentTime.forEach(function (item) {
            var apptTimeFrom_m = moment(item.timeFrom, 'HH:mm');
            var apptTimeTo_m = moment(item.timeTo, 'HH:mm');
            $ul.find('li').each(function () {
                $this = $(this);
                timeData_m = moment($this.attr('data-time'), 'HH:mm');
                if ((timeData_m.isSame(apptTimeFrom_m) || timeData_m.isAfter(apptTimeFrom_m))
                    && (timeData_m.isBefore(apptTimeTo_m))) {
                    $this.addClass('disabled');
                }
            })
        });
    };

    this.__applyCurrentTime = function () {
        var $this, dateTime, dateTime_m, now_m;

        function perform() {
            $ul.find('li').each(function () {
                $this = $(this);
                dateTime = scheduleDate + " " + $this.attr('data-time');
                dateTime_m = moment(dateTime, 'YYYY-MM-DD HH:mm');
                now_m = moment();
                $this.removeClass('current');

                //emphasize current time
                if ((now_m.isSame(dateTime_m) || now_m.isAfter(dateTime_m))
                    && (now_m.isSame(dateTime_m.clone().add(15, 'm')) || now_m.isBefore(dateTime_m.clone().add(15, 'm')))) {
                    $this.addClass('current');
                }

                //disable past time
                if (dateTime_m.isBefore(now_m)) {
                    $this.addClass('disabled');
                }
            })
        }

        clearInterval(intervalPromise);
        perform();
        intervalPromise = setInterval(perform, 60000);
    };

    this.__emphasizeLastApptTime = function () {
        $ul.find("li[data-time='" + lastApptTime + "']").addClass('last_appt_time');
    };

    this.__renderTimeBlock = function (time) {
        var $timeBlock = $(time_block_partial);
        $timeBlock.attr(DATA_TIME_ATTR, time);
        $timeBlock.find('.time_text').text(time);
        return $timeBlock;
    };

    this.setSelectedTimeRange = function (time) {
        if (!this.__isCanBookAtTime(time)) {
            toastr.warning($myscope.i18n.msg_booking_time_invalid);
            return false;
        }

        selectedTimeFrom = time;
        selectedTimeTo = this.__calculateStaffWorkingToTime(time);

        return true;
    };

    this.initSelectedTimeRange = function (time) {
        selectedTimeFrom=0;
        selectedTimeTo=0;

        if (!this.__isCanBookAtTime(time)) {
            return false;
        }

        selectedTimeFrom = time;
        selectedTimeTo = this.__calculateStaffWorkingToTime(time);

        return true;
    };

    this.__isCanBookAtTime = function (time) {
        if (!this.__isValidTime(time)) {
            return false;
        }
        if (!this.__isBeforeLastApptTime(time)) {
            return false;
        }
        var toTime = this.__calculateStaffWorkingToTime(time);
        if (!toTime) {
            return false;
        }

        if (!this.__isNotCoverAssignedTime(time, toTime)) {
            return false;
        }

        if(!appointmentResourceService.getFirstEmployeeCanBookOnTimes(employeeWorkingTimes, time, toTime))
        {
            return false;
        }

        return true;
    };

    this.__calculateStaffWorkingToTime = function (fromTime) {
        var toTime_m = moment(fromTime, 'HH:mm').add(duration, 'm');
        var toTime = toTime_m.format('HH:mm');

        if (!this.__isValidTime(toTime)) {
            return false
        } else {
            return toTime;
        }
    };

    this.__isValidTime = function (time) {
        return this.__isAvailableTime(time) && this.__isInStaffWorkingTime(time) && this.__isAfterCurrentTime(time)
            && this.__isInShopWorkingTime(time) ;
    };
    this.__isInShopWorkingTime = function (time) {
        var time_m = moment(time, 'HH:mm');
        return (time_m.isSame(shopWorkingTimeFrom_m) || time_m.isAfter(shopWorkingTimeFrom_m))
            && (time_m.isSame(shopWorkingTimeTo_m) || time_m.isBefore(shopWorkingTimeTo_m));
    };
    this.__isInStaffWorkingTime = function (time) {
        var time_m = moment(time, 'HH:mm');
        return (time_m.isSame(staffWorkingFromTime_m) || time_m.isAfter(staffWorkingFromTime_m))
            && (time_m.isSame(staffWorkingToTime_m) || time_m.isBefore(staffWorkingToTime_m));
    };

    this.__isAvailableTime = function (time) {
        var time_m = moment(time, 'HH:mm');
        var isInAssignedTime = false, timeFrom_m, timeTo_m;
        appointmentTime.forEach(function (item) {
            timeFrom_m = moment(item.timeFrom, 'HH:mm');
            timeTo_m = moment(item.timeTo, 'HH:mm');
            isInAssignedTime = isInAssignedTime || (time_m.isAfter(timeFrom_m))
                && (time_m.isBefore(timeTo_m));
        });
        return !isInAssignedTime;
    };

    this.__isAfterCurrentTime = function (time) {
        var dateTime = scheduleDate + ' ' + time;
        var dateTime_m = moment(dateTime, 'YYYY-MM-DD HH:mm');
        return dateTime_m.isAfter(moment());
    };

    this.__isNotCoverAssignedTime = function (selectedTimeFrom, selectedTimeTo) {
        var isCover = false, timeFrom_m, timeTo_m;
        var selectedTimeFrom_m = moment(selectedTimeFrom, 'HH:mm');
        var selectedTimeTo_m = moment(selectedTimeTo, 'HH:mm');
        appointmentTime.forEach(function (item) {
            timeFrom_m = moment(item.timeFrom, 'HH:mm');
            timeTo_m = moment(item.timeTo, 'HH:mm');

            isCover = isCover || ((timeFrom_m.isSame(selectedTimeFrom_m))
                && (timeTo_m.isSame(selectedTimeTo_m)));

            isCover = isCover || ((timeFrom_m.isAfter(selectedTimeFrom_m))
                && (timeFrom_m.isBefore(selectedTimeTo_m)));

            isCover = isCover || ((timeTo_m.isAfter(selectedTimeFrom_m))
                && (timeTo_m.isBefore(selectedTimeTo_m)));
        });
        return !isCover;
    };

    this.__isBeforeLastApptTime = function (time) {
        var time_m = moment(time, 'HH:mm');
        if (!lastApptTime) {
            return true
        }
        var lastApptTime_m = moment(lastApptTime, 'HH:mm');
        var after =time_m.isAfter(lastApptTime_m);
        return  !after;
    };

    this.__getSelectedTimeRange = function () {
        return {
            timeFrom: selectedTimeFrom,
            timeTo: selectedTimeTo
        }
    };

    this.triggerSelectedEvent = function () {
            if(this.__isCanBookAtTime(selectedTimeFrom)){
                selectedEvent(selectedTimeFrom, selectedTimeTo);
            }
    };

    this.highlightSelectedRange = function () {
        appointmentTimeRange = this.__getSelectedTimeRange();

        if (!this.__isCanBookAtTime(appointmentTimeRange.timeFrom)) {
            return false
        }

        var $timeBlocks = $ul.find('li');
        var timeData, timeData_m, $this;
        var appointmentTimeRange, timeFrom_m, timeTo_m;

        timeFrom_m = moment(appointmentTimeRange.timeFrom, 'HH:mm');
        timeTo_m = moment(appointmentTimeRange.timeTo, 'HH:mm');
        $timeBlocks.each(function () {
            $this = $(this);
            timeData = $this.attr('data-time');
            timeData_m = moment(timeData, 'HH:mm');
            if ((timeData_m.isSame(timeFrom_m) || timeData_m.isAfter(timeFrom_m))
                && (timeData_m.isBefore(timeTo_m))) {
                $this.find('.time_text').addClass('selected');
                $this.find('.horizontal_line').addClass('selected');
            }
        });
        $timeBlocks.filter("[data-time='" + appointmentTimeRange.timeTo + "']").find('.time_text').addClass('selected');
    };

    this.removeHighlight = function () {
        var $timeBlocks = $ul.find('li');
        $timeBlocks = $elm.find('li');
        $timeBlocks.find('.time_text').removeClass('selected');
        $timeBlocks.find('.horizontal_line').removeClass('selected');
    }

    this.isAllTimesDisable=function(){
        var isDisable = true
        var $this;
        $ul.find('li').each(function () {
          $this = $(this);
          if(!$this.hasClass('disabled')){
              isDisable=false;
              return isDisable
          }
        });
        return isDisable;
    }
}]);
