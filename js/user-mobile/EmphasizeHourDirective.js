/**
 * Created by sangdao on 13/07/2015.
 */
"use strict";

app.directive("ngEmphasizeHour", [function () {
    return {
            restrict: "A",
            scope: false,
            compile: function (scope, elm, attr) {
                function postLink (scope, elm, attr){
                    var dataTime = attr.time;
                    var dataTime_m  = moment(dataTime, 'HH:mm');
                    if (dataTime_m.minute() == 0) {
                        $(elm).addClass('emphasized');
                    }
                }

                return {
                    post: postLink
                }
            }
        }
}]);