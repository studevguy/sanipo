/**
 * Created by sangdao on 14/07/2015.
 */
"use strict";

app.directive("ngColumnScale", [function () {
    return {
        restrict: "A",
        scope: false,
        compile: function (elm, attr) {
            var $elm = $(elm);
            var $productsColumn = $elm.find('.products');
            var $employeeColumn = $elm.find('.employees');
            var $timeColumn = $elm.find('.times');
            $elm.swipe({
                allowPageScroll: 'auto',
                swipe: function ($event, direction) {
                    if (direction == 'right') {
                        expandServiceColumn();
                    } else if (direction == 'left') {
                        shrinkServiceColumn();
                    }
                }
            });

            function expandServiceColumn() {
                $timeColumn.hide();
                $employeeColumn.filter('th').removeClass('col-xs-4').addClass('col-xs-2');
                $productsColumn.filter('th').removeClass('col-xs-4').addClass('col-xs-10');

            }

            function shrinkServiceColumn() {
                $productsColumn.filter('th').removeClass('col-xs-10').addClass('col-xs-4');
                $employeeColumn.filter('th').removeClass('col-xs-2').addClass('col-xs-4');
                $timeColumn.show();
            }

        }
    }
}]);