'use strict';
app.controller('HistoryController',
    ['$scope',
        '$route',
        '$routeParams',
        '$modal',
        'AppointmentResourceService',
        'ShopResourceService',
        'i18nService',
        'LocalStorageService',
        'Constants',
        'UtilityService',

        function ($scope,
                  $route,
                  $routeParams,
                  $modal,
                  appointmentResourceService, shopResourceService,
                  i18nService, localStorageService, Constants, UtilityService) {
            $scope.i18n = i18nService.getLanguage("usermobile");

            var fn = {};
            $scope.fn = fn;
            var shopId = $routeParams.shopId;
            var info = localStorageService.retrieve(Constants.BOOKER_INFO);
            if(!info)
                info ={emailAddress: '', customerId: ''};
            var params = {email: info.emailAddress, customerId: info.customerId};
            var LIST = 1, DETAIL = 2;

            var FIRST_STEP = LIST;
            var LAST_STEP = DETAIL;

            $scope.currentStep = FIRST_STEP;


            $scope.steps = {
                list: LIST,
                detail: DETAIL
            };

            loadData();
            function loadData() {
                appointmentResourceService.getAllAppointments(params, finishGetAllAppointments);
            }


            function finishGetAllAppointments(response) {
                console.log("finishGetAllAppointments")
                console.log(response)

                response.currentFutureAppointment.forEach(function (item) {
                    var appointmentDetail = _.first(item.appointmentDetails);
                    if (appointmentDetail) {

                        item.employee = appointmentDetail.employee;
                       // item.canEdit = (item.status != 'CANCEL' && item.status != 'ATSTORE');
                        item.canEdit =false;
                    }
                    var productNames="";
                    item.appointmentDetails.forEach(function (detail) {
                            if(productNames=="")
                                productNames=detail.product.productName;
                            else
                                productNames=productNames+","+detail.product.productName;
                    });
                    item.productNames = productNames;
                });

                response.pastAppointment.forEach(function (item) {
                    var appointmentDetail = _.first(item.appointmentDetails);
                    if (appointmentDetail) {
                        item.employee = appointmentDetail.employee;
                        item.canEdit =false;
                    }
                    var productNames="";
                    item.appointmentDetails.forEach(function (detail) {
                        if(productNames=="")
                            productNames=detail.product.productName;
                        else

                            productNames=productNames+","+detail.product.productName;
                    });
                    item.productNames = productNames;
                });

                $scope.currentFutureAppointment = response.currentFutureAppointment;
                $scope.pastAppointment = response.pastAppointment;
                $scope.$digest();
            }

            $scope.fn.back = function () {
                var url = '#/m/shopper/';
                if (shopId)
                    url = '#/m/shopper/select/' + shopId;
                window.location = url;
            };

            $scope.fn.display = function (item) {
                $scope.item = item;

                var totalPrice=0;
                item.appointmentDetails.forEach(function (detail) {
                    totalPrice+=detail.product.price;
                    detail.product.priceFormat=  UtilityService.formatCurrency(detail.product.price);
                });

                $scope.totalPrice = UtilityService.formatCurrency(totalPrice);

                $scope.item.statusText = UtilityService.getStatusText($scope.item.status, $scope.i18n)
                $scope.currentStep = DETAIL;

            };

            $scope.fn.showHistory = function () {
                $scope.currentStep = LIST;
            };
            $scope.fn.cancelAppointment = function () {
                showConfirmCancel();
            };
            $scope.fn.gotoEdit = function () {
               var id= $scope.item.id;
                var shopId= $scope.item.shopId;

                var url = '#/m/shopper/edit/' + shopId+'/'+id;
                window.location = url;
            };

            function showSuccessDeleting() {

                toastr.warning($scope.i18n.msg_appointment_deleted_success);
                $scope.currentStep = LIST;
                loadData();
                $scope.$digest();

            }

            function showConfirmCancel() {
                var params = {};
                params.message = $scope.i18n.msg_appointment_confirm_deleted;
                params.title = $scope.i18n.title_popup_confirm;
                params.showConfirm = true;

                var modalInstance = $modal.open({
                    animation: true,
                    templateUrl: '../templates/popup.html',
                    controller: 'ModalPopupController',
                    resolve: {
                        params: function () {
                            return params;
                        }
                    }
                });

                modalInstance.result.then(function (selectedItem) {
                    console.info(selectedItem);
                    if (selectedItem == 'ok')
                        shopResourceService.cancelAppointment($scope.item.id).then(showSuccessDeleting)
                }, function () {


                });


            };
        }]);
