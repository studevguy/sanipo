'use strict';
app.controller('SelectShopController',
    ['$scope',
        '$route',
        '$routeParams',
        '$modal',
        'AppointmentResourceService',
        'ApiWebResourceService',
        'TimeLineService',
        'i18nService',
        'LocalStorageService',
        'Constants', 'Unicodes', 'UtilityService',
        function ($scope, $route, $routeParams, $modal,
                  appointmentResourceService,
                  apiWebResourceService,
                  timeLineService,
                  i18nService,
                  localStorageService,
                  Constants,
                  Unicodes,
                  UtilityService) {
            var YYYYMMDD = "YYYYMMDD";

            var SELECT_TIME = 1, CUSTOMER_INFO = 2, CONFIRM = 3;
            var ANY_ONE_ID = appointmentResourceService.ANY_ONE_ID;
            var anyone;
            var FIRST_STEP = SELECT_TIME;
            var LAST_STEP = CONFIRM;
            $scope.ANY_ONE_ID = ANY_ONE_ID;
            $scope.currentStep = FIRST_STEP;
            var previousStep = FIRST_STEP;
            $scope.activeButtonNextStep1 = false;
            $scope.activeButtonNextStep2 = false;
            $scope.i18n = i18nService.getLanguage("usermobile");
            var shopId = $routeParams.shopId;
            var customerId = $routeParams.customerId;
            var appointmentId = $routeParams.appointmentId;
            var firstLoad = true;
            var fn = {};
            $scope.fn = fn;

            $scope.steps = {
                selectTime: SELECT_TIME,
                customerInfo: CUSTOMER_INFO,
                confirm: CONFIRM
            };

            var appointmentForm = {};

            appointmentForm.info = {
                id: '',
                name: '',
                phoneNumber: '',
                emailAddress: '',
                scheduleDate: '',
                fromTime: '',
                toTime: '',
                notes: '',
                customerId: customerId
            };
            appointmentForm.products = [];
            var info = localStorageService.retrieve(Constants.BOOKER_INFO);
            if (info) {
                appointmentForm.info = info;
                customerId = info.customerId;
            } else {
                if (customerId) {
                    var params = {shopId: shopId, customerId: customerId};
                    apiWebResourceService.getRegistrationInfo(params, finishGetRegistrationInfo);
                }
            }

            function finishGetRegistrationInfo(response) {
                if (response.status == 0) {
                    appointmentForm.info.name = response.customerName;
                    appointmentForm.info.emailAddress = response.emailAddress;
                    appointmentForm.info.phoneNumber = response.telephone;
                }
            }

            $scope.displayDate = '';
            $scope.dateOfMonth = '';
            $scope.form = appointmentForm;

            $scope.fn.nextStep = function () {
                var currentStep = $scope.currentStep;
                previousStep = currentStep;
                if (validateDataAtStep(currentStep, true))
                    $scope.currentStep = currentStep + 1 <= LAST_STEP ? currentStep + 1 : currentStep;
            };

            $scope.fn.showHistory = function () {
                var url = '#/m/shopper/history/' + shopId;
                window.location = url;
            };

            $scope.fn.prevStep = function () {
                $scope.currentStep = previousStep;

                var currentStep = $scope.currentStep;
                previousStep = currentStep - 1 >= FIRST_STEP ? currentStep - 1 : currentStep;

            };

            $scope.fn.gotoStep = function (step) {
                $scope.currentStep = step;
                var currentStep = $scope.currentStep;
                previousStep = currentStep - 1 >= FIRST_STEP ? currentStep - 1 : currentStep;
            };
            $scope.fn.hideDatePicker = function () {
                $('#scheduleDate').datepicker('hide');
            };
            $scope.fn.showDatePicker = function (e) {
                $('#scheduleDate').datepicker('show');
                e.stopPropagation();
            };
            $scope.fn.gotoCustomerInfoOrConfirmStep = function () {
                var currentStep = $scope.currentStep;

                previousStep = $scope.currentStep;
                if (validateDataAtStep(currentStep, false))
                    currentStep = currentStep + 1 <= LAST_STEP ? currentStep + 1 : currentStep;
                if (validateDataAtStep(currentStep, false))
                    currentStep = currentStep + 1 <= LAST_STEP ? currentStep + 1 : currentStep;
                if (currentStep != $scope.currentStep)
                    $scope.currentStep = currentStep;
            };
            $scope.fn.createNewAppointment = function () {

                var data = {
                    shopId: shopId,
                    scheduleDate: appointmentForm.info.scheduleDate,
                    fromTime: appointmentForm.info.fromTime,
                    toTime: appointmentForm.info.toTime,
                    channel: 2,
                    notes: appointmentForm.info.notes
                };
                data.bookerInfo = {
                    displayName: appointmentForm.info.name,
                    phoneNumber: appointmentForm.info.phoneNumber,
                    emailAddress: appointmentForm.info.emailAddress
                };

                if (appointmentForm.info.customerId)
                    data.bookerInfo.customerId = appointmentForm.info.customerId;

                data.details = [];
                var detail = {};
                appointmentForm.products.forEach(function (product) {
                    detail = {};
                    detail.employeeId = appointmentForm.employee.id;
                    detail.productId = product.id;
                    data.details.push(detail);
                });

                appointmentResourceService.saveAppointment(angular.toJson(data), finishCreateNewAppointment);
            };

            function finishCreateNewAppointment(response) {
                console.log(response);
                console.log("-------------end of createNewAppointment---------------");
                var bookingStatus = response.bookingStatus;
                if (response.status == 0 && (bookingStatus == 'SUCCESS' || bookingStatus == 'SUCCESS_UNAPPROVED')) {

                    if (bookingStatus == 'SUCCESS')
                        $scope.message = $scope.i18n.msg_reservation_success_approved;
                    else
                        $scope.message = $scope.i18n.msg_reservation_success_unapproved;

                    showPopup(true);
                } else {
                    var msg = $scope.i18n.msg_reservation_error;
                    msg = msg.replace('{0}', bookingStatus);
                    msg = msg.replace('{1}', response.status);

                    $scope.message = msg;
                    showPopup(false);
                }
            }

            function showPopup(goHomeWhenClose, url) {
                var params = {};
                params.message = $scope.message;
                var modalInstance = $modal.open({
                    animation: true,
                    templateUrl: '../templates/popup.html',
                    controller: 'ModalPopupController',
                    resolve: {
                        params: function () {
                            return params;
                        }
                    }
                });
                if (goHomeWhenClose) {
                    modalInstance.result.then(function (selectedItem) {
                        //  console.info(selectedItem + new Date());
                    }, function () {
                        console.info('reload data at date ' + appointmentForm.info.scheduleDate);

                        localStorageService.store(Constants.BOOKER_INFO, appointmentForm.info);
                        if (url) {
                            window.location = url;
                        }
                        else {
                            $scope.currentStep = SELECT_TIME;
                            appointmentForm.info.fromTime = 0;
                            appointmentForm.info.toTime = 0;
                            reloadData(appointmentForm.info.scheduleDate);
                        }
                    });

                }
            };

            function validateDataAtStep(step, showMessage) {
                if (step == SELECT_TIME)
                    return $scope.activeButtonNextStep1;

                if (step == CUSTOMER_INFO) {
                    $scope.activeButtonNextStep2 = validateDataAtCustomerInfoScreen(showMessage);
                    return $scope.activeButtonNextStep2;
                }

                return true;
            }

            $scope.fn.selectEmployee = function ($event, item) {
                if (item.dayOff)
                    return false;
                if (item.disable)
                    return false;
                var target = $event.currentTarget;
                if ($(target).hasClass("disable"))
                    return false;

                if (appointmentForm.employee.id && appointmentForm.employee.id == item.id)
                    appointmentForm.employee = anyone;
                else
                    appointmentForm.employee = item;

                disableAndActiveItems();
            };
            $scope.fn.removeAllSpaces = function ($event) {
                var $target = $($event.currentTarget);
                var v = $.trim($target.val());
                v = v.split(' ').join('');
                $target.val(v);
            }

            $scope.fn.removeSpaces = function ($event) {
                var $target = $($event.currentTarget);
                var v = $.trim($target.val());
                $target.val(v);
            }

            $scope.fn.selectProduct = function ($event, item) {
                if (item.disable)
                    return false;
                item.active = item.active ? false : true;
                if (item.active)
                    appointmentForm.products.push(item);
                else {
                    _.remove(appointmentForm.products, function (n) {
                        return n.id == item.id;
                    });
                }


                disableAndActiveItems();
            };

            $scope.fn.previousDate = function () {
                if (!$scope.showPreDate)
                    return;

                var scheduleDate = $scope.scheduleDate;
                var date = moment(scheduleDate, 'YYYY/MM/DD');
                date.add(-1, 'days');
                scheduleDate = date.format('YYYY/MM/DD');

                reloadData(scheduleDate);
            };

            $scope.fn.nextDate = function () {
                if (!$scope.showNextDate)
                    return;
                var scheduleDate = $scope.scheduleDate;
                var date = moment(scheduleDate, 'YYYY/MM/DD');
                date = date.add(1, 'days');
                scheduleDate = date.format('YYYY/MM/DD');
                reloadData(scheduleDate);
            };

            function validateDataAtSelectTimeScreen() {
                if (appointmentForm.products.length == 0)
                    return false;

                if (!appointmentForm.employee.id)
                    return false;

                if (!appointmentForm.info.fromTime || appointmentForm.info.fromTime == '')
                    return false;

                var selectedTimeRange = timeLineService.__getSelectedTimeRange();
                if (!selectedTimeRange.timeFrom || selectedTimeRange.timeFrom == '0')
                    return false;

                //TODO: validate selected time range
                return true;
            }

            function trimSpaces() {

                if (!angular.isUndefined(appointmentForm.info.name)) {
                    var v = $.trim(appointmentForm.info.name);
                    v = v.split(' ').join('');
                    appointmentForm.info.name = v;
                }
                if (!angular.isUndefined(appointmentForm.info.phoneNumber)) {
                    var v = $.trim(appointmentForm.info.phoneNumber);
                    v = v.split(' ').join('');
                    appointmentForm.info.phoneNumber = v;
                }
                if (!angular.isUndefined(appointmentForm.info.emailAddress)) {
                    var v = $.trim(appointmentForm.info.emailAddress);
                    v = v.split(' ').join('');
                    appointmentForm.info.emailAddress = v;
                }
                if (!angular.isUndefined(appointmentForm.info.notes)) {
                    var v = $.trim(appointmentForm.info.notes);

                    appointmentForm.info.notes = v;
                }

            }

            function validateDataAtCustomerInfoScreen(showMessage) {
                trimSpaces();

                if (angular.isUndefined(appointmentForm.info.name) || UtilityService.getWordCount(appointmentForm.info.name) > 20) {
                    if (showMessage)
                        toastr.warning($scope.i18n.msg_invalid_maxlength_bookerName);
                    return false;
                }
                if (appointmentForm.info.name == '') {
                    if (showMessage)
                        toastr.warning($scope.i18n.msg_required_name);
                    return false;
                }

                var tmp = UtilityService.getInvalidUnicode(appointmentForm.info.name, Unicodes.chars)
                if (tmp) {
                    var msg = $scope.i18n.msg_invalid_customer_name;
                    msg = msg.replace('%1', tmp);
                    if (showMessage)
                        toastr.warning(msg);
                    return false;
                }

                if (angular.isUndefined(appointmentForm.info.phoneNumber) ||
                    UtilityService.getWordCount(appointmentForm.info.phoneNumber) > 17) {
                    if (showMessage)
                        toastr.warning($scope.i18n.msg_invalid_maxlength_phoneNumber);
                    return false;
                }
                if (appointmentForm.info.phoneNumber == '') {
                    if (showMessage)
                        toastr.warning($scope.i18n.msg_required_phone);
                    return false;
                }

                if (angular.isUndefined(appointmentForm.info.emailAddress)) {
                    if (showMessage)
                        toastr.warning($scope.i18n.msg_invalid_email);
                    return false;
                }

                if (appointmentForm.info.emailAddress != '') {

                    if (
                        UtilityService.getWordCount(appointmentForm.info.emailAddress) > 200) {
                        if (showMessage)
                            toastr.warning($scope.i18n.msg_invalid_maxlength_email);
                        return false;
                    }
                }

                if (angular.isUndefined(appointmentForm.info.notes) ||
                    UtilityService.getWordCount(appointmentForm.info.notes) > 200) {
                    if (showMessage)
                        toastr.warning($scope.i18n.msg_invalid_maxlength_notes);
                    return false;
                }
                return true;
            }

            function disableAndActiveItems() {
                disableAndActiveProducts();
                disableAndActiveEmployes();

                renderTimeLine();
                $scope.activeButtonNextStep1 = validateDataAtSelectTimeScreen();
                getTotalPriceOfSelectedProducts();
                setHeaderClass();
            }

            function disableAndActiveProducts() {
                var enable = true;
                $scope.products.forEach(function (item) {
                    if (appointmentForm.employee.id)
                        enable = appointmentResourceService.isEmployeeAndProductHaveRelation(appointmentForm.employee.id, item.id);

                    item.disable = !enable;
                    item.active = false;
                    if (!item.disable && isProductAlreadySelected(item)) {
                        item.active = true;
                    }
                });
            }

            function isProductAlreadySelected(product) {
                var res = false;
                appointmentForm.products.forEach(function (item) {
                    if (product.id == item.id) {
                        res = true;
                        return true;
                    }
                });
                return res;
            }

            function roundDuration(duration, block) {
                var r = duration % block;
                if (r == 0)
                    return duration;
                return (duration - r) + block;
            }

            function getDurationOfSelectedProducts() {
                var duration = 0;
                var block = $scope.shopInfo.timeBlock;
                appointmentForm.products.forEach(function (product) {
                    duration += product.duration;
                });
                if (duration == 0)
                    return block;
                return roundDuration(duration, block);
            }

            function getTotalPriceOfSelectedProducts() {
                var totalPrice = 0;
                var duration = 0;
                appointmentForm.products.forEach(function (product) {
                    totalPrice += product.priceOriginal;
                    duration += product.duration;
                });
                var selectedTimeFrom = appointmentForm.info.fromTime;
                if (selectedTimeFrom && selectedTimeFrom != '') {
                    var fromTime = moment(selectedTimeFrom, 'HH:mm');
                    var toTime = fromTime.clone();
                    toTime.add(duration, 'm');
                    appointmentForm.info.toTime = toTime.format('HH:mm');
                }

                $scope.totalPrice = UtilityService.formatCurrency(totalPrice);
                return totalPrice;
            }

            function isEmployeeHaveRelationWithSelectedProducts(employee) {
                var res = true;
                var haveRelation = true;
                appointmentForm.products.forEach(function (product) {
                    haveRelation = appointmentResourceService.isEmployeeAndProductHaveRelation(employee.id, product.id);
                    if (!haveRelation) {
                        res = false;
                        return false;
                    }
                });
                return res;
            }

            function disableAndActiveEmployes() {
                var enable = true;

                var timeFrom = appointmentForm.info.fromTime;
                var timeTo = appointmentForm.info.toTime;
                var anyoneDisable = true;
                $scope.employeeWorkingTimes.forEach(function (item) {
                    enable = true;
                    if (item.dayOff) {
                        item.active = false;
                        item.disable = true;
                    }
                    else {
                        enable = isEmployeeHaveRelationWithSelectedProducts(item);
                        item.disable = !enable;
                        item.active = false;

                        if (enable && appointmentForm.employee.id && appointmentForm.employee.id == item.id) {
                            item.active = true;
                        }

                        if (anyoneDisable && enable && item.id != ANY_ONE_ID) {
                            anyoneDisable = false;
                        }

                    }
                });
                if (anyoneDisable) {
                    anyone.disable = true;
                    anyone.active = false;
                    appointmentForm.employee = anyone;
                }
            }

            function onTimeRangeSelecting(timeFrom, timeTo) {
                if (appointmentForm.info.fromTime == timeFrom) {
                    appointmentForm.info.fromTime = '';
                    appointmentForm.info.toTime = '';
                    renderTimeLine();
                } else {
                    appointmentForm.info.fromTime = timeFrom;
                    appointmentForm.info.toTime = timeTo;
                }

                $scope.activeButtonNextStep1 = validateDataAtSelectTimeScreen();

                setHeaderClass();
                $scope.$digest();

            }

            var date = $routeParams.date == null ? "" : $routeParams.date;
            appointmentForm.employee = {};

            reloadData(date);
            function reloadData(dateInput) {

                var params = {shopId: shopId, date: dateInput};
                if (appointmentId)
                    params = {shopId: shopId, date: dateInput, appointmentId: appointmentId}

                console.log(params);
                $scope.error = false;
                $scope.message = "";
                $scope.scheduleDate = dateInput;

                $scope.datesOff = [];
                $scope.employeeWorkingTimes = [];
                $scope.products = [];
                $scope.shopInfo = {};
                $scope.employeeProductRelations = [];

                appointmentResourceService.getShopScheduleDetails(params, finishGetShopScheduleDetails);

            }

            function validateAndResetSelectTime(appointmentTime) {

                var duration = getDurationOfSelectedProducts();

                var selectedTimeFrom = appointmentForm.info.fromTime;
                if (selectedTimeFrom && selectedTimeFrom != '') {
                    var fromTime = moment(selectedTimeFrom, 'HH:mm');
                    var toTime = fromTime.clone();
                    toTime.add(duration, 'm');

                    if (appointmentForm.employee.id) {
                        var valid = appointmentResourceService.isEmployeeCanBookOnTimes(appointmentForm.employee, fromTime.format('HH:mm'), toTime.format('HH:mm'), appointmentTime);
                        if (!valid) {
                            toastr.warning($scope.i18n.msg_booking_time_invalid);
                            appointmentForm.info.fromTime = '';
                            appointmentForm.info.toTime = '';
                        }

                    }
                }
            }

            function renderTimeLine() {
                if ($scope.shopInfo.dayOff)
                    return;
                var duration = getDurationOfSelectedProducts();

                var staffWorkingFromTime = $scope.shopInfo.startTime ? $scope.shopInfo.startTime : '07:00';
                var staffWorkingToTime = $scope.shopInfo.endTime ? $scope.shopInfo.endTime : '18:00';

                var appointmentTime = [];
                anyone.booktimes = appointmentResourceService.getBookeTimesOfAnyOne($scope.shopInfo, $scope.scheduleDate, $scope.employeeWorkingTimes);


                anyone.booktimes.forEach(function (item) {
                    var time = {
                        timeFrom: item.fromTime,
                        timeTo: item.toTime,
                    };
                    appointmentTime.push(time);
                });

                if (appointmentForm.employee.id) {
                    staffWorkingFromTime = appointmentForm.employee.fromTime;
                    staffWorkingToTime = appointmentForm.employee.toTime;
                    appointmentForm.employee.booktimes.forEach(function (item) {
                        var time = {
                            timeFrom: item.fromTime,
                            timeTo: item.toTime
                        };
                        appointmentTime.push(time);
                    });
                }

                appointmentTime = _.uniq(appointmentTime, function (i) {
                    return i.timeFrom + "_" + i.timeTo;
                })

                validateAndResetSelectTime(appointmentTime);
                var selectedTimeFrom = appointmentForm.info.fromTime;

                var timeLineOptions = {
                    shopWorkingFromTime: $scope.shopInfo.startTime,
                    shopWorkingToTime: $scope.shopInfo.endTime,
                    staffWorkingFromTime: staffWorkingFromTime,
                    staffWorkingToTime: staffWorkingToTime,
                    timeScale: $scope.shopInfo.timeBlock,
                    serviceDuration: duration,
                    employeeName: appointmentForm.employee.fullName,
                    lastApptTime: $scope.shopInfo.lastApptTime,
                    scheduleDate: appointmentForm.info.scheduleDate,
                    selectedTimeFrom: selectedTimeFrom,
                    appointmentTime: appointmentTime,
                    employeeWorkingTimes: $scope.employeeWorkingTimes,
                    callback: onTimeRangeSelecting
                };

                timeLineService.render(timeLineOptions, $scope);
            }

            function initCalendar() {

                var mindate = '+' + $scope.shopInfo.acceptanceToDay;
                var maxDate = '+' + $scope.shopInfo.acceptanceFromDay;

                var arrayDisableDates = $scope.datesOff;
                var options = {
                    dateFormat: "yy/mm/dd",
                    buttonImageOnly: false,
                    changeMonth: false,
                    changeYear: false,
                    yearRange: '-0:+1',
                    minDate: mindate,
                    maxDate: maxDate,
                    showAnim: 'drop',
                    currentText: $scope.i18n.today,
                    closeText: $scope.i18n.label_close,
                    dayNamesMin: $scope.i18n.weekdayNamesShort,
                    monthNames: $scope.i18n.monthNames,
                    monthNamesShort: $scope.i18n.monthNames,
                    showButtonPanel: true,
                    beforeShowDay: function (date) {
                        var string = jQuery.datepicker.formatDate('yy/mm/dd', date);
                        var disable = arrayDisableDates.indexOf(string) == -1;

                        var dayOfWeek = date.getUTCDay();
                        if (dayOfWeek == 5) //Saturday
                            return [disable, 'blue', ''];
                        if (dayOfWeek == 6) //Sunday
                            return [disable, 'red', ''];
                        return [disable];
                    },
                    beforeShow: function (input, inst) {
                    },
                    onSelect: function (dateText) {

                        reloadData(dateText);
                    }
                };
                $('#scheduleDate').datepicker(options);

            }

            function setDateCalendarHeader() {
                var scheduleDate = $scope.scheduleDate;
                var date = moment(scheduleDate, 'YYYY/MM/DD');
                var dateOfMonth = date.date();

                var dayOfWeek = date.day();
                var month = date.month();
                var weekdayName = $scope.i18n.weekdayNamesShort[dayOfWeek];
                var monthName = $scope.i18n.monthNames[month];

                var currentDate = moment();
                var tomorrow = moment().add(1, 'days');
                var displayDate = "";


                var strDate = date.format(YYYYMMDD);
                var strCurrentDate = currentDate.format(YYYYMMDD);
                var strTomorrow = tomorrow.format(YYYYMMDD)
                if (strDate == strCurrentDate)
                    displayDate = $scope.i18n.today;//today resource
                else if (strDate == strTomorrow)
                    displayDate = $scope.i18n.tomorrow;//tomorrow
                else {
                    if (strDate == strCurrentDate) {
                        displayDate = "(" + weekdayName + ")";
                    } else {
                        displayDate = monthName + " (" + weekdayName + ")";
                    }
                }
                $scope.displayDate = displayDate;
                $scope.dateOfMonth = dateOfMonth;

            }

            function isAllOfProudctDisable() {
                var products = $scope.products;
                var disable = true;
                products.forEach(function (item) {
                    if (!item.disable) {
                        disable = false;
                        return disable;
                    }
                });
                return disable;
            }

            function setHeaderClass() {

                $scope.headerTimeClass = 'headerDisable';
                $scope.headerProductClass = 'headerGuide';
                $scope.headerEmployeeClass = 'headerActive';

                if (anyone.disable)
                    $scope.headerEmployeeClass = 'headerDisable';


                var allProudctsDisable = isAllOfProudctDisable();
                if (allProudctsDisable)
                    $scope.headerProductClass = 'headerDisable';
                else if (appointmentForm.products.length > 0) {
                    $scope.headerProductClass = 'headerActive';
                }

                var allTimesDisable = timeLineService.isAllTimesDisable();
                if (allTimesDisable) {
                    $scope.headerTimeClass = 'headerDisable';
                    return;
                }
                var validTime = true;
                if (!appointmentForm.info.fromTime || appointmentForm.info.fromTime == '')
                    validTime = false;
                else {
                    var selectedTimeRange = timeLineService.__getSelectedTimeRange();
                    if (!selectedTimeRange.timeFrom || selectedTimeRange.timeFrom == '0')
                        validTime = false;
                }

                if (validTime) {
                    $scope.headerTimeClass = 'headerActive';
                } else {

                    if ($scope.headerProductClass == 'headerActive') {
                        $scope.headerTimeClass = 'headerGuide';
                    } else {
                        $scope.headerTimeClass = 'headerDisable';
                    }
                }

            }

            function setDisablePreviosAndNextButton() {
                $scope.showPreDate = appointmentResourceService.isShopAcceptableBookingTime($scope.datesOff, $scope.shopInfo, $scope.scheduleDate, true);
                $scope.showNextDate = appointmentResourceService.isShopAcceptableBookingTime($scope.datesOff, $scope.shopInfo, $scope.scheduleDate, false);

            }

            function finishGetShopScheduleDetails(response) {
                console.log("--finishGetShopScheduleDetails----");
                console.log(response)
                if (response.status != 0) {
                    $scope.error = true;
                    var msg = $scope.i18n.msg_shop_is_not_configured
                    msg = msg.replace('{0}', shopId);
                    $scope.message = msg;

                } else {

                    $scope.shopInfo = response.shopSettings;
                    $scope.datesOff = response.datesOff;

                    $scope.employeeWorkingTimes = response.employeeWorkingTimes;
                    $scope.products = response.products;
                    $scope.shopInfo = response.shopSettings;
                    $scope.showMsgFromShop = $scope.shopInfo.msgFromShop != null;
                    $scope.employeeProductRelations = response.employeeProductRelations;
                    $scope.scheduleDate = response.date;
                    $scope.products.forEach(function (item) {
                        item.priceOriginal = item.price;
                        item.price = UtilityService.formatCurrency(item.price);
                    });
                    appointmentForm.info.scheduleDate = $scope.scheduleDate;
                    anyone = _.find($scope.employeeWorkingTimes, function (item) {
                        return item.id == ANY_ONE_ID;
                    });
                    anyone.fullName = $scope.i18n.label_anyone;

                    //make anyone is selected as default
                    anyone.disable = false;
                    anyone.active = true;
                    anyone.anyone = true;
                    if (appointmentForm.employee.id) {
                        appointmentForm.employee = _.find($scope.employeeWorkingTimes, function (item) {
                            return item.id == appointmentForm.employee.id;
                        });
                    } else
                        appointmentForm.employee = anyone;

                    /*    if (appointmentForm.product.id) {
                     appointmentForm.product = _.find($scope.products, function (item) {
                     return item.id == appointmentForm.product.id;
                     });
                     }
                     */
                    setDateCalendarHeader();
                    initCalendar();
                    setDisablePreviosAndNextButton();
                    if (!$scope.shopInfo.dayOff) {

                        disableAndActiveItems();
                    }
                }
                $scope.$digest();
            }
        }]);
