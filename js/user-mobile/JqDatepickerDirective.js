"use strict";

app.directive("ngDatepicker", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, elem, attrs, ngModelCtrl) {
            var updateModel = function (dateText) {
                scope.$apply(function () {
                    ngModelCtrl.$setViewValue(dateText);
                });
            };

            var arrayDisableDates = scope.datesOff;
            console.log(arrayDisableDates);
            var options = {
                dateFormat: "yy/mm/dd",
                buttonImageOnly : false,
                changeMonth : false,
                changeYear : false,
                yearRange : '-0:+1',
                minDate : '+0',
                maxDate: '+35',
                showAnim : 'drop',
                currentText : 'Current',
                closeText : 'Close',
                showButtonPanel : true,
                beforeShowDay: function(date){
                    var string = jQuery.datepicker.formatDate('yy/mm/dd', date);
                    var disable=arrayDisableDates.indexOf(string) == -1;

                    var dayOfWeek = date.getUTCDay();
                    if(dayOfWeek==5) //Saturday
                        return [disable, 'blue' , ''];
                    if(dayOfWeek==6) //Sunday
                        return [disable, 'red' , ''];
                    return [disable];
                },
                beforeShow: function(input, inst) {
                    // $(inst).center() // not sure if this works
                   // $('.ui-datepicker').center() // this should work
                },
                onSelect: function (dateText) {
                    updateModel(dateText);
                }
            };
            elem.datepicker(options);
        }

    }
});