/**
 * Created by sangdao on 13/07/2015.
 */
"use strict";

app.directive("ngTimeLineSelecting", ['TimeLineService', function (timeLineService) {
    return {
            restrict: "A",
            scope: false,
            compile: function (elm, attr) {
                function postLink(scope, elm, attr){
                    var $elm = $(elm);
                    var selectedTimeFrom;

                    $elm.click(function () {
                        selectedTimeFrom = attr.time;
                        timeLineService.setSelectedTimeRange(selectedTimeFrom);
                    });
                }

                return {
                    post: postLink
                }
            }
        }
}]);