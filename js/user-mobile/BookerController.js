'use strict';
app.controller('BookerController',
    ['$scope',
     'ApiWebResourceService',
     'i18nService',

        function ($scope,
                  ApiWebResourceService,
                  i18nService
        ) {
            $scope.i18n = i18nService.getLanguage("usermobile");

            ApiWebResourceService.findShopInfoList().then(function(data){
                    $scope.shops= data.shops;
                    $scope.$digest();
            });


            $scope.showHistory=function(){
                var url= '#/m/shopper/history';
                window.location = url;
            }
        }]);
