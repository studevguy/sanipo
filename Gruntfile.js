module.exports = function (grunt) {
    'use strict';

    var feature = grunt.option("feature");

    var today = new Date();

    var project = {
        bowerComponent: 'bower_components/',
        scriptSrc: 'js/',
        templateSrc: 'templates/',
        lessSrc: 'assets/less/main.less',
        cssDir: 'assets/css/',
        dirs: {},
        buildTracking: '' + today.getFullYear() + today.getMonth() + today.getDate() + today.getHours() + today.getMinutes(),
        files: {
            scripts: 'js/',
            vendor: '/scripts/vendor',
            any: '/**/*',
            thisDir: '/',
            dot: {
                javascript: '.js',
                html: '.html'
            }
        }
    };


    var booker_dirs = {
        root: 'booker/',
        temp: 'booker/temp/'
    };


    var templates_booker = require('./grunt/config/templates-booker.js');

    var manifest_booker = require('./grunt/config/manifest-booker.js');


    console.log('build for booker app');
    console.log(templates_booker);
    console.log(manifest_booker);
    project.feature = 'booker';
    project.dirs = booker_dirs;
    project.files.indexHtml = 'booker/booker.html';
    project.files.mainJs = 'main-booker.js';
    project.files.mainJs = 'main-booker.js';
    project.files.templates = templates_booker;
    project.files.manifest = manifest_booker;


    var config = {
        pkg: grunt.file.readJSON('package.json'),
        project: project
    };

    grunt.initConfig(config);

    // Read config files from the `grunt/config/` folder
    grunt.file.expand('grunt/config/*.js').forEach(function (path) {
        var property = /grunt\/config\/(.+)\.js/.exec(path)[1],
            module = require('./' + path);
        config[property] = typeof module === 'function' ? module(grunt) : module;
    });

    // Load development dependencies specified in package.json
    for (var dependency in config.pkg.devDependencies) {
        if (/^grunt-/.test(dependency)) {
            grunt.loadNpmTasks(dependency);
        }
    }

    // Load tasks from the `grunt-tasks/` folder
    grunt.loadTasks('grunt/tasks');
};
