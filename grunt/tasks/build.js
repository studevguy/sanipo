module.exports = function (grunt) {
    grunt.registerTask('build', [
        'clean:default',
        'less:build',
        'processhtml',
        'useref',
        'concat',
        'ngtemplates:templateModule',
        'copy:uglify',
        'uglify',
        'tags:build2',
        'copy:finalize',
        'clean:finalize'
    ]);
};