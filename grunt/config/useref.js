/**
 * Created by sangdao on 30/06/2015.
 */
module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');

    return {
            html: project.dirs.root + 'index.html',
            temp: project.dirs.root
    };
};