/**
 * Created by sangdao on 30/06/2015.
 */
module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');

    return {
        default: [
            project.dirs.root + "css",
            project.dirs.root + "fonts",
            project.dirs.root + "js",
            project.cssDir + project.feature + '.*.min.css'

        ],
        finalize: [
            project.dirs.root + 'temp'
        ]
    };
};