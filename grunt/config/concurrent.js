/**
 * Created by kevin on 2/9/15.
 */
module.exports = function (grunt) {
    return {
        'dev-deploy': {
            tasks: ['http-server','watch:less'],
            options: {
                'logConcurrentOutput': true
            }
        }
    };
}
