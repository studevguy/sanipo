/**
 * Created by sangdao on 30/06/2015.
 */
module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');


    return {
        templateModule: {
            src: project.files.templates,
            dest: project.dirs.root + 'js/templates.' + project.buildTracking + '.js',
            options: {
                url: function (url) {
                    return '../' + url;
                }
            }
        }
    };
};