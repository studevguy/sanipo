/**
 * Created by kevin on 12/9/14.
 */
module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');

    var files_build = {};

    files_build['assets/css/'+ project.feature +'.' + project.buildTracking + '.min.css'] = project.lessSrc;

    return {
        'compile': {
            'options': {
                'paths': ['assets/', '/'],
                'cleancss': true,
                'optimize': 0,
                'strictImports': true,
                'strictMath': true,
                'strictUnits': true,
                'compress': true,
                'report': 'min'
            },
            'files': {
                'assets/css/merchant.min.css': 'assets/less/main.less'
            }
        },

        'build': {
            'options': {
                'paths': ['assets/', '/'],
                'cleancss': true,
                'optimize': 0,
                'strictImports': true,
                'strictMath': true,
                'strictUnits': true,
                'compress': true,
                'report': 'min'
            },
            'files': files_build
        }
    };
};