/**
 * Created by sangdao on 30/06/2015.
 */
module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');

    return {
        build2: {
            options: {
                scriptTemplate: '<script type="text/javascript" src="{{ path }}"></script>',
                openTag: '<!-- start template tags -->',
                closeTag: '<!-- end template tags -->'
            },
            src: [

                project.dirs.root + 'js/app.'+project.buildTracking+'.js',
                project.dirs.root + 'js/templates.' + project.buildTracking + '.js'
            ],
            dest: project.dirs.root + 'index.html'
        }
    };
};