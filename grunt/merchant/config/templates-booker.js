var templates_booker = [
    'templates/user-mobile/shop_list.html',
    'templates/user-mobile/shop_select.html',
    'templates/user-mobile/select_time.html',
    'templates/user-mobile/customer_info.html',
    'templates/user-mobile/confirm.html',
    'templates/user-mobile/error.html',
    'templates/user-mobile/time_block_partial.html',
    'templates/user-mobile/history.html',
    'templates/user-mobile/history_list.html',
    'templates/user-mobile/display.html',
    'templates/pgloading.html',
    'templates/popup.html'
];

module.exports = templates_booker;