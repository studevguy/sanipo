/**
 * Created by sangdao on 30/06/2015.
 */
module.exports = function (grunt) {
    'use strict';

    var files = {}, project = grunt.config.get('project');

    files[project.dirs.root + 'index.html'] = [project.files.indexHtml];

    return {
        default: {
            files: files
        }
    };
};