var templates_merchant = [
    '../templates/merchant/week_list.html',
    '../templates/merchant/select_service.html',
    '../templates/merchant/select_staff.html',
    '../templates/merchant/select_time.html',
    '../templates/merchant/fill_details.html',
    '../templates/merchant/time_cell_partial.html',
    '../templates/merchant/new_appointment.html',
    '../templates/merchant/appointment.html',
    '../templates/merchant/update_success_popup.html',
    '../templates/merchant/signout_popup.html',
    '../templates/merchant/cancel_popup.html',
    '../templates/merchant/employee_filter.html',

    '../templates/login.html',
    '../templates/pgloading.html'
];

module.exports = templates_merchant;