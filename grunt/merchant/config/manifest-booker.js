var templates_booker = [
    'js/main-booker.js',

    'js/common/JSPrototypeExtension.js',
    'js/common/Constants.js',
    'js/common/Unicodes.js',
    'js/common/Configuration.js',
    'js/common/ConfigMomentLocale.js',
    'js/common/AuthenticationRequest.js',
    'js/common/InitApplicationData.js',
    'js/common/WizardDirective.js',
    'js/common/TemplateUrlDirective.js',
    'js/common/ProcessAttributeDirective.js',
    'js/common/AjaxBlockUIController.js',
    'js/common/SwipeDirective.js',
    'js/common/DateTimePickerService.js',
    'js/common/DateTimePickerDirective.js',
    'js/common/VisibilityDirective.js',

    'js/services/AppointmentResourceService.js',
    'js/services/LocalStorageService.js',
    'js/services/i18nService.js',
    'js/services/AjaxService.js',
    'js/services/ShopResourceService.js',
    'js/services/PopupService.js',
    'js/services/ApiWebResourceService.js',
    'js/services/UtilityService.js',


    'js/user-mobile//ModalPopupController.js',
    'js/user-mobile/BookerController.js',
    'js/user-mobile/SelectShopController.js',
    'js/user-mobile/TimeLineDirective.js',
    'js/user-mobile/TimeLineService.js',
    'js/user-mobile/EmphasizeHourDirective.js',
    'js/user-mobile/TimeLineSelectingDirective.js',
    'js/user-mobile/ColumnScaleDirective.js',
    'js/user-mobile/HistoryController.js',
];

module.exports = templates_booker;