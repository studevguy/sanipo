/**
 * Created by kevin on 2/9/15.
 */
module.exports = function (grunt) {
    return {
        dev: {
            root: '.',
            port: '6789',
            host: "0.0.0.0",
            cache: 0, // no cache!
            showDir: false,
            autoIndex: false,
            runInBackground: false
        }
    };
};