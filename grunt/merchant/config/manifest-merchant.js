var manifest_merchant = [
    '../js/merchant/common/JSPrototypeExtension',
    '../js/merchant/common/Constants',
    '../js/merchant/common/Configuration',
    '../js/merchant/common/ConfigMomentLocale',
    '../js/merchant/common/AuthenticationRequest',
    '../js/merchant/common/InitApplicationData',
    '../js/merchant/common/WizardDirective',
    '../js/merchant/common/TemplateUrlDirective',
    '../js/merchant/common/ProcessAttributeDirective',
    '../js/merchant/common/AjaxBlockUIController',
    '../js/merchant/common/SwipeDirective',
    '../js/merchant/common/DateTimePickerService',
    '../js/merchant/common/DateTimePickerDirective',
    '../js/merchant/common/VisibilityDirective',
    '../js/merchant/common/Sidebar.sp',

    '../js/merchant/services/AppointmentResourceService',
    '../js/merchant/services/LocalStorageService',
    '../js/merchant/services/i18nService',
    '../js/merchant/services/AjaxService',
    '../js/merchant/services/ShopResourceService',
    '../js/merchant/services/PopupService',
    '../js/merchant/services/ApiWebResourceService',
    '../js/merchant/services/UtilityService',

    '../js/merchant/appointment-list/AppointmentController',
    '../js/merchant/appointment-list/WeekdaysService',
    '../js/merchant/appointment-list/ContentSizeDirective',
    '../js/merchant/appointment-list/InfoStickyDirective',
    '../js/merchant/appointment-list/InfoStickyService',
    '../js/merchant/appointment-list/SelectedDateDirective',
    '../js/merchant/appointment-list/EmployeeFilterController',
    '../js/merchant/appointment-list/EmployeeFilterView',
    '../js/merchant/appointment-list/EmployeeFilterEventBus',

    '../js/merchant/new-appointment/NewAppointmentController',
    '../js/merchant/new-appointment/TimeGridService',
    '../js/merchant/new-appointment/TimeGridDirective',
    '../js/merchant/new-appointment/TimeGridDirective',
    '../js/merchant/new-appointment/ChanelSelectionDirective',
    '../js/merchant/new-appointment/AdjustContentSizeDirective',

    '../js/LoginController'
];

manifest_merchant = manifest_merchant.map(function (fileName) {
    fileName = fileName.replace('../', '');
    return fileName + '.js';
});

manifest_merchant.unshift('js/merchant/main.js');

module.exports = manifest_merchant;