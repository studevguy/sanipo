/**
 * Created by tiger on 30/06/2015.
 */
module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');

    var files_app = {}, files_templates = {};

    files_app[project.dirs.root + 'js/app.' + project.buildTracking + '.js'] = [project.dirs.temp + 'app.' + project.buildTracking + '.js'];

    files_templates[project.dirs.root + 'js/templates.' + project.buildTracking + '.js'] = [project.dirs.temp + 'templates.' + project.buildTracking + '.js'];

    return {
        app: {
            files: files_app
        },
        template: {
            files: files_templates
        }
    };
};