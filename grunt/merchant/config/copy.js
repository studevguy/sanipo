/**
 * Created by sangdao on 30/06/2015.
 */

module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');

    return {
        uglify: {
            files: [
                {
                    flatten: true,
                    expand: true,
                    src: [
                        project.dirs.root + 'js/app.' + project.buildTracking + '.js',
                        project.dirs.root + 'js/templates.' + project.buildTracking + '.js'
                    ],
                    dest: project.dirs.temp
                }
            ]
        },

        finalize: {
            files: [
                {
                    flatten: true,
                    expand: true,
                    src: [
                        project.bowerComponent + 'bxslider-4/src/images/**.*',
                        project.bowerComponent + 'jquery-ui/themes/base/images/**.*'
                    ],
                    dest: project.dirs.root + 'css/images'
                },
                {
                    flatten: true,
                    expand: true,
                    src: [
                        project.bowerComponent + 'bootstrap/fonts/**.*'
                    ],
                    dest: project.dirs.root + 'fonts'
                }
            ]
        },

        prepareBuild: {
            files: [
                {
                    flatten: true,
                    expand: true,
                    src: [
                        project.application.commonDir + project.application.configFile
                    ],
                    dest: project.dirs.temp
                }

            ]
        }
    };
};