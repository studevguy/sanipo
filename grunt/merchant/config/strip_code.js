module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');

    return {
        'strip-code': {
            options: {
                start_comment: 'start-dev-block',
                end_comment: 'end-dev-block'
            },
            src: project.dirs.temp +  project.application.configFile
        }
    };
};