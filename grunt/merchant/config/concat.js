/**
 * Created by sangdao on 30/06/2015.
 */
module.exports = function (grunt) {
    'use strict';

    var project = grunt.config.get('project');

    return {
        default: {
            src: project.files.manifest,
            dest: project.dirs.root + 'js/app.' + project.buildTracking + '.js'
        }
    };
};